package util;

public class LoginUser {

	private String username;
	private String password;

	public LoginUser() {
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
}
