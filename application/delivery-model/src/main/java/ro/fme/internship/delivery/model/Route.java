package ro.fme.internship.delivery.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Route {

	private int idRoute;
	private int idDeposit;
	private int idDriver;
	private String expCounty;
	private String destCounty;

	public Route() {
	}

	public Route(int idDeposit, int idDriver, String expCounty, String destCounty) {
		this.idDeposit = idDeposit;
		this.idDriver = idDriver;
		this.expCounty = expCounty;
		this.destCounty = destCounty;
	}

	public int getIdRoute() {
		return idRoute;
	}

	public void setIdRoute(int idRoute) {
		this.idRoute = idRoute;
	}

	public int getIdDeposit() {
		return idDeposit;
	}

	public void setIdDeposit(int idDeposit) {
		this.idDeposit = idDeposit;
	}

	public int getIdDriver() {
		return idDriver;
	}

	public void setIdDriver(int idDriver) {
		this.idDriver = idDriver;
	}

	public String getExpCounty() {
		return expCounty;
	}

	public void setExpCounty(String expCounty) {
		this.expCounty = expCounty;
	}

	public String getDestCounty() {
		return destCounty;
	}

	public void setDestCounty(String destCounty) {
		this.destCounty = destCounty;
	}

	@Override
	public String toString() {
		return "Route [idRoute=" + idRoute + ", idDeposit=" + idDeposit + ", idDriver=" + idDriver + ", expCounty="
				+ expCounty + ", destCounty=" + destCounty + "]";
	}

}
