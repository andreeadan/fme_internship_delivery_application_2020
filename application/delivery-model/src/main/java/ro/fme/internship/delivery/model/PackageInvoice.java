package ro.fme.internship.delivery.model;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@XmlRootElement
@JsonAutoDetect
public class PackageInvoice {

	private int invoiceId;
	private int returnId;
	private int expClientId;
	private int destClientId;
	private String orderDescription;
	private long orderCost;
	@XmlElement(name = "creation_date")
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date creationDate;
	private String orderStatus;
	private long orderWeight;
	private int depositId;
	@XmlElement(name = "delivery_date")
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date deliveryDate;
	@XmlElement(name = "modify_date")
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date modifyDate;
	private String contentType;
	private String contentLocation;
	private int cashierId;

	public PackageInvoice() {
	}

	public PackageInvoice(int returnId, int expClientId, int destClientId, String orderDescription,
			long orderCost, Date creationDate, String orderStatus, long orderWeight, int depositId,
			Date deliveryDate, Date modifyDate, String contentType, String contentLocation, int cashierId) {
		super();
		this.returnId = returnId;
		this.expClientId = expClientId;
		this.destClientId = destClientId;
		this.orderDescription = orderDescription;
		this.orderCost = orderCost;
		this.creationDate = creationDate;
		this.orderStatus = orderStatus;
		this.orderWeight = orderWeight;
		this.depositId = depositId;
		this.deliveryDate = deliveryDate;
		this.modifyDate = modifyDate;
		this.contentType = contentType;
		this.contentLocation = contentLocation;
		this.cashierId = cashierId;
	}
	
	public PackageInvoice(int expClientId, int destClientId, String orderDescription,
			long orderCost, Date creationDate, String orderStatus, long orderWeight, int depositId,
			Date deliveryDate, Date modifyDate, String contentType, String contentLocation, int cashierId) {
		super();
		this.expClientId = expClientId;
		this.destClientId = destClientId;
		this.orderDescription = orderDescription;
		this.orderCost = orderCost;
		this.creationDate = creationDate;
		this.orderStatus = orderStatus;
		this.orderWeight = orderWeight;
		this.depositId = depositId;
		this.deliveryDate = deliveryDate;
		this.modifyDate = modifyDate;
		this.contentType = contentType;
		this.contentLocation = contentLocation;
		this.cashierId = cashierId;
	}

	public int getInvoice_id() {
		return invoiceId;
	}

	public void setInvoice_id(int invoice_id) {
		invoiceId = invoice_id;
	}

	public int getReturn_id() {
		return returnId;
	}

	public void setReturn_id(int return_id) {
		returnId = return_id;
	}

	public int getExp_client_id() {
		return expClientId;
	}

	public void setExp_client_id(int exp_client_id) {
		expClientId = exp_client_id;
	}

	public int getDest_client_id() {
		return destClientId;
	}

	public void setDest_client_id(int dest_client_id) {
		destClientId = dest_client_id;
	}

	public String getOrder_description() {
		return orderDescription;
	}

	public void setOrder_description(String order_description) {
		orderDescription = order_description;
	}

	public long getOrder_cost() {
		return orderCost;
	}

	public void setOrder_cost(long order_cost) {
		orderCost = order_cost;
	}

	public Date getCreation_date() {
		return creationDate;
	}

	public void setCreation_date(Date creation_date) {
		creationDate = creation_date;
	}

	public String getOrder_status() {
		return orderStatus;
	}

	public void setOrder_status(String order_status) {
		orderStatus = order_status;
	}

	public long getOrder_weight() {
		return orderWeight;
	}

	public void setOrder_weight(long order_weight) {
		orderWeight = order_weight;
	}

	public int getDeposit_id() {
		return depositId;
	}

	public void setDeposit_id(int deposit_id) {
		depositId = deposit_id;
	}

	public Date getDelivery_date() {
		return deliveryDate;
	}

	public void setDelivery_date(Date delivery_date) {
		deliveryDate = delivery_date;
	}

	public Date getModify_date() {
		return modifyDate;
	}

	public void setModify_date(Date modify_date) {
		modifyDate = modify_date;
	}

	public String getContent_type() {
		return contentType;
	}

	public void setContent_type(String content_type) {
		contentType = content_type;
	}

	public String getContent_location() {
		return contentLocation;
	}

	public void setContent_location(String content_location) {
		contentLocation = content_location;
	}

	public int getCashier_id() {
		return cashierId;
	}

	public void setCashier_id(int cashier_id) {
		cashierId = cashier_id;
	}

}

