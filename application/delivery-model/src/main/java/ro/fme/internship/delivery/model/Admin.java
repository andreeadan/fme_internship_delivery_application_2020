package ro.fme.internship.delivery.model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement
@XmlSeeAlso({ User.class })
public class Admin extends User {

	int adminID;

	public Admin() {
	}

	public Admin(String username, String password, String firstName, String lastName) {
		super(username, password, firstName, lastName);
	}

	public int getAdminID() {
		return adminID;
	}

	public void setAdminID(int adminID) {
		this.adminID = adminID;
	}

	@Override
	public String toString() {
		return "Admin " + super.toString();
	}

}
