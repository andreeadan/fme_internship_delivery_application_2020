package ro.fme.internship.delivery.model;

public class RelationFolderInFolder {

	private int relationID;
	private int sourceFolderID;
	private int targetFolderID;

	public RelationFolderInFolder(int sourceFolderID, int targetFolderID) {
		this.sourceFolderID = sourceFolderID;
		this.targetFolderID = targetFolderID;
	}

	public RelationFolderInFolder(int relationID, int sourceFolderID, int targetFolderID) {
		this.relationID = relationID;
		this.sourceFolderID = sourceFolderID;
		this.targetFolderID = targetFolderID;
	}

	public int getRelationID() {
		return relationID;
	}

	public void setRelationID(int relationID) {
		this.relationID = relationID;
	}

	public int getSourceFolderID() {
		return sourceFolderID;
	}

	public void setSourceFolderID(int sourceFolderID) {
		this.sourceFolderID = sourceFolderID;
	}

	public int getTargetFolderID() {
		return targetFolderID;
	}

	public void setTargetFolderID(int targetFolderID) {
		this.targetFolderID = targetFolderID;
	}

}
