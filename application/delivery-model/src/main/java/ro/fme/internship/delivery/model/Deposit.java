package ro.fme.internship.delivery.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Deposit {

	private int depositID;
	private String county;
	private String address;
	private String status;

	public Deposit(String county, String address, String status) {
		this.county = county;
		this.address = address;
		this.status = status;
	}

	public Deposit() {

	}

	public int getDepositID() {
		return depositID;
	}

	public void setDepositID(int depositID) {
		this.depositID = depositID;
	}

	@XmlElement(name = "county")
	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	@XmlElement(name = "address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@XmlElement(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Deposit [depositID=" + depositID + ", county=" + county + ", address=" + address + ", status=" + status
				+ "]";
	}

}
