package ro.fme.internship.delivery.model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement
@XmlSeeAlso({ User.class })
public class Cashier extends User {

	private int cashierID;
	
	public Cashier(String username, String password, String firstName, String lastName) {
		super(username, password, firstName, lastName);
	}

	public Cashier() { }

	public int getCashierID() {
		return cashierID;
	}

	public void setCashierID(int cashierID) {
		this.cashierID = cashierID;
	}

	@Override
	public String toString() {
		return "Cashier " + super.toString();
	}

}
