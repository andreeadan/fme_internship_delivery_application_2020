package ro.fme.internship.delivery.model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement
@XmlSeeAlso({ User.class })
public class Client extends User {

	private int clientID;
	private String address;
	private String phoneNumber;
	private int zipCode;
	private String city;
	private String county;

	public Client() {
	}

	public Client(String username, String password, String firstName, String lastName, String address, String phoneNumber,
			int zipCode, String city, String county) {
		super(username, password, firstName, lastName);
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.zipCode = zipCode;
		this.city = city;
		this.county = county;
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getZipCode() {
		return zipCode;
	}

	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	@Override
	public String toString() {
		return "Client " + super.toString() + ", address=" + address + ", phoneNumber=" + phoneNumber + ", zipCode="
				+ zipCode + ", city=" + city + ", county=" + county;
	}

}
