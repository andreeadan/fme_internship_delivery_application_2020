package ro.fme.internship.delivery.model;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement
public class Folder {

	
	private int folderId;

	private String name;
	@XmlElement(name = "creation_date")
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date creationDate;
	private String owner;
	private int childNr;
	@XmlElement(name = "modify_date")
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date modifyDate;
	private String folderType;
	private ArrayList<Folder> subFolders;


	public Folder() {}
	
	public Folder(String name, Date creationDate, String owner, int childNr, Date modifyDate, String folderType ) {
		this.folderId=folderId;
		this.name=name;
		this.creationDate=creationDate;
		this.owner=owner;
		this.childNr=childNr;
		this.modifyDate=modifyDate;
		this.folderType=folderType;

	}

	public int getFolder_id() {
		return folderId;
	}

	public void setFolder_id(int folderId) {
		this.folderId = folderId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreation_date() {
		return creationDate;
	}

	public void setCreation_date(Date creation_date) {
		this.creationDate = creation_date;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public int getChild_nr() {
		return childNr;
	}

	public void setChild_nr(int child_nr) {
		this.childNr = child_nr;
	}

	public Date getModify_date() {
		return modifyDate;
	}

	public void setModify_date(Date modify_date) {
		this.modifyDate = modify_date;
	}

	public String getFolder_type() {
		return folderType;
	}

	public void setFolder_type(String folder_type) {
		this.folderType = folder_type;
	}

	public ArrayList<Folder> getSubFolders() {
		return subFolders;
	}

	public void setSubFolders(ArrayList<Folder> subFolders) {
		this.subFolders = subFolders;
	}
}



