package ro.fme.internship.delivery.model;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.xml.bind.annotation.adapters.XmlAdapter;

class DateAdapter extends XmlAdapter<String, Date> {

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-d");

	@Override
	public String marshal(Date v) {
		return dateFormat.format(v);
	}

	@Override
	public Date unmarshal(String v) {
		try {
			return new java.sql.Date(dateFormat.parse(v).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
}

