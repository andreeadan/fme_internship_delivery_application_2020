package ro.fme.internship.delivery.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Driver extends User {

	private int driverID;
	private String car;

	public Driver() {
	}

	public Driver(String username, String password, String firstName, String lastName, String car) {
		super(username, password, firstName, lastName);
		this.car = car;
	}

	public int getDriverID() {
		return driverID;
	}

	public void setDriverID(int driverID) {
		this.driverID = driverID;
	}

	@XmlElement(name = "car")
	public String getCar() {
		return car;
	}

	public void setCar(String car) {
		this.car = car;
	}

	@Override
	public String toString() {
		return "Driver: " + super.toString() + ", car=" + car;
	}

}
