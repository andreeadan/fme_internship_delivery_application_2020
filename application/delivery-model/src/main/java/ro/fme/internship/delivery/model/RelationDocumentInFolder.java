package ro.fme.internship.delivery.model;

public class RelationDocumentInFolder {
	private int relationId;
	private int documentId;
	private int folderId;

	public RelationDocumentInFolder(int documentId, int folderId) {
		this.documentId = documentId;
		this.folderId = folderId;
	}

	public int getRelationId() {
		return relationId;
	}

	public void setRelationId(int relationId) {
		this.relationId = relationId;
	}

	public int getDocumentId() {
		return documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	public int getFolderId() {
		return folderId;
	}

	public void setFolderId(int folderId) {
		this.folderId = folderId;
	}

}
