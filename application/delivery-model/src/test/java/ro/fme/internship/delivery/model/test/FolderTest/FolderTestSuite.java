package ro.fme.internship.delivery.model.test.FolderTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import ro.fme.internship.delivery.model.RelationFolderInFolder;

@RunWith(Suite.class)

@Suite.SuiteClasses({
   FolderTest.class,
   RelationFolderInFolder.class,  
})

public class FolderTestSuite {   
} 