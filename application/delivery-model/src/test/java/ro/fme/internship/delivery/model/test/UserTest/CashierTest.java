package ro.fme.internship.delivery.model.test.UserTest;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import ro.fme.internship.delivery.model.Cashier;

public class CashierTest {
	
	static Cashier cashier;
	@BeforeClass
	public static  void setUp() {
		cashier = new Cashier("cashUsername","cashPassword","cash","ier");
	}

	@Test
	public void testSetGetCashierID() {
		cashier.setCashierID(2);
		assertTrue(cashier.getCashierID()==2);
		assertFalse(cashier.getCashierID()==1);
	}

}
