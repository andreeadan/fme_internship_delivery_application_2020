package ro.fme.internship.delivery.model.test.UserTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ro.fme.internship.delivery.model.Client;

public class ClientTest {
	
	Client client;
	/**
	 * set up data for test
	 */
	@Before
	public void setUp() {
		client = new Client("clientUser","clientPass","clientFirst","clientLast","clientAddress","1234567890",1234,"clientCity","clientCounty");
	}

	/**
	 * test methods setClientID and getClientID
	 */
	@Test
	public void testSetGetClientID() {
		client.setClientID(1);
		assertTrue(client.getClientID()==1);
		assertFalse(client.getClientID()==2);
	}
	
	/**
	 * test method getAddress
	 */
	@Test
	public void testGetAddress() {
		assertTrue(client.getAddress().equals("clientAddress"));
		assertFalse(client.getAddress().equals("clientAdd"));	
	}
	
	/**
	 * test method setAddress
	 */
	@Test
	public void testSetAddress() {
		client.setAddress("clientNewAddress");
		assertTrue(client.getAddress().equals("clientNewAddress"));
		assertFalse(client.getAddress().equals("clientAddress"));
	}
	
	/**
	 * test method getPhoneNumber
	 */
	@Test
	public void testGetPhoneNumber() {
		assertTrue(client.getPhoneNumber()=="1234567890");
		assertFalse(client.getPhoneNumber()=="1234567899");
	}
	
	/**
	 * test method setPhoneNumber
	 */
	@Test
	public void testSetPhoneNuber() {
		client.setPhoneNumber("1234567999");
		assertTrue(client.getPhoneNumber()=="1234567999");
		assertFalse(client.getPhoneNumber()=="1234567890");
	}
	
	/**
	 * test method getZipCode
	 */
	@Test
	public void testGetZipCode() {
		assertTrue(client.getZipCode()==1234);
		assertFalse(client.getZipCode()==1222);
	}
	
	/**
	 * test method setZipCode
	 */
	@Test
	public void testSetZipCode() {
		client.setZipCode(1222);
		assertTrue(client.getZipCode()==1222);
		assertFalse(client.getZipCode()==1234);
	}
	
	/**
	 * test method getCity
	 */
	@Test
	public void testGetCity() {
		assertTrue(client.getCity().equals("clientCity"));
		assertFalse(client.getCity().equals("city"));
	}
	
	/**
	 * test method setCity
	 */
	@Test
	public void testSetCity() {
		client.setCity("newCity");
		assertTrue(client.getCity().equals("newCity"));
		assertFalse(client.getCity().equals("clientCity"));
		
	}
	
	/**
	 * test method getCounty
	 */
	@Test
	public void testGetCounty() {
		assertTrue(client.getCounty().equals("clientCounty"));
		assertFalse(client.getCounty().equals("client"));
	}
	
	/**
	 * test method setCounty
	 */
	@Test
	public void testSetCounty() {
		client.setCounty("clientNewCity");
		assertTrue(client.getCounty().equals("clientNewCity"));
		assertFalse(client.getCounty().equals("clientCounty"));
	}
	
}
