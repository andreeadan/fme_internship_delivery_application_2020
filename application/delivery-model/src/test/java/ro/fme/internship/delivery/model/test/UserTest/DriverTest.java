package ro.fme.internship.delivery.model.test.UserTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ro.fme.internship.delivery.model.Driver;

public class DriverTest {
	Driver driver;
	
	/**
	 * set up data for test
	 */
	@Before
	public void setUp() {
		driver = new Driver("driverUsername","driverPass","driverFirst","driverLast","car");
	}

	/**
	 * test methods setDriverID and getDriverID
	 */
	@Test
	public void testSetGetDriverID() {
		driver.setDriverID(1);
		assertTrue(driver.getDriverID()==1);
		assertFalse(driver.getDriverID()==2);
	}
	
	/**
	 * test method getCar
	 */
	@Test
	public void testGetCar() {
		assertTrue(driver.getCar().equals("car"));
		assertFalse(driver.getCar().equals("anotherCar"));
	}
	
	/**
	 * test method setCar
	 */
	@Test
	public void testSetCar() {
		driver.setCar("newCar");
		assertEquals("newCar",driver.getCar());
		assertFalse(driver.getCar().equals("car"));
	}

}
