package ro.fme.internship.delivery.model.test.PackageInvoiceTest;
import static org.junit.Assert.assertEquals;

import java.sql.Date;

import org.junit.Before;
import org.junit.Test;

import ro.fme.internship.delivery.model.PackageInvoice;

public class PackageInvoiceTest {
	
	PackageInvoice pack;
	
	/**
	 * set up data test
	 */
	@Before
	public void setup() {
		Date d1=new Date(2020, 9, 9);
		Date d2=new Date(2020, 8, 9);
		Date d3=new Date(2020, 10, 10);
		 pack = new PackageInvoice(1, 1, 1, "description",(long) 100, d1, "delivered", (long) 100, 1,d2, d3, "books", "Cluj-Napoca", 1);
				
	}
	
	/**
	 * test the method getReturnId
	 */
	@Test
    public void testGetReturnId()
    {   	
		assertEquals(1, pack.getReturn_id());
    }
	
	/**
	 * test the method GetExpClientId
	 */
	@Test
    public void testGetExpClientId()
    {   	
		assertEquals(1, pack.getExp_client_id());
    }
	
    /**
     * test the method getDestClientId
     */
	@Test
    public void testGetDestClientId()
    {   	
		assertEquals(1, pack.getDest_client_id());	 
    }
	
	/**
	 * test the method GetOrderDescription
	 */
	@Test
    public void testGetOrderDescription()
    {   	
		assertEquals("description", pack.getOrder_description());
    }
	
	/**
	 * test the method getOrderCost
	 */
	@Test
    public void testGetOrderCost()
    {   	
		assertEquals((long) 100, pack.getOrder_cost());
		 
    }
	
	/**
	 * test the method getCreationDate
	 */
	@Test
    public void testGetCreationDate()
    {   	
		Date d1=new Date(2020, 9, 9);
		assertEquals(d1, pack.getCreation_date());		 
    }
	
	/**
	 * test the method getOrderStatus
	 */
	@Test
    public void testGetOrderStatus()
    {   	
		assertEquals("delivered", pack.getOrder_status());
		 
    }
	
	/**
	 * test the method getOrderWeight
	 */
	@Test
    public void testGetOrderWeight()
    {   	
		assertEquals((long) 100, pack.getOrder_weight());

    }
	
	/**
	 * test the method getDepositId
	 */
	@Test
    public void testGetDepositId()
    {   	
		
		assertEquals(1, pack.getDeposit_id());
	 
    }
	
	/**
	 * test the method getDeliveryDate
	 */
	@Test
    public void testGetDeliveryDate()
    {   	
		Date d2=new Date(2020, 8, 9);
		assertEquals(d2, pack.getDelivery_date());
		 
    }
	
	/**
	 * test the method getModifyDate
	 */
	@Test
    public void testGetModifyDate()
    {   	
		Date d3=new Date(2020, 10, 10);
		assertEquals(d3, pack.getModify_date());
    }
	
	/**
	 * test the method getContentType
	 */
	@Test
    public void testGetContentType()
    {   	
     	assertEquals("books", pack.getContent_type()); 
    }
	
	/**
	 * test the method getContentLocation 
	 */
	@Test
    public void testGetContentLocation()
    {   	
		assertEquals("Cluj-Napoca", pack.getContent_location());	 
    }
	
	/**
	 * test the method getCashierId
	 */
	@Test
    public void testGetCashierId()
    {   
		assertEquals(1, pack.getCashier_id());
    }
	
}
