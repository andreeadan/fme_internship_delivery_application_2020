package ro.fme.internship.delivery.model.test.UserTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ro.fme.internship.delivery.model.Admin;

public class AdminTest {
	Admin adminTest;
	/**
	 * set up data test
	 */
	@Before
	public void setUp() {
		 adminTest = new Admin("adminTest", "passTest","admin","test"); 
	}
	
	/**
	 * test method getUsername
	 */

	@Test
	public void testGetUsername() {
		assertEquals("adminTest",adminTest.getUsername());
		assertFalse(adminTest.getUsername().equals("admin"));
		
	}
	
	/**
	 * test method setUsername
	 */
	@Test
	public void testSetUsername() {
		adminTest.setUsername("adminTestNew");
		assertEquals("adminTestNew",adminTest.getUsername());
		assertFalse(adminTest.getUsername().equals("admin"));
		
	}
	
	/**
	 * test method getPassword
	 */
	
	@Test
	public void testGetPassword() {
		assertEquals("passTest",adminTest.getPassword());
		assertFalse(adminTest.getPassword().equals("pass"));
	}
	
	/**
	 * test method setPassword
	 */
	
	@Test
	public void testSetPassword() {
		adminTest.setPassword("passTestNew");
		assertEquals("passTestNew",adminTest.getPassword());
		assertFalse(adminTest.getPassword().equals("pass"));
	}
	
	/**
	 * test method getFirstName
	 */
	@Test
	public void testGetFirstName() {
		assertEquals("admin",adminTest.getFirstName());
		assertFalse(adminTest.getFirstName().equals("first"));
	}
	
	/**
	 * test method setFirstName
	 */
	@Test
	public void testSetFirstName() {
		adminTest.setFirstName("adminNew");
		assertEquals("adminNew",adminTest.getFirstName());
		assertFalse(adminTest.getFirstName().equals("first"));

	}
	
	/**
	 * test method getLastName
	 */
	@Test
	public void testGetLastName() {
		assertEquals("test",adminTest.getLastName());
		assertFalse(adminTest.getLastName().equals("last"));

	}
	
	/**
	 * test method setLastName
	 */
	@Test
	public void testSetLastName() {
		adminTest.setLastName("testNew");
		assertEquals("testNew",adminTest.getLastName());
		assertFalse(adminTest.getLastName().equals("last"));

	}
	
	/**
	 * test methods setAdminID and getAdminID 
	 */
	@Test
	public void testSetGetAdminID() {
		adminTest.setAdminID(1);
		assertTrue(adminTest.getAdminID()==1);
		assertFalse(adminTest.getAdminID()==2);
	}
	
	
	
	
	
	

}
