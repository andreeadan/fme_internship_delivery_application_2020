package ro.fme.internship.delivery.model.test.FolderTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.sql.Date;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Folder;
import ro.fme.internship.delivery.model.RelationFolderInFolder;

public class RelationFolderInFolderTest {
	
	private static final Logger logger = LoggerFactory.getLogger(RelationFolderInFolderTest.class);
	RelationFolderInFolder relation;
	
	@Before
	public void setUp() {
		relation = new RelationFolderInFolder(0, 1);
	}
	
	@Test
	public void testConstructor() {
		logger.info("\nconstructor testing started...");
		
		logger.info("set source id test case...");
		RelationFolderInFolder relation1 = new RelationFolderInFolder(1, 1);
		relation1.setSourceFolderID(0);
		assertEquals(relation.getSourceFolderID(), relation1.getSourceFolderID());
		
		logger.info("set target id test case...");
		RelationFolderInFolder relation2 = new RelationFolderInFolder(0, 1);
		relation2.setTargetFolderID(0);
		assertNotEquals(relation.getTargetFolderID(), relation2.getTargetFolderID());
		
		logger.info("get on inexsitent source id test case...");
		assertNotEquals(3, relation.getSourceFolderID());
		
		logger.info("get on inexsitent targer id test case...");
		assertNotEquals(3, relation.getTargetFolderID());
		
		logger.info("constructor testing stopped...\n");
	}
	
	/**
	 * test the getters and setters for relation id
	 */
	@Test
	public void testRelationID() {
		logger.info("\ntestRelationID started...");
		relation.setRelationID(0);
		
		logger.info("test a good relation id...");
		assertEquals(0, relation.getRelationID());
		
		logger.info("test a bad relation id...");
		assertNotEquals(3, relation.getRelationID());
		
		logger.info("testRelationID finished...\n");
	}
	
	/**
	 * test the getters and setters for source folder id
	 */
	@Test
	public void testSourceFolderID() {
		logger.info("\ntestSourceFolderID started...");
		
		logger.info("test current get on source folder id...");
		assertEquals(0, relation.getSourceFolderID());
		
		logger.info("test setting a new source folder id...");
		relation.setSourceFolderID(1);
		assertEquals(1, relation.getSourceFolderID());
		
		logger.info("test bad source folder id as input...");
		assertNotEquals(3, relation.getSourceFolderID());
		
		logger.info("testSourceFolderID finished...\n");
	}
	
	/**
	 * test the getters and setters for target folder id
	 */
	@Test
	public void testTargetFolderID() {
		logger.info("\ntestTargetFolderID started...");
		
		logger.info("test current get on target folder id...");
		assertEquals(1, relation.getTargetFolderID());
		
		logger.info("test setting a new target folder id...");
		relation.setTargetFolderID(2);
		assertEquals(2, relation.getTargetFolderID());
		
		logger.info("test bad target folder id as input...");
		assertNotEquals(3, relation.getTargetFolderID());
		
		logger.info("testTargetFolderID finished...\n");
	}


}
