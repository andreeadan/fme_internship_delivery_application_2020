package ro.fme.internship.delivery.model.test.FolderTest;

import static org.junit.Assert.*;

import java.sql.Date;

import org.junit.Before;
import org.junit.Test;

import ro.fme.internship.delivery.model.Folder;

public class FolderTest {
	
	Folder folderTest;
	Date creationD;
	/**
	 * set up data for test
	 */
	@Before
	public void setUp() {
		creationD = new Date(2020,10,10);
		folderTest = new Folder("f",creationD,"f",2,creationD,"type");
	}
	
	@Test
	public void testSetGetFolder_id() {
		folderTest.setFolder_id(1);
		assertTrue(folderTest.getFolder_id()==1);
		assertFalse(folderTest.getFolder_id()==2);
	}
	/**
	 * test method getName
	 */
	@Test
	public void testGetName() {
		assertTrue(folderTest.getName().equals("f"));
		assertFalse(folderTest.getModify_date().equals("a"));
	}
	
	/**
	 * test method setName
	 */
	@Test
	public void testSetName() {
		folderTest.setName("folder");
		assertTrue(folderTest.getName().equals("folder"));
		assertFalse(folderTest.getName().equals("f"));
	}
	
	/**
	 * test method getCreationDate
	 */
	@Test
	public void testGetCreationDate() {
		assertEquals(creationD,folderTest.getCreation_date());
		Date anotherDate = new Date(2020,11,11);
		assertNotEquals(anotherDate,folderTest.getCreation_date());
	}
	
	/**
	 * test method setCreationDate
	 */
	@Test
	public void testSetCreationDate() {
		Date newDate = new Date(2020,9,9);
		folderTest.setCreation_date(newDate);
		assertEquals(newDate, folderTest.getCreation_date());
		assertNotEquals(creationD, folderTest.getCreation_date());
	}
	
	/**
	 * test method getOwner
	 */
	@Test
	public void testGetOwner() {
		assertEquals("f",folderTest.getOwner());
		assertNotEquals("ff", folderTest.getOwner());
	}
	
	/**
	 * test method setOwner
	 */
	@Test
	public void testSetOwner() {
		folderTest.setOwner("newOwner");
		assertEquals("newOwner",folderTest.getOwner());
		assertNotEquals("f", folderTest.getOwner());
	}
	
	/**
	 * test method getChild_nr
	 */
	
	@Test
	public void testGetChildID() {
		assertTrue(folderTest.getChild_nr()==2);
		assertFalse(folderTest.getChild_nr()==1);
	}
	
	/**
	 *  test method setChild_nr
	 */
	@Test
	public void testSetChildID() {
		folderTest.setChild_nr(3);
		assertTrue(folderTest.getChild_nr()==3);
		assertFalse(folderTest.getChild_nr()==2);
	}
	
	/**
	 * test method getModify_date
	 */
	@Test
	public void testGetModify_date() {
		assertEquals(creationD,folderTest.getModify_date());
		Date newDate = new Date(2020,9,9);
		assertNotEquals(newDate, folderTest.getModify_date());
		
	}
	/**
	 * test method setModify_date
	 */
	@Test
	public void testSetModify_date() {
		Date newDate = new Date(2020,9,9);
		folderTest.setModify_date(newDate);
		assertEquals(newDate, folderTest.getModify_date());
		assertNotEquals(creationD,folderTest.getModify_date());
	}
	
	/**
	 * test method getFolder_type
	 */
	@Test
	public void testGetFolder_type() {
		assertTrue(folderTest.getFolder_type().equals("type"));
		assertFalse(folderTest.getFolder_type().equals("noType"));
	}

	/**
	 * test method setFolder_type
	 */
	@Test
	public void testSetFolder_type() {
		folderTest.setFolder_type("newType");
		assertTrue(folderTest.getFolder_type().equals("newType"));
		assertFalse(folderTest.getFolder_type().equals("type"));
	}
}
