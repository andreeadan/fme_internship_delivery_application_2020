package ro.fme.internship.delivery.model.test.UserTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
   AdminTest.class,
   CashierTest.class,
   ClientTest.class,
   DriverTest.class,
})

public class TestSuiteUser {   
} 
