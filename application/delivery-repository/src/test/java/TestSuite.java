import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import ro.fme.internship.delivery.database.connection.test.DBConnection.DBConnectionTest;
import ro.fme.internship.delivery.model.test.PackageInvoiceTest.PackageInvoiceTest;
import ro.fme.internship.delivery.repository.test.DepositRepositoryTest.DepositRepositoryTest;
import ro.fme.internship.delivery.repository.test.DepositRepositoryTest.RouteRepositoryTest;
import ro.fme.internship.delivery.repository.test.FolderRepositoryTest.FolderInFolderRepositoryTest;
import ro.fme.internship.delivery.repository.test.FolderRepositoryTest.FolderRepositoryTest;
import ro.fme.internship.delivery.repository.test.UserRepositoyTest.UserDeleteTest;
import ro.fme.internship.delivery.repository.test.UserRepositoyTest.UserInsertTest;
import ro.fme.internship.delivery.repository.test.UserRepositoyTest.UserUpdateTest;

@RunWith(Suite.class)

@Suite.SuiteClasses({
   DBConnectionTest.class,
   UserInsertTest.class,
   UserDeleteTest.class,
   UserUpdateTest.class,
   DepositRepositoryTest.class,
   RouteRepositoryTest.class,
   PackageInvoiceTest.class,
   FolderRepositoryTest.class,
   FolderInFolderRepositoryTest.class
})

public class TestSuite {   
} 