package ro.fme.internship.delivery.repository.test.FolderRepositoryTest;

import static org.junit.Assert.*;

import java.sql.Date;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Folder;
import ro.fme.internship.delivery.repository.implementation.FolderRepository;
import ro.fme.internship.delivery.repository.implementation.RelationFolderInFolderRepository;

public class FolderRepositoryTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(FolderRepositoryTest.class);
	FolderRepository folderRepository;

	/**
	 * set data for tests
	 */
	@Before
	public void setUp() {
		folderRepository = new FolderRepository();
	}
	
	/**
	 * test if a folder was found in Folder table
	 */
	
	
	@Test
	public void testGetFolderById() {
		LOGGER.info("\ntestGetFolderById started...");
		
		Folder folder;
		int id=1;
		folder=folderRepository.getFolderWithId(id);
		
		LOGGER.info("test if the folder was found in db");
		assertFalse(folder!=null);
	
		LOGGER.info("testGetFolderById finished...\n");
	}

	/**
	 * test if a folder was added in Folder table
	 */
	@Test
	public void testInsertFolder() {
		LOGGER.info("\ntestInsertFolder started...");
		
		int initialSize = folderRepository.getAllFolders().size();
		Date creationD = new Date(2020, 10, 10);
		Folder folder = new Folder("f", creationD, "f", 2, creationD, "f");
		
		LOGGER.info("test if the folder created did not affect the repository...");
		assertTrue(folderRepository.getAllFolders().size() == initialSize);
		
		LOGGER.info("insert created folder in the repository...");
		folderRepository.insertFolder(folder);
		LOGGER.info("test if the insertion worked...");
		assertTrue(folderRepository.getAllFolders().size() == initialSize + 1);
		
		LOGGER.info("test if the folder data was also saved...");
		assertTrue(folderRepository.getAllFolders().get(initialSize).getChild_nr() == 2);
		assertFalse(folderRepository.getAllFolders().get(initialSize).getChild_nr() == 3);
		
		LOGGER.info("testInsertFolder finished...\n");
	}
	
	@Test
	public void testGetFolderByName() {
		LOGGER.info("\ntestGetFolderByName started...");
		LOGGER.info("get started...");
		Folder folder= folderRepository.getFolderByName("2020");
		LOGGER.info("test if the folder is not null...");
		assertNotNull(folder);
		LOGGER.info("test if the folder owner is correct...");
		assertEquals("me",folder.getOwner());
		LOGGER.info("test if the folder type is correct...");
		assertEquals("intern",folder.getFolder_type());
	}

}
