package ro.fme.internship.delivery.repository.test.UserRepositoyTest;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.runner.OrderWith;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import ro.fme.internship.delivery.model.Admin;
import ro.fme.internship.delivery.model.Cashier;
import ro.fme.internship.delivery.model.Client;
import ro.fme.internship.delivery.model.Driver;
import ro.fme.internship.delivery.repository.implementation.UserRepository;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserInsertTest {

	private static UserRepository userRepo;
	private static final Logger LOGGER = LoggerFactory.getLogger(UserInsertTest.class);

	@BeforeClass
	public static void setUp() {
		userRepo = new UserRepository();
	}

	/**
	 * tests if an admin was added in Admin table
	 */
	@Test
	public void testInsertAdmin() {
		LOGGER.info("\ntestInsertAdmin started...");
		Admin admin = new Admin("a", "a", "a", "a");

		int listSize = userRepo.getAdminUsers().size();

		LOGGER.info("perform insert...");
		userRepo.insertUser(admin);

		LOGGER.info("test size change...");
		assertTrue(userRepo.getAdminUsers().size() == (listSize + 1));
		LOGGER.info("test the new user's username equals a good value...");
		assertTrue(userRepo.getAdminUsers().get(listSize).getUsername().equals("a"));
		LOGGER.info("test the new user's username does not equal a bad value...");
		assertFalse(userRepo.getAdminUsers().get(listSize).getUsername().equals("aa"));
		
		LOGGER.info("testInsertAdmin finished...\n");
	}

	/**
	 * tests if a driver was added in Driver Table
	 */
	@Test
	public void testInsertDriver() {
		LOGGER.info("\ntestInsertDriver started...");
		Driver driverTest = new Driver("d", "d", "d", "d", "d");

		int listSize = userRepo.getDriverUsers().size();

		LOGGER.info("perform insert...");
		userRepo.insertUser(driverTest);

		LOGGER.info("test size change...");
		assertTrue(userRepo.getDriverUsers().size() == (listSize + 1));
		LOGGER.info("test the new user's username equals a good value...");
		assertTrue(userRepo.getDriverUsers().get(listSize).getCar().equals("d"));
		LOGGER.info("test the new user's username does not equal a bad value...");
		assertFalse(userRepo.getDriverUsers().get(listSize).getCar().equals("a"));
		
		LOGGER.info("testInsertDriver finished...\n");
	}

	/**
	 * test if a client was inserted in Client table
	 */
	@Test
	public void testInsertClient() {
		LOGGER.info("\ntestInsertClient started...");
		Client clientTest = new Client("c", "c", "c", "c", "c", "1111111111", 1111, "c", "c");

		int listSize = userRepo.getClientUsers().size();
		
		LOGGER.info("perform insert...");
		userRepo.insertUser(clientTest);

		LOGGER.info("test size change...");
		assertTrue(userRepo.getClientUsers().size() == (listSize + 1));
		LOGGER.info("test the new user's username equals a good value...");
		assertTrue(userRepo.getClientUsers().get(listSize).getCounty().equals("c"));
		LOGGER.info("test the new user's username does not equal a bad value...");
		assertFalse(userRepo.getClientUsers().get(listSize).getCounty().equals("a"));
		
		LOGGER.info("testInsertClient finished...\n");
	}

	/**
	 * test if a cashier was inserted in Cashier table
	 */
	@Test
	public void testInsertCashier() {
		LOGGER.info("\ntestInsertCashier started...");
		Cashier cashier = new Cashier("c", "c", "c", "c");

		int listSize = userRepo.getCashierUsers().size();

		LOGGER.info("perform insert...");
		userRepo.insertUser(cashier);

		LOGGER.info("test size change...");
		assertTrue(userRepo.getCashierUsers().size() == (listSize + 1));
		LOGGER.info("test the new user's username equals a good value...");
		assertTrue(userRepo.getCashierUsers().get(listSize).getUsername().equals("c"));
		LOGGER.info("test the new user's username does not equal a bad value...");
		assertFalse(userRepo.getCashierUsers().get(listSize).getUsername().equals("aa"));
		
		LOGGER.info("testInsertCashier finished...\n");
	}
	
}
