package ro.fme.internship.delivery.repository.test.UserRepositoyTest;

import static org.junit.Assert.assertNotEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Admin;
import ro.fme.internship.delivery.model.Cashier;
import ro.fme.internship.delivery.model.Client;
import ro.fme.internship.delivery.model.Driver;
import ro.fme.internship.delivery.repository.implementation.UserRepository;

public class UserUpdateTest {

	private static UserRepository userRepo;
	private static final Logger LOGGER = LoggerFactory.getLogger(UserUpdateTest.class);

	@BeforeClass
	public static void setUp() {
		userRepo = new UserRepository();
	}

	/**
	 * test if an admin was updated in the Admin table
	 */
	@Test
	public void testUpdateAdmin() {
		LOGGER.info("\ntestUpdateAdmin started...");
		Admin oldAdmin = new Admin("big_boss", "pass", "Big", "Boss");
		Admin updatedAdmin = new Admin("big_boss", "pass1", "Big1", "Boss1");
		userRepo.insertUser(oldAdmin);

		LOGGER.info("perform update...");
		userRepo.updateUser(updatedAdmin, oldAdmin.getUsername());
		LOGGER.info("test the user's update on password...");
		assertNotEquals(oldAdmin.getPassword(), updatedAdmin.getPassword());
		LOGGER.info("test the user's update on last name...");
		assertNotEquals(oldAdmin.getLastName(), updatedAdmin.getLastName());
		LOGGER.info("test the user's update on first name...");
		assertNotEquals(oldAdmin.getFirstName(), updatedAdmin.getFirstName());

		LOGGER.info("testUpdateAdmin finished...\n");
	}

	/**
	 * test if a cashier was updated in the Cashier table
	 */
	@Test
	public void testUpdateCashier() {
		LOGGER.info("\ntestUpdateCashier started...");
		Cashier oldCashier = new Cashier("is_cashy_cashy", "pass", "Big", "Cashz");
		Cashier updatedCashier = new Cashier("is_cashy_cashy", "pass1", "Big1", "Cash");
		userRepo.insertUser(oldCashier);

		LOGGER.info("perform update...");
		userRepo.updateUser(updatedCashier, oldCashier.getUsername());
		LOGGER.info("test the user's update on password...");
		assertNotEquals(oldCashier.getPassword(), updatedCashier.getPassword());
		LOGGER.info("test the user's update on last name...");
		assertNotEquals(oldCashier.getLastName(), updatedCashier.getLastName());
		LOGGER.info("test the user's update on first name...");
		assertNotEquals(oldCashier.getFirstName(), updatedCashier.getFirstName());

		LOGGER.info("testUpdateCashier finished...\n");
	}

	/**
	 * test if a driver was updated in the Driver table
	 */
	@Test
	public void testUpdateDriver() {
		LOGGER.info("\ntestUpdateDriver started...");
		Driver oldDriver = new Driver("me_driver", "pass", "Big", "Driver", "car");
		Driver updatedDriver = new Driver("me_driver", "pass1", "Big1", "Driver1", "car1");
		userRepo.insertUser(oldDriver);

		LOGGER.info("perform update...");
		userRepo.updateUser(updatedDriver, oldDriver.getUsername());
		LOGGER.info("test the user's update on password...");
		assertNotEquals(oldDriver.getPassword(), updatedDriver.getPassword());
		LOGGER.info("test the user's update on last name...");
		assertNotEquals(oldDriver.getLastName(), updatedDriver.getLastName());
		LOGGER.info("test the user's update on first name...");
		assertNotEquals(oldDriver.getFirstName(), updatedDriver.getFirstName());
		LOGGER.info("test the user's update on car...");
		assertNotEquals(oldDriver.getCar(), updatedDriver.getCar());

		LOGGER.info("testUpdateDriver finished...\n");
	}

	/**
	 * test if a client was updated in the Client table
	 */
	@Test
	public void testUpdateClient() {
		LOGGER.info("\ntestUpdateClient started...");
		Client oldClient = new Client("caracatita_fericita", "pass", "Tony", "Octopus", "langa coapcul din parc",
				"555000555", 400292, "cluj-napoca", "cluj");
		Client updatedClient = new Client("caracatita_fericita", "pass1", "Honey", "Stingray",
				"langa noul coapc din parc", "555111555", 400291, "brasov", "brasov");
		userRepo.insertUser(oldClient);

		LOGGER.info("perform update...");
		userRepo.updateUser(updatedClient, oldClient.getUsername());
		LOGGER.info("test the user's update on password...");
		assertNotEquals(oldClient.getPassword(), updatedClient.getPassword());
		LOGGER.info("test the user's update on last name...");
		assertNotEquals(oldClient.getLastName(), updatedClient.getLastName());
		LOGGER.info("test the user's update on first name...");
		assertNotEquals(oldClient.getFirstName(), updatedClient.getFirstName());
		LOGGER.info("test the user's update on address...");
		assertNotEquals(oldClient.getAddress(), updatedClient.getAddress());
		LOGGER.info("test the user's update on phone number...");
		assertNotEquals(oldClient.getPhoneNumber(), updatedClient.getPhoneNumber());
		LOGGER.info("test the user's update on zip code...");
		assertNotEquals(oldClient.getZipCode(), updatedClient.getZipCode());
		LOGGER.info("test the user's update on city...");
		assertNotEquals(oldClient.getCity(), updatedClient.getCity());
		LOGGER.info("test the user's update on county...");
		assertNotEquals(oldClient.getCounty(), updatedClient.getCounty());

		LOGGER.info("testUpdateClient finished...\n");
	}

	/**
	 * test if a driver's car was updated in the Driver table
	 */
	@Test
	public void testUpdateDriverCar() {
		LOGGER.info("\ntestUpdateDriverCar started...");
		String newCar = "new_car";
		String oldCar = "car";
		Driver driver = new Driver("driver_has_car", "pass", "New", "Driver", oldCar);
		Driver updatedDriver = new Driver("driver_has_car", "pass", "New", "Driver", newCar);
		userRepo.insertUser(driver);

		LOGGER.info("perform update...");
		userRepo.updateDriverCar(newCar, driver.getUsername());
		LOGGER.info("test the user's update on car...");
		assertNotEquals(oldCar, updatedDriver.getCar());

		LOGGER.info("testUpdateDriverCar finished...\n");
	}
}
