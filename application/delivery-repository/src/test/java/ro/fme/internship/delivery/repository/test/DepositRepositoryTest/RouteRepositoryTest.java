package ro.fme.internship.delivery.repository.test.DepositRepositoryTest;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Route;
import ro.fme.internship.delivery.repository.implementation.RouteRepository;

public class RouteRepositoryTest {

	private static RouteRepository routeRepo;
	private static final Logger LOGGER = LoggerFactory.getLogger(RouteRepositoryTest.class);

	@BeforeClass
	public static void setUp() {
		routeRepo = new RouteRepository();
	}

	/**
	 * test if a route was updated in the Route table
	 */
	@Test
	public void testUpdateDeposit() {
		LOGGER.info("\ntestUpdateDeposit started...");
		Route oldRoute = new Route(1, 51, "Cluj", "Brasov");
		Route updatedRoute = new Route(2, 61, "Cluj", "Brasov");

		LOGGER.info("inserting the route...");
		routeRepo.insertRoute(oldRoute);

		LOGGER.info("performing the update...");
		routeRepo.updateRoute(updatedRoute, oldRoute.getIdRoute());

		LOGGER.info("test the deposit id change...");
		assertNotEquals(oldRoute.getIdDeposit(), updatedRoute.getIdDeposit());
		LOGGER.info("test the driver id change...");
		assertNotEquals(oldRoute.getIdDriver(), updatedRoute.getIdDriver());

		LOGGER.info("testUpdateDeposit finished...");
	}

	/**
	 * test if a route can be taken from the Route table
	 */
	@Test
	public void testGetRouteByID() {
		LOGGER.info("\ntestGetRouteByID started...");

		LOGGER.info("get the route from database...");
		Route route = routeRepo.getRouteByID(3);

		LOGGER.info("test the values from route fields...");
		assertTrue(route.getIdDeposit() == 1);
		assertTrue(route.getIdDriver() == 1);
		assertTrue(route.getDestCounty().equals("countyDest"));
		assertTrue(route.getExpCounty().equals("countyExp"));

		LOGGER.info("testGetRouteByID finished...\n");
	}

}
