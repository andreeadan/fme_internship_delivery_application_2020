package ro.fme.internship.delivery.repository.test.UserRepositoyTest;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Admin;
import ro.fme.internship.delivery.model.Cashier;
import ro.fme.internship.delivery.model.Client;
import ro.fme.internship.delivery.model.Driver;
import ro.fme.internship.delivery.repository.implementation.UserRepository;

public class UserDeleteTest {

	private static UserRepository userRepo;
	private static final Logger LOGGER = LoggerFactory.getLogger(UserDeleteTest.class);

	/**
	 * set data for tests
	 */
	@BeforeClass
	public static void setUp() {
		userRepo = new UserRepository();
	}

	/**
	 * test if an admin was correctly deleted from the database
	 */
	@Test
	public void testDeleteAdmin() {
		LOGGER.info("\ntestDeleteAdmin started...");
		Admin admin = new Admin("admin", "admin", "admin", "admin");
		userRepo.insertUser(admin);

		int listSize = userRepo.getAdminUsers().size();

		LOGGER.info("perform delete...");
		userRepo.deleteUser(admin);

		LOGGER.info("test for no size change...");
		assertTrue(userRepo.getAdminUsers().size() == listSize);
		LOGGER.info("test username change to null");
		assertNull(userRepo.getAdminUsers().get(listSize - 1).getUsername());

		LOGGER.info("testDeleteAdmin finished...\n");
	}

	/**
	 * test if a driver was correctly deleted from the database
	 */
	@Test
	public void testDeleteDriver() {
		LOGGER.info("\ntestDeleteDriver started...");
		Driver driver = new Driver("driver", "driver", "driver", "driver", "driver");
		userRepo.insertUser(driver);

		int listSize = userRepo.getDriverUsers().size();

		LOGGER.info("perform delete...");
		userRepo.deleteUser(driver);

		LOGGER.info("test for no size change...");
		assertTrue(userRepo.getDriverUsers().size() == listSize);
		LOGGER.info("test username change to null");
		assertNull(userRepo.getDriverUsers().get(listSize - 1).getUsername());
		
		LOGGER.info("testDeleteDriver finished...\n");
	}

	/**
	 * test if a cashier was correctly deleted from the database
	 */
	@Test
	public void testDeleteCashier() {
		LOGGER.info("\ntestDeleteCashier started...");
		Cashier cashier = new Cashier("cashier", "cashier", "cashier", "cashier");
		userRepo.insertUser(cashier);

		int listSize = userRepo.getCashierUsers().size();

		LOGGER.info("perform delete...");
		userRepo.deleteUser(cashier);

		LOGGER.info("test for no size change...");
		assertTrue(userRepo.getCashierUsers().size() == listSize);
		LOGGER.info("test username change to null");
		assertNull(userRepo.getCashierUsers().get(listSize - 1).getUsername());
		
		LOGGER.info("testDeleteCashier finished...\n");
	}

	/**
	 * test if a client was correctly deleted from the database
	 */
	@Test
	public void testDeleteClient() {
		LOGGER.info("\ntestDeleteClient started...");
		Client client = new Client("client", "client", "client", "client", "client", "2", 3, "client", "client");
		userRepo.insertUser(client);

		int listSize = userRepo.getCashierUsers().size();

		LOGGER.info("perform delete...");
		userRepo.deleteUser(client);

		LOGGER.info("test for no size change...");
		assertTrue(userRepo.getClientUsers().size() == listSize);
		LOGGER.info("test username change to null");
		assertNull(userRepo.getClientUsers().get(listSize - 1).getUsername());
		
		LOGGER.info("testDeleteClient finished...\n");
	}
}
