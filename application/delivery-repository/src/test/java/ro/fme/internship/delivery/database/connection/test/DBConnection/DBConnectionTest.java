package ro.fme.internship.delivery.database.connection.test.DBConnection;
import static org.junit.Assert.assertNotEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.database.connection.DBConnection;

public class DBConnectionTest {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DBConnectionTest.class);
	DBConnection dbConn;
	
	/**
	 * set data for tests
	 */
	@Before
	public void setup() {
		dbConn = new DBConnection();
	}
	
	/**
	 * test the db connection
	 */
	@Test
	public void testConnection() {
		LOGGER.info("\ntestConnection started...");
		
		LOGGER.info("test the connection...");
		assertNotEquals(null, dbConn.getConnection());
		
		LOGGER.info("testConnection finished...\n");
	}
	
	@After
	public void cleanup() {
		dbConn.closeConnection();
	}
}
