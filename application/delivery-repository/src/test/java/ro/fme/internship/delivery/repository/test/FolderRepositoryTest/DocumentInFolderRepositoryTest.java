package ro.fme.internship.delivery.repository.test.FolderRepositoryTest;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.database.connection.test.DBConnection.DBConnectionTest;
import ro.fme.internship.delivery.model.Folder;
import ro.fme.internship.delivery.model.PackageInvoice;
import ro.fme.internship.delivery.model.RelationDocumentInFolder;
import ro.fme.internship.delivery.repository.IRelationDocumentInFolder;
import ro.fme.internship.delivery.repository.implementation.RelationDocumentInFolderRepository;

public class DocumentInFolderRepositoryTest {
	private static IRelationDocumentInFolder relationDocumentInFolder;
	private static final Logger LOGGER = LoggerFactory.getLogger(DocumentInFolderRepositoryTest.class);

	@BeforeClass
	public static void setUp() {
		
		 relationDocumentInFolder = new RelationDocumentInFolderRepository();
	}
	
	@Test
	public void testGetAllRelDocInFol() {
		LOGGER.info("\ntestGetAllRelDocInFol started...");
		
		LOGGER.info("perform get...");
		ArrayList<RelationDocumentInFolder> docInFol = relationDocumentInFolder.getAllRelationDocumentInFolder();
		LOGGER.info("check if the size of list match...");
		assertEquals(2, docInFol.size());
		LOGGER.info("check if the targetId match");
		assertEquals(9, docInFol.get(0).getFolderId());
		LOGGER.info("check if the sourceId match");
		assertEquals(41, docInFol.get(0).getDocumentId());
		LOGGER.info("testGetAllRelDocInFol finished...\n");
	}
	
	@Test
	public void testGetDocumentsOfTargetFolder() {
		LOGGER.info("\ntestGetDocumentsOfTargetFolder started...");
		
		Folder folder =new Folder("fol1", new Date(10,10,20),"owner", 8,new Date(10,11,21),"fol2");
		folder.setFolder_id(9);

		LOGGER.info("perform get...");
		ArrayList<RelationDocumentInFolder> filteredRelaions = relationDocumentInFolder.getDocumentsOfTargetFolder(folder);
		LOGGER.info("check if the size of list match...");
		assertEquals(1, filteredRelaions.size());
		
		LOGGER.info("testGetDocumentsOfTargetFolder finished...\n");
	}
	
	@Test
	public void testgetInvoicesOfTargetFolder() {
		LOGGER.info("\ntestgetInvoicesOfTargetFolder started...");
		
		Folder folder =new Folder("fol1", new Date(10,10,20),"owner", 8,new Date(10,11,21),"fol2");
		folder.setFolder_id(9);

		LOGGER.info("perform get...");
		ArrayList<PackageInvoice> filteredRelaions = relationDocumentInFolder.getInvoicesOfTargetFolder(folder);
		LOGGER.info("check if the size of list match...");
		assertEquals(1, filteredRelaions.size());
		
		LOGGER.info("check if the description match...");
		assertEquals("ceva", filteredRelaions.get(0).getOrder_description());
		
		LOGGER.info("testgetInvoicesOfTargetFolder finished...\n");
	}
}
