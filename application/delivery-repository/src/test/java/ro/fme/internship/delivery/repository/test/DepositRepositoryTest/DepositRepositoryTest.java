package ro.fme.internship.delivery.repository.test.DepositRepositoryTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Deposit;
import ro.fme.internship.delivery.repository.implementation.DepositRepository;

public class DepositRepositoryTest {

	private static DepositRepository depositRepo;
	private static final Logger LOGGER = LoggerFactory.getLogger(DepositRepositoryTest.class);

	@BeforeClass
	public static void setUp() {
		depositRepo = new DepositRepository();
	}
	
	/**
	 * test if a deposit can be recovered from the Deposit table
	 */
	@Test
	public void testGetDeposit() {
		LOGGER.info("\ntestGetDeposit started...");
		Deposit deposit = new Deposit("cluj", "langa strada", "activ");
		
		LOGGER.info("perform get...");
		Deposit newDeposit = depositRepo.getDeposit(1);
		
		LOGGER.info("test if the county matches...");
		assertEquals(deposit.getCounty(), newDeposit.getCounty());
		LOGGER.info("test if the address matches...");
		assertEquals(deposit.getAddress(), newDeposit.getAddress());
		LOGGER.info("test if the status matches...");
		assertEquals(deposit.getStatus(), newDeposit.getStatus());
		
		LOGGER.info("testGetDeposit finished...\n");
	}

	/**
	 * test if a deposit was updated in the Deposit table
	 */
	@Test
	public void testUpdateDeposit() {
		LOGGER.info("\ntestUpdateDeposit started...");
		Deposit oldDeposit = depositRepo.getDeposit(3);
		Deposit updatedDeposit = new Deposit("county1", "address1", "status1");

		LOGGER.info("perform update...");
		depositRepo.updateDeposit(updatedDeposit, oldDeposit.getDepositID());
		
		LOGGER.info("test the county change...");
		assertNotEquals(oldDeposit.getCounty(), updatedDeposit.getCounty());
		LOGGER.info("test the address change...");
		assertNotEquals(oldDeposit.getAddress(), updatedDeposit.getAddress());
		LOGGER.info("test the status change...");
		assertNotEquals(oldDeposit.getStatus(), updatedDeposit.getStatus());
		
		LOGGER.info("testUpdateDeposit finished...\n");
	}
	

}
