package ro.fme.internship.delivery.repository.test.UserRepositoyTest;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Admin;
import ro.fme.internship.delivery.model.Cashier;
import ro.fme.internship.delivery.model.Client;

import ro.fme.internship.delivery.model.Driver;
import ro.fme.internship.delivery.repository.implementation.UserRepository;

public class UserGetTest {
	
	private static UserRepository userRepo;
	private static final Logger LOGGER = LoggerFactory.getLogger(UserGetTest.class);
	
	/**
	 * set data for tests
	 */
	@BeforeClass
	public static void setUp() {
		userRepo = new UserRepository();
	}
	
	/**
	 * test the get from the Driver table
	 */
	@Test
	public void testGetDriver() {
		LOGGER.info("\ntestGetDriver started...");
		Driver driver = new Driver("user4", "pass4", "Mihai", "Eminescu", "Tesla");
		
		LOGGER.info("perform get...");
		Driver newDriver = userRepo.getDriver(81);
		
		LOGGER.info("test if the username matches...");
		assertEquals(driver.getUsername(), newDriver.getUsername());
		LOGGER.info("test if the password matches...");
		assertEquals(driver.getPassword(), newDriver.getPassword());
		LOGGER.info("test if the first name matches...");
		assertEquals(driver.getFirstName(), newDriver.getFirstName());
		LOGGER.info("test if the last name matches...");
		assertEquals(driver.getLastName(), newDriver.getLastName());
		LOGGER.info("test if the car matches...");
		assertEquals(driver.getCar(), newDriver.getCar());
		
		LOGGER.info("testGetDriver finished...\n");
	}

	
	/**
	 * test the get from the Client table
	 */
	@Test
	public void testGetClient() {
		LOGGER.info("\ntestGetClient started...");
		
		Client client = new Client("hello","world","john","wick","copacul de langa parc","800555555",400288,"cluj","romaina");
		
		LOGGER.info("perform get...");
		Client newClient = userRepo.getClient(1);
		
		LOGGER.info("test if the username matches...");
		assertEquals(client.getUsername(), newClient.getUsername());
		LOGGER.info("test if the password matches...");
		assertEquals(client.getPassword(), newClient.getPassword());
		LOGGER.info("test if the first name matches...");
		assertEquals(client.getFirstName(), newClient.getFirstName());
		LOGGER.info("test if the last name matches...");
		assertEquals(client.getLastName(), newClient.getLastName());
		LOGGER.info("test if the address matches...");
		assertEquals(client.getAddress(), newClient.getAddress());
		LOGGER.info("test if the phone number matches...");
		assertEquals(client.getPhoneNumber(), newClient.getPhoneNumber());
		LOGGER.info("test if the zipcode matches...");
		assertEquals(client.getZipCode(), newClient.getZipCode());
		LOGGER.info("test if the city matches...");
		assertEquals(client.getCity(), newClient.getCity());
		LOGGER.info("test if the county matches...");
		assertEquals(client.getCounty(), newClient.getCounty());
		
		LOGGER.info("testGetClient finished...\n");
	}
	
	/**
	 * test getAdminById
	 */
	@Test
	public void testGetAdminByID() {
		LOGGER.info("\ntestGetAdmin started...");
		Admin admin = null;
		
		LOGGER.info("perform get...");
		admin = userRepo.getAdmin(4);
		
		LOGGER.info("test if the username matches...");
		assertEquals(admin.getUsername(), "a");
		LOGGER.info("test if the password matches...");
		assertEquals(admin.getPassword(), "a");
		LOGGER.info("test if the first name matches...");
		assertEquals(admin.getFirstName(),"a");
		LOGGER.info("test if the last name matches...");
		assertEquals(admin.getLastName(), "a");
		
	}
	
	@Test
	public void testGetCashier() {
	LOGGER.info("\ntestGetCashierstarted...");
	Cashier cashier= new Cashier("AnaP22", "ana22", "Ana", "Pop");

	LOGGER.info("perform get...");
	Cashier newCashier = userRepo.getCashier(81);

	LOGGER.info("test if the username matches...");
	assertEquals(newCashier.getUsername(), cashier.getUsername());
	LOGGER.info("test if the password matches...");
	assertEquals(newCashier.getPassword(), cashier.getPassword());
	LOGGER.info("test if the first name matches...");
	assertEquals(newCashier.getFirstName(), cashier.getFirstName());
	LOGGER.info("test if the last name matches...");
	assertEquals(newCashier.getLastName(), cashier.getLastName());

	LOGGER.info("testGetCashier finished...\n");
	}


}
