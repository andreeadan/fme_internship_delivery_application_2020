package ro.fme.internship.delivery.repository.test.FolderRepositoryTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.sql.Date;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Folder;
import ro.fme.internship.delivery.model.RelationFolderInFolder;
import ro.fme.internship.delivery.repository.implementation.FolderRepository;
import ro.fme.internship.delivery.repository.implementation.RelationFolderInFolderRepository;

public class FolderInFolderRepositoryTest {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FolderInFolderRepositoryTest.class);
	Folder sourceFolder, targetFolder;
	RelationFolderInFolderRepository folderInFolderRelationRepo;
	RelationFolderInFolder folderInFolderRelation;
	FolderRepository folderRepo;

	/**
	 * set data for tests
	 */
	@Before
	public void setup() {
		LOGGER.info("\ncreate the folder-in-folder repository object...");
		folderInFolderRelationRepo = new RelationFolderInFolderRepository();
		
		LOGGER.info("create the folder repository object...");
		folderRepo = new FolderRepository();
		
		LOGGER.info("create 2 folder objects...");
		Date creationDate = new Date(2020, 8, 9);
		Date modifyDate = new Date(2020, 8, 11);
		
		sourceFolder = new Folder("8-9", creationDate, "owner_1", 3, modifyDate, "intern");
		LOGGER.info("add folder 1 to database...");
		folderRepo.insertFolder(sourceFolder);
		
		targetFolder = new Folder("2020", creationDate, "owner_2", 3, modifyDate, "intern");
		LOGGER.info("add folder 2 to database...");
		folderRepo.insertFolder(targetFolder);
	}
	
	/**
	 * test insert into the repository
	 */
	@Test
	public void testinsertRelationFolderInFolder() {
		LOGGER.info("\ntestinsertRelationFolderInFolder started...");
		int initialSize = folderInFolderRelationRepo.getAllRelationFolderInFolder().size();
		System.out.println("target year from tests:"+targetFolder.getCreation_date().getYear());
		LOGGER.info("insert started...");
		folderInFolderRelationRepo.insertRelationFolderInFolder(sourceFolder, targetFolder);
		
		LOGGER.info("check if the insert registered...");
		assertNotEquals(initialSize, folderInFolderRelationRepo.getAllRelationFolderInFolder().size());
		System.out.println(folderInFolderRelationRepo.getAllRelationFolderInFolder().size()+1);
		assertEquals(initialSize+1, folderInFolderRelationRepo.getAllRelationFolderInFolder().size());
		
		LOGGER.info("testinsertRelationFolderInFolder finished...\n");
	}
}
