package ro.fme.internship.delivery.repository.test.PackageInvoiceRepositoyTest;

import static org.junit.Assert.*;

import java.sql.Date;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.PackageInvoice;
import ro.fme.internship.delivery.repository.implementation.PackageInvoiceRepository;

public class GetPackageInvoiceRepositoryFromDBTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(PackageInvoiceRepositoryTest.class);
	private static PackageInvoiceRepository packRepo;
	
	@BeforeClass
	public static void setUp() {
		packRepo = new PackageInvoiceRepository();
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void test() {

		Date d1 = new Date(2020, 9, 9);
		Date d2 = new Date(2020, 8, 9);
		Date d3 = new Date(2020, 10, 10);
		
		LOGGER.info("Creating a new object of type PackageInvoice...");
		PackageInvoice pack = new PackageInvoice(20, 67, 68, "description2", 100, d1, "delivered", 100, 1, d2, d3, "books", "Cluj-Napoca", 61);
		pack.setInvoice_id(6);
		
		LOGGER.info("Comparing the newly created object with the one retrieved from the database by using the repository...");
		assertEquals(pack.toString(),packRepo.getPackageInvoiceById(6).toString());
		
		LOGGER.info("Verify that the object retrieved from the DB is not null...");
		assertNotEquals(packRepo.getPackageInvoiceById(6), null);
	}

}
