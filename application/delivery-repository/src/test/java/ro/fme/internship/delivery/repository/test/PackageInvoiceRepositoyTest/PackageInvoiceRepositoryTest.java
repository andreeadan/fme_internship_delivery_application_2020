package ro.fme.internship.delivery.repository.test.PackageInvoiceRepositoyTest;

import static org.junit.Assert.*;


import java.sql.Date;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.PackageInvoice;
import ro.fme.internship.delivery.repository.implementation.PackageInvoiceRepository;

public class PackageInvoiceRepositoryTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(PackageInvoiceRepositoryTest.class);
	private static PackageInvoiceRepository packRepo;

	/**
	 * set data for tests
	 */
	@BeforeClass
	public static void setUp() {
		packRepo = new PackageInvoiceRepository();
	}

	/**
	 * tests if a packageInvoice was added in PackageInvoice table
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void testInsertPackageInvoice() {
		LOGGER.info("\ntestInsertPackageInvoice started...");

		Date d1 = new Date(2020, 9, 9);
		Date d2 = new Date(2020, 8, 9);
		Date d3 = new Date(2020, 10, 10);
		PackageInvoice pack = new PackageInvoice(14, 67, 68, "description2", (long) 100, d1, "delivered", (long) 100, 1, d2, d3, "books", "Cluj-Napoca", 61);

		int listSize = packRepo.getPackageInvoices().size();

		LOGGER.info("insert the created package into the repository...");
		packRepo.insertPackageInvoice(pack);

		LOGGER.info("test packRepo size change...");
		assertTrue(packRepo.getPackageInvoices().size() == (listSize + 1));

		LOGGER.info("test the new packeageinvoice's description equals a good value...");
		assertTrue(packRepo.getPackageInvoices().get(listSize).getOrder_description().equals("description2"));

		LOGGER.info("testInsertPackageInvoice finished...\n");
	}
	
	/**
	 * tests if a packageInvoice was updated in PackageInvoice table
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void testUpdatePackageInvoice() {
		LOGGER.info("\ntestUpdatePackageInvoice started...");
		Date d1 = new Date(2020, 9, 9);
		Date d2 = new Date(2020, 8, 9);
		Date d3 = new Date(2020, 10, 10);
		PackageInvoice oldPack = new PackageInvoice(14, 36, 37, "description2", (long) 100, d1, "delivered", (long) 100, 1, d2, d3, "books", "Cluj-Napoca", 36);
		PackageInvoice updatedPack = new PackageInvoice(15, 36, 37, "description2", (long) 200, d2, "processing", (long) 100, 1, d3, d1, "books", "Cluj-Napoca", 36);

		LOGGER.info("insert the created package into the repository...");
		packRepo.insertPackageInvoice(oldPack);
		
		LOGGER.info("performing the update...");
		packRepo.updatePackageInvoice(updatedPack, oldPack.getInvoice_id());
		
		LOGGER.info("test the return id update...");
		assertNotEquals(oldPack.getReturn_id(), updatedPack.getReturn_id());
		LOGGER.info("test the order cost update...");
		assertNotEquals(oldPack.getOrder_cost(), updatedPack.getOrder_cost());
		LOGGER.info("test the creation date update...");
		assertNotEquals(oldPack.getCreation_date(), updatedPack.getCreation_date());
		LOGGER.info("test the delivery date update...");
		assertNotEquals(oldPack.getDelivery_date(), updatedPack.getDelivery_date());
		LOGGER.info("test the modify date update...");
		assertNotEquals(oldPack.getModify_date(), updatedPack.getModify_date());
		LOGGER.info("test the order status update...");
		assertNotEquals(oldPack.getOrder_cost(), updatedPack.getOrder_cost());
		
		LOGGER.info("testUpdatePackageInvoice finished...\n");
	}
}
