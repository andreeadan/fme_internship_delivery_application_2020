import org.junit.Test;
import static org.junit.Assert.*;

public class TestCasesTemplate {
	
	/*
	 *  SMALL ANNOTATIONS TUTORIAL
	 *  	@Before & @After
	 *  	use fore methods that run before & after each test case method
	 *  
	 *  	@BeforeClass & @fterClass
	 *  	use for methods that run before & after all test case methods
	 *  	usually used for expensive initializations of variables that do not make sense to create and kill after each test
	 */
	
	@Test
	public void test() {
		String str = "this is a test";
		assertEquals("this is a test", str);
	}

}
