package ro.fme.internship.delivery.repository;

import java.util.ArrayList;

import ro.fme.internship.delivery.model.Folder;

public interface IFolderRepository {

	public int insertFolder(Folder folder);

	public void updateFolder(Folder folder, int id);

	public ArrayList<Folder> getAllFolders();

	public int findTargetFolderId(Folder folder);

	public int findSourceFolderId(Folder folder);

	public Folder getFolderByName(String name);

	public Folder getFolderWithId(int id);

	public ArrayList<Folder> getAllChildren(int parent_id);

	public ArrayList<Folder> getAllYearFolders();

}
