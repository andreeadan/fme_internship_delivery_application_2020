package ro.fme.internship.delivery.repository;

import java.util.ArrayList;

import ro.fme.internship.delivery.model.Route;

public interface IRouteRepository {

	public int insertRoute(Route route);

	public void updateRoute(Route route, int number);

	public Route getRouteByID(int id);

	public ArrayList<Route> getAllRoutes();

	public ArrayList<Route> getAllRoutesByDriverID(int idDriver);
}

