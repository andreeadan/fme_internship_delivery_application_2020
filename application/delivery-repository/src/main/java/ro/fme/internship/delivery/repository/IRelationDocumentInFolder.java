package ro.fme.internship.delivery.repository;

import java.util.ArrayList;

import ro.fme.internship.delivery.model.Folder;
import ro.fme.internship.delivery.model.PackageInvoice;
import ro.fme.internship.delivery.model.RelationDocumentInFolder;

public interface IRelationDocumentInFolder {

	public void insertDocumentInFolder(PackageInvoice documentSource, Folder folderSource);

	ArrayList<PackageInvoice> getInvoicesOfTargetFolder(Folder targetFolder);

	ArrayList<RelationDocumentInFolder> getDocumentsOfTargetFolder(Folder targetFolder);

	ArrayList<RelationDocumentInFolder> getAllRelationDocumentInFolder();

}
