package ro.fme.internship.delivery.repository.implementation;

import static util.QueryHelperFolder.INSERT_RELATION_DOCUMENT_IN_FOLDER_STATEMENT;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.database.connection.DBConnection;
import ro.fme.internship.delivery.model.Folder;
import ro.fme.internship.delivery.model.PackageInvoice;
import ro.fme.internship.delivery.model.RelationDocumentInFolder;
import ro.fme.internship.delivery.model.RelationFolderInFolder;
import ro.fme.internship.delivery.repository.IRelationDocumentInFolder;

public class RelationDocumentInFolderRepository implements IRelationDocumentInFolder {

	private static final Connection DB_CONNECTION = DBConnection.getConnection();

	private static final Logger LOGGER = LoggerFactory.getLogger(RelationDocumentInFolderRepository.class);
	private PackageInvoiceRepository packageInvoiceRepository = new PackageInvoiceRepository();
	private FolderRepository folderRepository = new FolderRepository();

	public RelationDocumentInFolderRepository() {
	}

	/**
	 * Insert a DocumentInFolder relation in the database
	 * 
	 * @param documentSource: PackageInvoice, folderSource: Folder
	 */
	@Override
	public void insertDocumentInFolder(PackageInvoice documentSource, Folder folderSource) {
		LOGGER.debug("Enter insertDocumentInFolder in repository");
		int packageId = packageInvoiceRepository.getInvoiceIdByContentLocation(documentSource.getContent_location());
		int folderId = folderRepository.findTargetFolderId(folderSource);
		PreparedStatement prepSt = null;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = DB_CONNECTION.prepareStatement(INSERT_RELATION_DOCUMENT_IN_FOLDER_STATEMENT);
			prepSt.setInt(1, packageId);
			prepSt.setInt(2, folderId);
			LOGGER.debug("Executing update...");
			int resp = prepSt.executeUpdate();
			if (resp == 1) {
				LOGGER.debug("New relation document in folder was created!");
				LOGGER.debug("Leave insertDocumentInFolder in repository");
			} else {
				LOGGER.debug("Relation document in folder not created!");
			}
		} catch (SQLException e) {
			LOGGER.error("Error in RelationDocumentInFolderRepository, insertDocumentInFolder ", e);
		}

	}

	/**
	 * Get all DocumentInFolder relations from the database
	 * 
	 * @return relations: ArrayList<RelationDocumentInFolder>
	 */
	@Override
	public ArrayList<RelationDocumentInFolder> getAllRelationDocumentInFolder() {
		LOGGER.debug("Enter getAllRelationDocumentInFolder in repository");
		ArrayList<RelationDocumentInFolder> relations = new ArrayList<>();

		LOGGER.debug("Creating query...");
		String sql = "Select * from RELATION_DOCUMENT_IN_FOLDER";
		try {
			LOGGER.debug("Prepare statement...");
			PreparedStatement prepSt = DB_CONNECTION.prepareStatement(sql);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery(sql);
			while (rs.next()) {
				int sourceFolderID = rs.getInt("SOURCE_ID_FK");
				int targetFolderID = rs.getInt("TARGET_ID_FK");

				RelationDocumentInFolder relation = new RelationDocumentInFolder(sourceFolderID, targetFolderID);
				relation.setRelationId(rs.getInt("RELATION_ID_PK"));
				relations.add(relation);
			}
		} catch (SQLException e) {
			LOGGER.error("Error in RelationDocumentInFolderRepository, getAllRelationDocumentInFolder ", e);
		}
		LOGGER.debug("Leave getAllRelationDocumentInFolder in repository");
		return relations;
	}

	/**
	 * Get all relations that are linked to a given target folder
	 * 
	 * @param targetFolder: Folder
	 * @return relationsByTarget: ArrayList<RelationDocumentInFolder>
	 */
	@Override
	public ArrayList<RelationDocumentInFolder> getDocumentsOfTargetFolder(Folder targetFolder) {
		LOGGER.debug("Enter getDocumentsOfTargetFolder in repository");
		ArrayList<RelationDocumentInFolder> relationsRepo = getAllRelationDocumentInFolder();
		ArrayList<RelationDocumentInFolder> relationsByTarget = new ArrayList<>();
		try {
			LOGGER.debug("Parsing through all relations...");
			for (RelationDocumentInFolder relation : relationsRepo) {
				if (relation.getFolderId() == targetFolder.getFolder_id())
					relationsByTarget.add(relation);
			}
		} catch (Exception e) {
			LOGGER.error("Error in RelationDocumentInFolderRepository, getDocumentsOfTargetFolder ", e);
		}
		LOGGER.debug("Leave getDocumentsOfTargetFolder in repository");
		return relationsByTarget;
	}

	/**
	 * Get all packageInvoices that are linked to a given target folder
	 * 
	 * @param targetFolder: Folder
	 * @return packageInvoicesByTarget: ArrayList<PackageInvoice>
	 */
	@Override
	public ArrayList<PackageInvoice> getInvoicesOfTargetFolder(Folder targetFolder) {
		LOGGER.debug("Enter getInvoicesOfTargetFolder in repository");
		ArrayList<RelationDocumentInFolder> relationsRepo = getDocumentsOfTargetFolder(targetFolder);
		ArrayList<PackageInvoice> invoices = new ArrayList<>();
		try {
			LOGGER.debug("Parsing through all relations...");
			for (RelationDocumentInFolder relation : relationsRepo) {
				int docId = relation.getDocumentId();
				PackageInvoice myinvoice = packageInvoiceRepository.getPackageInvoiceById(docId);
				invoices.add(myinvoice);

			}
			
			return invoices;
		} catch (Exception e) {
			LOGGER.error("Error in RelationDocumentInFolderRepository, getDocumentsOfTargetFolder ", e);
		}
		LOGGER.debug("Leave getInvoicesOfTargetFolder in repository");

		return invoices;
	}

}
