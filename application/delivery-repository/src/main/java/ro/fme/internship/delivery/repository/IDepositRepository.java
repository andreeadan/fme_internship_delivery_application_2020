package ro.fme.internship.delivery.repository;

import java.util.ArrayList;

import ro.fme.internship.delivery.model.Deposit;

public interface IDepositRepository {

	public Deposit getDeposit(int id);

	public int insertDeposit(Deposit d);

	public int updateDeposit(Deposit d, int number);
	
	public ArrayList<Deposit> getAllDeposits();
	
	public int getDepositIdByCounty(String county);

}
