package ro.fme.internship.delivery.repository.implementation;

import static util.QueryHelperDeposit.getDepositByIdStatement;
import static util.QueryHelperDeposit.getDepositStatement;
import static util.QueryHelperDeposit.updateDepositStatement;
import static util.QueryHelperDeposit.getAllDepositsStatement;
import static util.QueryHelperDeposit.getDepositByCountyStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.database.connection.DBConnection;
import ro.fme.internship.delivery.model.Cashier;
import ro.fme.internship.delivery.model.Deposit;
import ro.fme.internship.delivery.repository.IDepositRepository;
import util.QueryHelperCashier;

public class DepositRepository implements IDepositRepository {

	private static final Connection DB_CONNECTION = DBConnection.getConnection();
	private static final Logger LOGGER = LoggerFactory.getLogger(DepositRepository.class);
	private PreparedStatement prepSt = null;

	public DepositRepository() {
	}

	/**
	 * Get a deposit based on a given id
	 * 
	 * @param id: integer
	 * @return deposit: Deposit
	 */
	@Override
	public Deposit getDeposit(int id) {
		LOGGER.debug("Enter getDeposit in repository");
		Deposit deposit = null;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = getDepositByIdStatement(id, DB_CONNECTION);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery();

			while (rs.next()) {
				String county = rs.getString("county");
				String address = rs.getString("address");
				String status = rs.getString("status");
				int myid = rs.getInt("deposit_id");
				deposit = new Deposit(county, address, status);
				deposit.setDepositID(myid);
				LOGGER.debug("Deposit was found!");
			}
		} catch (SQLException e) {
			LOGGER.error("Error in DepositRepository, getDeposit " + e);
			System.out.println(e.getMessage());
		}
		LOGGER.debug("Leave getDeposit  in repository");
		return deposit;
	}

	/**
	 * Insert a deposit in the database
	 * 
	 * @param deposit: Deposit
	 * @return response: integer
	 */
	@Override
	public int insertDeposit(Deposit deposit) {
		LOGGER.debug("Enter insertDeposit  in repository");
		int response = 0;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = getDepositStatement(deposit, DB_CONNECTION);
			LOGGER.debug("Executing update...");
			response = prepSt.executeUpdate();
			if (response == 1) {
				LOGGER.debug("Deposit was successfully created!");
				return response;
			} else {
				LOGGER.debug("There was a problem with the deposit creation!");
				return response;
			}
		} catch (SQLException e) {
			LOGGER.error("Error in DepositRepository, insertDeposit " + e);
		}
		LOGGER.debug("Leave insertDeposit  in repository");
		return response;
	}

	/**
	 * Update a deposit in the database
	 * 
	 * @param deposit: Deposit, id: integer
	 * @return response: integer
	 */
	@Override
	public int updateDeposit(Deposit deposit, int id) {
		LOGGER.debug("Enter updateDeposit in repository");
		int response = 0;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = updateDepositStatement(deposit, id, DB_CONNECTION);
			LOGGER.debug("Executing update...");
			response = prepSt.executeUpdate();
			if (response == 1) {
				LOGGER.debug("Deposit was successfully updated!");
				return response;
			} else {
				LOGGER.debug("There was a problem with the deposit update!");
				return response;
			}
		} catch (SQLException e) {
			LOGGER.error("Error in DepositRepository, updateDeposit " + e);
		}
		LOGGER.debug("Leave updateDeposit in repository");
		return response;
	}

	/**
	 * Get all deposits from database
	 */
	/**
	 * @return deposits: ArrayList<Deposit>
	 */
	@Override
	public ArrayList<Deposit> getAllDeposits() {
		LOGGER.debug("Enter getAllDeposits in repository");
		ArrayList<Deposit> deposits = new ArrayList<Deposit>();
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = getAllDepositsStatement(DB_CONNECTION);
			LOGGER.debug("Executing update...");
			ResultSet result = prepSt.executeQuery();
			while (result.next()) {
				String county = result.getString("county");
				String address = result.getString("address");
				String status = result.getString("status");
				Deposit deposit = new Deposit(county, address, status);
				int depositId = result.getInt("deposit_ID");
				deposit.setDepositID(depositId);
				deposits.add(deposit);
			}
		} catch (SQLException e) {
			LOGGER.error("Error in DepositRepository, getAllDeposits " + e);
		}
		LOGGER.debug("Leave getAllDeposits in repository");
		return deposits;
	}

	/**
	 * get deposit id by county
	 */
	/**
	 * @param couny:String
	 * @return depositId:int
	 */
	@Override
	public int getDepositIdByCounty(String county) {
		LOGGER.debug("Enter getDepositIdByCounty");
		int depositId = 0;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = getDepositByCountyStatement(county,DB_CONNECTION);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery();
			while(rs.next()) {
				depositId = rs.getInt("DEPOSIT_ID");
			}
		}catch(SQLException e) {
			LOGGER.error("Error in DepositRepository, getDepositIdByCounty " + e);
		}
		LOGGER.debug("Leave getDepositIdByCounty");
		return depositId;
	}
}

