package ro.fme.internship.delivery.database.connection;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DBConnection {

	private static Connection connection;

	private static final Logger LOGGER = LoggerFactory.getLogger(DBConnection.class);

	/**
	 * Creates connection if it does not already exist
	 * 
	 * @return connection: Connection
	 */
	public static Connection getConnection() {
		LOGGER.debug("Enter getConnection");
		LOGGER.debug("Create configurations...");
		Properties config = new Properties();

		// get properties
		try {
			// FileInputStream fis = new
			// FileInputStream("C:\\Users\\Roronoa\\Desktop\\test_2\\package-delivery-project-javainternship2020\\application\\delivery-repository\\resources\\dbConfig.properties");
			LOGGER.debug("Get  config file location...");
			InputStream fis = getFileFromResourceAsStream("dbConfig.properties");
			LOGGER.debug("Load config file...");
			config.load(fis);
			LOGGER.debug("Properties loaded");
		} catch (IOException e1) {
			System.out.println(e1.getMessage());
			LOGGER.error("ERROR", e1.getMessage());
		}

		try {
			// connection to database
			if (connection == null) {
				Class.forName("oracle.jdbc.driver.OracleDriver");
				connection = DriverManager.getConnection(
						config.getProperty("hostname") + ":" + config.getProperty("port") + ":orcl",
						config.getProperty("username"), config.getProperty("password"));
				LOGGER.debug("Connection created");

			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			LOGGER.error(String.format("SQL State: %s%n%s", e.getSQLState(), e.getMessage()));
		} catch (Exception e) {
			System.out.println(e.getMessage());
			LOGGER.error("ERROR", e.getMessage());
		}

		return connection;
	}

	/**
	 * Gets a file from the resource folder and turns it into a stream
	 * 
	 * @param fileName: String
	 * @return inputStream: InputStream
	 */
	private static InputStream getFileFromResourceAsStream(String fileName) {
		LOGGER.debug("Enter getFileFromResourceAsStream");
		// The class loader that loaded the class
		ClassLoader classLoader = DBConnection.class.getClassLoader();
		InputStream inputStream = classLoader.getResourceAsStream(fileName);

		// the stream holding the file content
		if (inputStream == null) {
			throw new IllegalArgumentException("file not found! " + fileName);
		} else {
			LOGGER.debug("Leave getFileFromResourceAsStream");
			return inputStream;
		}
	}

	/**
	 * Close the connection to the database
	 */
	public static void closeConnection() {
		LOGGER.debug("Enter closeConnection");
		if (connection != null) {
			try {
				connection.close();
				LOGGER.debug("Connection closed");
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				LOGGER.error("ERROR", e);
			}
		}
	}

}
