package ro.fme.internship.delivery.repository.implementation;

import static util.QueryHelperAdmin.deleteAdminStatement;
import static util.QueryHelperAdmin.getAdminByIdStatement;
import static util.QueryHelperAdmin.getAdminStatement;
import static util.QueryHelperAdmin.getAdminUserByUsernameAndPassword;
import static util.QueryHelperAdmin.updateAdminStatement;
import static util.QueryHelperAdmin.updateAdminUserStatement;
import static util.QueryHelperCashier.deleteCashierStatement;
import static util.QueryHelperCashier.getCashierByIdStatement;
import static util.QueryHelperCashier.getCashierStatement;
import static util.QueryHelperCashier.getCashierUserByUsernameAndPassword;
import static util.QueryHelperCashier.updateCashierStatement;
import static util.QueryHelperCashier.updateCashierUserStatement;
import static util.QueryHelperClient.geClientIdByUsernameStatement;
import static util.QueryHelperClient.deleteClientStatement;
import static util.QueryHelperClient.getClientStatement;
import static util.QueryHelperClient.getClientUserByUsernameAndPassword;
import static util.QueryHelperClient.updateClientStatement;
import static util.QueryHelperClient.updateClientUserStatement;
import static util.QueryHelperDriver.deleteDriverStatement;
import static util.QueryHelperDriver.getDriverByIdStatement;
import static util.QueryHelperDriver.getDriverStatement;
import static util.QueryHelperDriver.updateDriverUserStatement;
import static util.QueryHelperDriver.getDriverUserByUsernameAndPassword;
import static util.QueryHelperDriver.updateCar;
import static util.QueryHelperDriver.updateDriverStatement;
import static util.TableNameQueryHelper.valueOf;
import static util.QueryHelperDriver.getDriverIdByUsername;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.database.connection.DBConnection;
import ro.fme.internship.delivery.model.Admin;
import ro.fme.internship.delivery.model.Cashier;
import ro.fme.internship.delivery.model.Client;
import ro.fme.internship.delivery.model.Driver;
import ro.fme.internship.delivery.model.PackageInvoice;
import ro.fme.internship.delivery.model.User;
import ro.fme.internship.delivery.repository.IUserRepository;
import util.QueryHelperAdmin;
import util.QueryHelperCashier;
import util.QueryHelperClient;
import util.QueryHelperDriver;

public class UserRepository implements IUserRepository {

	private static final Connection DB_CONNECTION = DBConnection.getConnection();
	private static final Logger LOGGER = LoggerFactory.getLogger(UserRepository.class);
	private PreparedStatement prepSt = null;
	private PreparedStatement prepStAdmin = null;
	private PreparedStatement prepStClient = null;
	private PreparedStatement prepStCashier = null;
	private PreparedStatement prepStDriver = null;

	/**
	 * Insert a user in the database
	 * 
	 * @param user: User
	 * @return response: integer
	 */
	@Override
	public int insertUser(User user) {
		LOGGER.debug("Enter insertUser");
		int response = 0;

		String password = user.getPassword();
		// String encryptedPass = encryptPass(password);
		// user.setPassword(encryptedPass);

		try {
			LOGGER.debug("Prepare statement based on user type...");
			if (user instanceof Driver)
				prepSt = getDriverStatement((Driver) user, DB_CONNECTION);

			else if (user instanceof Client)
				prepSt = getClientStatement((Client) user, DB_CONNECTION);

			else if (user instanceof Admin)
				prepSt = getAdminStatement((Admin) user, DB_CONNECTION);

			else if (user instanceof Cashier)
				prepSt = getCashierStatement((Cashier) user, DB_CONNECTION);
			LOGGER.debug("Executing update...");
			response = prepSt.executeUpdate();
			if (response == 1) {
				LOGGER.debug("User was successfully created!");
				return response;
			} else {
				LOGGER.debug("There was a problem with the user creation!");
				return response;
			}
		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, insertUser ", e);
			return response;
		}
	}

	/**
	 * Method used for password encryption
	 * 
	 * @param password
	 * @return
	 */
//	public static String encryptPass(String password) {
//		String hash = "35454B055CC325EA1AF2126E27707052";
//		MessageDigest md = null;
//		try {
//			md = MessageDigest.getInstance("MD5");
//		} catch (NoSuchAlgorithmException e) {
//			e.printStackTrace();
//		}
//		md.update(password.getBytes());
//		byte[] digest = md.digest();
//		String myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
//		System.out.println(myHash);
//		return myHash;
//	}

	/**
	 * Update a user in the database
	 * 
	 * @param user: User, username: String
	 * @return response: integer
	 */
	@Override
	public int updateUser(User user, String username) {
		LOGGER.debug("Enter updateUser");
		int response = 0;
		try {
			LOGGER.debug("Prepare statement based on user type...");
			if (user instanceof Driver)
				prepSt = updateDriverStatement((Driver) user, username, DB_CONNECTION);
			else if (user instanceof Client)
				prepSt = updateClientStatement((Client) user, username, DB_CONNECTION);
			else if (user instanceof Admin)
				prepSt = updateAdminStatement((Admin) user, username, DB_CONNECTION);
			else if (user instanceof Cashier)
				prepSt = updateCashierStatement((Cashier) user, username, DB_CONNECTION);
			LOGGER.debug("Executing update...");
			response = prepSt.executeUpdate();
			if (response == 1) {
				LOGGER.debug("User was successfully created!");
				return response;
			} else {
				LOGGER.debug("There was a problem with the user creation!");
				return response;
			}
		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, updateUser ", e);
			return response;
		}
	}

	/**
	 * Update a driver's car in the database
	 * 
	 * @param newCar: String, username: String
	 * @return response: integer
	 */
	@Override
	public int updateDriverCar(String newCar, String username) {
		LOGGER.debug("Enter updateDriverCar");
		int response = 0;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = updateCar(newCar, username, DB_CONNECTION);
			LOGGER.debug("Executing update...");
			response = prepSt.executeUpdate();
			if (response == 1) {
				LOGGER.debug("Car was successfully updated!");
				return response;
			} else {
				LOGGER.debug("There was a problem with the car update!");
				return response;
			}
		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, updateDriverCar ", e);
			return response;
		}
	}

	/**
	 * Delete a user in the database
	 * 
	 * @param user: User
	 */
	@Override
	public int deleteUser(User user) {
		LOGGER.debug("Enter deleteUser");
		PreparedStatement prepSt = null;
		int response = 0;
		try {
			LOGGER.debug("Prepare statement based on user type...");
			if (user instanceof Admin)
				prepSt = deleteAdminStatement((Admin) user, DB_CONNECTION);

			else if (user instanceof Client)
				prepSt = deleteClientStatement((Client) user, DB_CONNECTION);

			else if (user instanceof Driver)
				prepSt = deleteDriverStatement((Driver) user, DB_CONNECTION);

			else if (user instanceof Cashier)
				prepSt = deleteCashierStatement((Cashier) user, DB_CONNECTION);
			LOGGER.debug("Executing update...");
			response = prepSt.executeUpdate();
			if (response == 1) {
				LOGGER.debug("User was successfully deleted!");
				return response;
			} else {
				LOGGER.debug("There was a problem with the user deletion!");
				return response;
			}
		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, deleteUser ", e);
			return response;
		}
	}

	/**
	 * Delete all records in a given table
	 * 
	 * @param tableName: String
	 */
	@Override
	public void deleteAllRecords(String tableName) {
		LOGGER.debug("Enter deleteAllRecords");
		PreparedStatement prepSt = null;
		LOGGER.debug("Creating the query...");
		String sql = valueOf(tableName).getQuery();
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = QueryHelperAdmin.deleteAllRecords(sql, DB_CONNECTION);
			LOGGER.debug("Executing update...");
			int response = prepSt.executeUpdate();
			if (response == 1)
				LOGGER.debug("All records were successfully deleted!");
			else
				LOGGER.debug("There was a problem with records' deletion");
		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, deleteAllRecords ", e);
		}
	}

	/**
	 * Get all admins from Admin table
	 * 
	 * @return admins: ArrayList<Admin>
	 */
	@Override
	public ArrayList<Admin> getAdminUsers() {
		LOGGER.debug("Enter getAdminUsers");
		ArrayList<Admin> admins = new ArrayList<>();
		LOGGER.debug("Creating the query...");
		String sql = "Select * from Admin";
		try {
			LOGGER.debug("Prepare statement...");
			PreparedStatement prepSt = DB_CONNECTION.prepareStatement(sql);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery(sql);

			while (rs.next()) {
				String username = rs.getString("username");
				String password = rs.getString("password");
				String firstName = rs.getString("firstname");
				String lastName = rs.getString("lastname");

				Admin admin = new Admin(username, password, firstName, lastName);

				admin.setAdminID(rs.getInt("admin_id"));
				admins.add(admin);
			}
		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, getAdminUsers ", e);
		}
		LOGGER.debug("Leave getAdminUsers");
		return admins;
	}

	/**
	 * Get all drivers from Driver table
	 * 
	 * @return drivers: ArrayList<Driver>
	 */
	@Override
	public ArrayList<Driver> getDriverUsers() {
		LOGGER.debug("Enter getDriverUsers");
		ArrayList<Driver> drivers = new ArrayList<>();
		LOGGER.debug("Creating the query...");
		String sql = "Select * from Driver";
		try {
			LOGGER.debug("Prepare statement...");
			PreparedStatement prepSt = DB_CONNECTION.prepareStatement(sql);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery(sql);

			while (rs.next()) {
				String username = rs.getString("username");
				String password = rs.getString("password");
				String firstName = rs.getString("firstname");
				String lastName = rs.getString("lastname");
				String car = rs.getString("car");
				Driver driver = new Driver(username, password, firstName, lastName, car);
				driver.setDriverID(rs.getInt("driver_id"));
				drivers.add(driver);
			}
		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, getDriverUsers ", e);
		}
		LOGGER.debug("Leave getDriverUsers");
		return drivers;
	}

	/**
	 * Get all clients from Client table
	 * 
	 * @return list of clients
	 */
	@Override
	public ArrayList<Client> getClientUsers() {
		LOGGER.debug("Enter getClientUsers");
		ArrayList<Client> clients = new ArrayList<>();
		LOGGER.debug("Creating the query...");
		String sql = "Select * from Client";
		try {
			LOGGER.debug("Prepare statement...");
			PreparedStatement prepSt = DB_CONNECTION.prepareStatement(sql);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery(sql);

			while (rs.next()) {
				String phone = rs.getString("PHONE_NUMBER");
				String username = rs.getString("username");
				String password = rs.getString("password");
				String firstName = rs.getString("firstname");
				String lastName = rs.getString("lastname");
				String address = rs.getString("address");
				int zipCode = rs.getInt("zipCode");
				String city = rs.getString("city");
				String county = rs.getString("county");
				

				Client client = new Client(username, password, firstName, lastName, address, phone, zipCode, city,
						county);
				client.setClientID(rs.getInt("client_id"));
				clients.add(client);
			}
		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, getClientUsers ", e);
		}
		LOGGER.debug("Leave getClientUsers");
		return clients;
	}

	/**
	 * Get all cashiers from Cashier table
	 * 
	 * @return cashiers: ArrayList<Cashier>
	 */
	@Override
	public ArrayList<Cashier> getCashierUsers() {
		LOGGER.debug("Enter getCashierUsers");
		ArrayList<Cashier> cashiers = new ArrayList<>();
		LOGGER.debug("Creating the query...");
		String sql = "Select * from Cashier";
		try {
			LOGGER.debug("Prepare statement...");
			PreparedStatement prepSt = DB_CONNECTION.prepareStatement(sql);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery(sql);

			while (rs.next()) {
				String username = rs.getString("username");
				String password = rs.getString("password");
				String firstName = rs.getString("firstname");
				String lastName = rs.getString("lastname");
				Cashier cashier = new Cashier(username, password, firstName, lastName);
				cashier.setCashierID(rs.getInt("cashier_id"));
				cashiers.add(cashier);
			}
		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, getCashierUsers ", e);
		}
		LOGGER.debug("Leave getCashierUsers");
		return cashiers;
	}

	/**
	 * Get a driver from the database with a given id
	 * 
	 * @param id: integer
	 * @return driver: Driver
	 */
	@Override
	public Driver getDriver(int id) {
		LOGGER.debug("Enter getDriver");
		Driver driver = null;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = getDriverByIdStatement(id, DB_CONNECTION);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery();

			while (rs.next()) {
				String username = rs.getString("username");
				String password = rs.getString("password");
				String firstname = rs.getString("firstname");
				String lastname = rs.getString("lastname");
				String car = rs.getString("car");
				int myid = rs.getInt("driver_id");
				driver = new Driver(username, password, firstname, lastname, car);
				driver.setDriverID(myid);
				LOGGER.debug("Driver was found!");
			}

		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, getDriver ", e);
			System.out.println(e.getMessage());
		}
		LOGGER.debug("Leave getDriver");
		return driver;
	}

	/**
	 * Get an admin from the database with a given id
	 * 
	 * @param id: integer
	 * @return admin: Admin
	 */
	@Override
	public Admin getAdmin(int id) {
		LOGGER.debug("Enter getAdmin");
		Admin admin = null;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = getAdminByIdStatement(id, DB_CONNECTION);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery();

			while (rs.next()) {
				String username = rs.getString("username");
				String password = rs.getString("password");
				String firstname = rs.getString("firstname");
				String lastname = rs.getString("lastname");
				admin = new Admin(username, password, firstname, lastname);
				admin.setAdminID(rs.getInt("admin_id"));
				LOGGER.debug("Admin was found!");
			}
		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, getAdmin ", e);
		}
		LOGGER.debug("Leave getAdmin");
		return admin;
	}

	/**
	 * Get a client from the database with a given id
	 * 
	 * @param id: integer
	 * @return client: Client
	 */
	@Override
	public Client getClient(int id) {
		LOGGER.debug("Enter getClient");
		Client client = null;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = QueryHelperClient.getClientByID(id, DB_CONNECTION);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery();

			while (rs.next()) {
				int idClient = rs.getInt("CLIENT_ID");
				String address = rs.getString("ADDRESS");
				String phone_number = rs.getString("PHONE_NUMBER");
				int zipcode = rs.getInt("ZIPCODE");
				String city = rs.getString("CITY");
				String county = rs.getString("COUNTY");
				String username = rs.getString("USERNAME");
				String password = rs.getString("PASSWORD");
				String first_name = rs.getString("FIRSTNAME");
				String last_name = rs.getString("LASTNAME");
				client = new Client(username, password, first_name, last_name, address, phone_number, zipcode, city,
						county);
				client.setClientID(idClient);
				LOGGER.debug("Client was found!");
			}
		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, getClient ", e);
		}
		LOGGER.debug("Leave getClient");
		return client;
	}

	/**
	 * Get a cashier from the database with a given id
	 * 
	 * @param id: integer
	 * @return cashier: Cashier
	 */
	@Override
	public Cashier getCashier(int id) {
		LOGGER.debug("Enter getCashier");
		Cashier cashier = null;
		try {
			LOGGER.debug("Prepare statement...");
			PreparedStatement prepSt = getCashierByIdStatement(id, DB_CONNECTION);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery();

			while (rs.next()) {
				String username = rs.getString("username");
				String password = rs.getString("password");
				String firstName = rs.getString("firstname");
				String lastName = rs.getString("lastname");
				cashier = new Cashier(username, password, firstName, lastName);
				cashier.setCashierID(id);
			}
		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, getCashier ", e);
		}
		LOGGER.debug("Leave getCashier");
		return cashier;
	}

	/**
	 * Get the from the database with a given username and password
	 * 
	 * @param username: String
	 * @param password: String
	 * @return user: User
	 */
	@Override
	public User getLoginUser(String username, String password) {
		LOGGER.debug("Enter getLoginUser");

		try {
			LOGGER.debug("Prepare statements...");
			prepStAdmin = getAdminUserByUsernameAndPassword(username, password, DB_CONNECTION);
			prepStClient = getClientUserByUsernameAndPassword(username, password, DB_CONNECTION);
			prepStCashier = getCashierUserByUsernameAndPassword(username, password, DB_CONNECTION);
			prepStDriver = getDriverUserByUsernameAndPassword(username, password, DB_CONNECTION);

			LOGGER.debug("Executing queries...");
			ResultSet adminrs = prepStAdmin.executeQuery();
			ResultSet clientrs = prepStClient.executeQuery();
			ResultSet cashierrs = prepStCashier.executeQuery();
			ResultSet driverrs = prepStDriver.executeQuery();

			if (cashierrs.next()) {

				String myusername = cashierrs.getString("username");
				String mypassword = cashierrs.getString("password");
				String myfirstname = cashierrs.getString("firstname");
				String mylastname = cashierrs.getString("lastname");

				Cashier cashier = new Cashier(myusername, mypassword, myfirstname, mylastname);
				cashier.setRole("cashier");
				cashier.setCashierID(cashierrs.getInt("cashier_id"));

				LOGGER.debug("Cashier was found!");
				return cashier;

			} else

			if (adminrs.next()) {

				String myusername = adminrs.getString("username");
				String mypassword = adminrs.getString("password");
				String myfirstname = adminrs.getString("firstname");
				String mylastname = adminrs.getString("lastname");

				Admin admin = new Admin(myusername, mypassword, myfirstname, mylastname);
				admin.setRole("admin");
				admin.setAdminID(adminrs.getInt("admin_id"));

				LOGGER.debug("Admin was found!");
				return admin;

			} else

			if (clientrs.next()) {

				String myusername = clientrs.getString("username");
				String mypassword = clientrs.getString("password");
				String myfirstname = clientrs.getString("firstname");
				String mylastname = clientrs.getString("lastname");
				String myaddress = clientrs.getString("address");
				String phoneNumber = clientrs.getString("phone_number");
				int zipCode = clientrs.getInt("zipcode");
				String city = clientrs.getString("city");
				String county = clientrs.getString("county");

				Client client = new Client(myusername, mypassword, myfirstname, mylastname, myaddress, phoneNumber,
						zipCode, city, county);
				client.setRole("client");
				client.setClientID(clientrs.getInt("client_id"));

				LOGGER.debug("Client was found!");
				return client;

			} else

			if (driverrs.next()) {

				String myusername = driverrs.getString("username");
				String mypassword = driverrs.getString("password");
				String myfirstname = driverrs.getString("firstname");
				String mylastname = driverrs.getString("lastname");
				String mycar = driverrs.getString("car");

				Driver driver = new Driver(myusername, mypassword, myfirstname, mylastname, mycar);
				driver.setRole("driver");
				driver.setDriverID(driverrs.getInt("driver_id"));

				LOGGER.debug("Driver was found!");
				return driver;
			}

		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, getLoginUser ", e);
			System.out.println(e.getMessage());
		}
		LOGGER.debug("Leave getLoginUser");
		return null;
	}

	/**
	 * Get the driver id from the database by a given username
	 * 
	 * @param username: String
	 * @return id_driver: int
	 */
	@Override
	public int getDriverIdByUsername(String username) {
		LOGGER.debug("Enter getDriverIdByUsername");
		int id_driver=-1;
		try {
			LOGGER.debug("Prepare statement...");
			PreparedStatement prepSt = QueryHelperDriver.getDriverIdByUsername(username, DB_CONNECTION);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery();
			if(rs.next()) {
			id_driver= rs.getInt("driver_id");
			return id_driver;
			}
		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, getDriverIdByUsername ", e);
		}
		LOGGER.debug("Leave getDriverIdByUsername");
		return id_driver;
	}


	@Override
	public int updateUserById(User user, int id) {
		LOGGER.debug("Enter updateUserById");
		int response = 0;
		try {
			LOGGER.debug("Prepare statement based on user type...");
			if (user instanceof Driver)
				prepSt = updateDriverUserStatement((Driver) user, id, DB_CONNECTION);
			else if (user instanceof Client)
				prepSt = updateClientUserStatement((Client) user, id, DB_CONNECTION);
			else if (user instanceof Admin)
				prepSt = updateAdminUserStatement((Admin) user, id, DB_CONNECTION);
			else if (user instanceof Cashier)
				prepSt = updateCashierUserStatement((Cashier) user, id, DB_CONNECTION);
			LOGGER.debug("Executing update...");
			response = prepSt.executeUpdate();
			if (response == 1) {
				LOGGER.debug("User was successfully created!");
				return response;
			} else {
				LOGGER.debug("There was a problem with the user update!");
				return response;
			}
		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, updateUserById ", e);
			return response;
		}
	}

	/**
	 * 
	 * Get the client id from the database by a given username
	 * 
	 * @param username: String
	 * @return id_driver: int
	 */

	@Override
	public int getClientIdByUsername(String username) {
		LOGGER.debug("Enter getClientIdByUsername");
		int id_client = -1;
		try {
			LOGGER.debug("Prepare statement...");
			PreparedStatement prepSt = geClientIdByUsernameStatement(username, DB_CONNECTION);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery();

			if (rs.next()) {
				id_client = rs.getInt("client_id");
				return id_client;
			}
		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, getClientIdByUsername ", e);
		}
		LOGGER.debug("Leave getClientIdByUsername");
		return id_client;

	}

	/**
	 * Get a client from the database with a given username
	 * 
	 * @param username: String
	 * @return client: Client
	 */
	@Override
	public Client getClientByUsername(String username) {
		LOGGER.debug("Enter getClientByUsername");
		Client client = null;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = QueryHelperClient.getClientUserByUsername(username, DB_CONNECTION);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery();

			while (rs.next()) {
				int idClient = rs.getInt("CLIENT_ID");
				String address = rs.getString("ADDRESS");
				String phone_number = rs.getString("PHONE_NUMBER");
				int zipcode = rs.getInt("ZIPCODE");
				String city = rs.getString("CITY");
				String county = rs.getString("COUNTY");
				;
				String password = rs.getString("PASSWORD");
				String first_name = rs.getString("FIRSTNAME");
				String last_name = rs.getString("LASTNAME");

				client = new Client(username, password, first_name, last_name, address, phone_number, zipcode, city,
						county);
				client.setClientID(idClient);
				LOGGER.debug("Client was found!");
			}
		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, getClientByUsername ", e);
		}
		LOGGER.debug("Leave getClientByUsername");
		return client;
	}

	/**
	 * Get a cashier from the database with a given username
	 * 
	 * @param username: String
	 * @return cashier: Cashier
	 */
	@Override
	public Cashier getCashierByUsername(String username) {
		LOGGER.debug("Enter getCashierByUsername");
		Cashier cashier = null;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = QueryHelperCashier.getCashierUserByUsername(username, DB_CONNECTION);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery();

			while (rs.next()) {
				String password = rs.getString("password");
				String firstname = rs.getString("firstname");
				String lastname = rs.getString("lastname");

				cashier = new Cashier(username, password, firstname, lastname);
				cashier.setCashierID(rs.getInt("cashier_id"));
				LOGGER.debug("Cashier was found!");
			}
		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, getCashierByUsername ", e);
		}
		LOGGER.debug("Leave getCashierByUsername");
		return cashier;
	}
	
	/**
	 * Get a admin from the database with a given username
	 * 
	 * @param username: String
	 * @return admin: Admin
	 */
	@Override
	public Admin getAdminByUsername(String username) {
		LOGGER.debug("Enter getAdminByUsername");
		Admin admin = null;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = QueryHelperAdmin.getAdminUserByUsername(username, DB_CONNECTION);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery();

			while (rs.next()) {
				String password = rs.getString("password");
				String firstname = rs.getString("firstname");
				String lastname = rs.getString("lastname");
				
				admin = new Admin(username, password, firstname, lastname);
				admin.setAdminID(rs.getInt("admin_id"));
				LOGGER.debug("Admin was found!");
			}
		} catch (SQLException e) {
			LOGGER.error("Error in UserRepository, getAdminByUsername ", e);
		}
		LOGGER.debug("Leave getAdminByUsername");
		return admin;
	}

}
