package ro.fme.internship.delivery.repository.implementation;

import static util.QueryHelperFolder.INSERT_RELATION_FOLDER_IN_FOLDER_STATEMENT;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.database.connection.DBConnection;
import ro.fme.internship.delivery.model.Folder;
import ro.fme.internship.delivery.model.RelationFolderInFolder;
import ro.fme.internship.delivery.repository.IRelationFolderInFolderRepository;

public class RelationFolderInFolderRepository implements IRelationFolderInFolderRepository {

	private static final Connection DB_CONNECTION = DBConnection.getConnection();
	private static final Logger LOGGER = LoggerFactory.getLogger(RelationFolderInFolderRepository.class);

	public RelationFolderInFolderRepository() {
	}

	/**
	 * Insert a FolderInFolder relation in the database
	 * 
	 * @param sourceFolder: Folder, targetFolder: Folder
	 * @return response: integer
	 */
	@Override
	public int insertRelationFolderInFolder(Folder sourceFolder, Folder targetFolder) {
		LOGGER.debug("Enter insertRelationFolderInFolder in repository");
		PreparedStatement prepSt = null;
		int response = 0;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = DB_CONNECTION.prepareStatement(INSERT_RELATION_FOLDER_IN_FOLDER_STATEMENT);
			FolderRepository folder_repo = new FolderRepository();

			int sourceId = folder_repo.findSourceFolderId(sourceFolder);
			int targetId = folder_repo.findTargetFolderId(targetFolder);
			prepSt.setInt(1, sourceId);
			prepSt.setInt(2, targetId);

			LOGGER.debug("Executing update...");
			response = prepSt.executeUpdate();
			if (response == 1) {
				LOGGER.debug("New relation folder in folder was created!");
				LOGGER.debug("Leave insertRelationFolderInFolder in repository");
				return response;
			} else {
				LOGGER.debug("Relation folder in folder not created!");
				return response;
			}
		} catch (SQLException e) {
			LOGGER.error("Error in RelationFolderInFolderRepository, insertRelationFolderInFolder ", e);
			return response;
		}
	}

	/**
	 * Get a folder's id from the database
	 * 
	 * @param folder: Folder
	 * @return id: Integer
	 */
	@Override
	public int getFolderId(Folder folder) {
		LOGGER.debug("Enter getFolderId in repository");
		PreparedStatement prepSt = null;
		int id = 0;
		try {
			LOGGER.debug("Creating query...");
			String sql = "Select FOLDER_ID from FOLDER where NAME=\'" + folder.getName() + "\'";
			LOGGER.debug("Prepare statement...");
			prepSt = DB_CONNECTION.prepareStatement(sql);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery(sql);
			while (rs.next()) {
				id = rs.getInt("folder_id");
			}
		} catch (SQLException e) {
			LOGGER.error("Error in RelationFolderInFolderRepository, getFolderId ", e);
		}
		LOGGER.debug("Leave getFolderId in repository");
		return id;
	}

	/**
	 * Get all FolderInFolder relations from the database
	 * 
	 * @return relations: ArrayList<RelationFolderInFolder>
	 */
	@Override
	public ArrayList<RelationFolderInFolder> getAllRelationFolderInFolder() {
		LOGGER.debug("Enter getAllRelationFolderInFolder in repository");
		ArrayList<RelationFolderInFolder> relations = new ArrayList<>();

		LOGGER.debug("Creating query...");
		String sql = "Select * from RELATION_FOLDER_IN_FOLDER";
		try {
			LOGGER.debug("Prepare statement...");
			PreparedStatement prepSt = DB_CONNECTION.prepareStatement(sql);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery(sql);
			while (rs.next()) {
				int sourceFolderID = rs.getInt("SOURCE_ID");
				int targetFolderID = rs.getInt("TARGET_ID");

				RelationFolderInFolder relation = new RelationFolderInFolder(sourceFolderID, targetFolderID);
				relation.setRelationID(rs.getInt("RELATION_ID"));
				relations.add(relation);
			}
		} catch (SQLException e) {
			LOGGER.error("Error in RelationFolderInFolderRepository, getAllRelationFolderInFolder ", e);
		}
		LOGGER.debug("Leave getAllRelationFolderInFolder in repository");
		return relations;
	}

	/**
	 * Get all relations that are linked to a given target folder
	 * 
	 * @param targetFolder: Folder
	 * @return relationsByTarget: ArrayList<RelationFolderInFolder>
	 */
	@Override
	public ArrayList<RelationFolderInFolder> getRelationsOfTargetFolder(Folder targetFolder) {
		LOGGER.debug("Enter getRelationsOfTargetFolder in repository");
		ArrayList<RelationFolderInFolder> relationsRepo = getAllRelationFolderInFolder();
		ArrayList<RelationFolderInFolder> relationsByTarget = new ArrayList<>();
		try {
			LOGGER.debug("Parsing through all relations...");
			for (RelationFolderInFolder relation : relationsRepo) {
				if (relation.getTargetFolderID() == targetFolder.getFolder_id())
					relationsByTarget.add(relation);
			}
		} catch (Exception e) {
			LOGGER.error("Error in RelationFolderInFolderRepository, getRelationsOfTargetFolder ", e);
		}
		LOGGER.debug("Leave getRelationsOfTargetFolder in repository");
		return relationsByTarget;
	}
}
