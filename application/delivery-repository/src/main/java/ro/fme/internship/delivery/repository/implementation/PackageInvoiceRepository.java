package ro.fme.internship.delivery.repository.implementation;

import static util.QueryHelperPackageInvoice.getPackageInvoiceByIdStatement;
import static util.QueryHelperPackageInvoice.getPackageInvoiceStatement;
import static util.QueryHelperPackageInvoice.selectAllPackageInvoices;
import static util.QueryHelperPackageInvoice.updatePackageInvoiceDetails;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.database.connection.DBConnection;
import ro.fme.internship.delivery.model.PackageInvoice;
import ro.fme.internship.delivery.repository.IPackageInvoiceRepository;

public class PackageInvoiceRepository implements IPackageInvoiceRepository {

	private static final Connection DB_CONNECTION = DBConnection.getConnection();
	private static final Logger LOGGER = LoggerFactory.getLogger(PackageInvoiceRepository.class);
	PreparedStatement prepSt = null;
	DateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");

	public PackageInvoiceRepository() {
	}

	/**
	 * Insert an invoice in the database
	 * 
	 * @param invoice: PackageInvoice
	 * @return response: integer
	 */
	@Override
	public int insertPackageInvoice(PackageInvoice invoice) {


		LOGGER.debug("Enter insertPackageInvoice in repository");

		int response = 0;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = getPackageInvoiceStatement(invoice, DB_CONNECTION);
			LOGGER.debug("Executing update...");
			response = prepSt.executeUpdate();
			if (response == 1) {
				LOGGER.debug("New package invoice was created!");
				LOGGER.debug("Leave insertPackageInvoice in repository");
				return response;
			} else {
				LOGGER.debug("Package invoice not created!");
				return response;
			}
		} catch (SQLException e) {
			LOGGER.error("Error in PackageInvoiceRepository, insertPackageInvoice ", e);
			return response;
		}
	}

	/**
	 * Update an invoice in the database
	 * 
	 * @param invoice: PackageInvoice, id: integer
	 * @return response: integer
	 */
	@Override
	public int updatePackageInvoice(PackageInvoice invoice, int id) {
		LOGGER.debug("Enter updatePackageInvoice in repository");
		int response = 0;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = updatePackageInvoiceDetails(invoice, id, DB_CONNECTION);
			LOGGER.debug("Executing update...");
			response = prepSt.executeUpdate();
			if (response == 1) {
				LOGGER.debug("Package invoice was updated!");
				return response;
			} else {
				LOGGER.debug("Package invoice was not updated!");
				return response;
			}
		} catch (SQLException e) {
			LOGGER.error("Error in PackageInvoiceRepository, updatePackageInvoice ", e);
		}
		LOGGER.debug("Leave updatePackageInvoice in repository");
		return response;
	}

	/**
	 * Get list of all invoices from PackageInvoice table
	 * 
	 * @return folders: ArrayList<PackageInvoice>
	 */
	@Override
	public ArrayList<PackageInvoice> getPackageInvoices() {

		LOGGER.debug("Enter getPackageInvoices in repository");

		ArrayList<PackageInvoice> packs = new ArrayList<>();
		try {
			LOGGER.debug("Prepare statement...");
			PreparedStatement prepSt = selectAllPackageInvoices(DB_CONNECTION);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery();

			while (rs.next()) {

				int returnId = rs.getInt("Return_id");
				int Exp_client_id = rs.getInt("Exp_client_id");
				int Dest_client_id = rs.getInt("Dest_client_id");
				String Order_description = rs.getString("Order_description");
				long Order_cost = rs.getLong("Order_cost");
				Date Creation_date = rs.getDate("Creation_date");
				String Order_status = rs.getString("Order_status");
				long Order_weight = rs.getLong("Order_weight");
				int Deposit_id = rs.getInt("Deposit_id");
				Date Delivery_date = rs.getDate("Delivery_date");
				Date Modify_date = rs.getDate("Modify_date");
				String Content_type = rs.getString("Content_type");
				String Content_location = rs.getString("Content_location");
				int cashierId = rs.getInt("Cashier_id");

				PackageInvoice newInvoice = new PackageInvoice(returnId, Exp_client_id, Dest_client_id,
						Order_description, Order_cost, Creation_date, Order_status, Order_weight, Deposit_id,
						Delivery_date, Modify_date, Content_type, Content_location, cashierId);
				newInvoice.setInvoice_id(rs.getInt("Invoice_id"));
				packs.add(newInvoice);
			}
		} catch (SQLException e) {
			LOGGER.error("Error in PackageInvoiceRepository, getPackageInvoices ", e);
		}
		LOGGER.debug("Leave getPackageInvoices in repository");
		return packs;
	}

	/**
	 * Get id of invoice from a specific type location
	 * 
	 * @param contentLocation: String
	 * @return id: integer
	 */
	@Override
	public int getInvoiceIdByContentLocation(String contentLocation) {
		LOGGER.debug("Enter getInvoiceIdByContentLocation in repository");
		int id = 0;
		String sql = "Select * from Package_Invoice where content_location =\'" + contentLocation + "\'";
		try {
			LOGGER.debug("Prepare statement...");
			PreparedStatement prepSt = DB_CONNECTION.prepareStatement(sql);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery(sql);

			while (rs.next()) {
				id = rs.getInt("invoice_id");
			}
		} catch (SQLException e) {
			LOGGER.error("Error in PackageInvoiceRepository, getInvoiceIdByContentLocation ", e);
		}

		LOGGER.debug("Leave getInvoiceIdByContentLocation in repository");
		return id;
	}

	/**
	 * Get invoice with given id from database
	 * 
	 * @param id: integer
	 * @return newInvoice: PackageInvoice
	 */
	@Override
	public PackageInvoice getPackageInvoiceById(int id) {
		LOGGER.debug("Enter getPackageInvoiceById in repository");
		PackageInvoice newInvoice = null;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = getPackageInvoiceByIdStatement(DB_CONNECTION, id);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery();

			while (rs.next()) {
				int Invoice_id = rs.getInt("INVOICE_ID");
				int returnId = rs.getInt("Return_id");
				int Exp_client_id = rs.getInt("Exp_client_id");
				int Dest_client_id = rs.getInt("Dest_client_id");
				String Order_description = rs.getString("Order_description");
				long Order_cost = rs.getLong("Order_cost");
				Date Creation_date = rs.getDate("CREATION_DATE");
				String Order_status = rs.getString("Order_status");
				long Order_weight = rs.getLong("Order_weight");
				int Deposit_id = rs.getInt("Deposit_id");
				Date Delivery_date = rs.getDate("Delivery_date");
				Date Modify_date = rs.getDate("Modify_date");
				String Content_type = rs.getString("Content_type");
				String Content_location = rs.getString("Content_location");
				int cashierId = rs.getInt("Cashier_id");

				newInvoice = new PackageInvoice(returnId, Exp_client_id, Dest_client_id, Order_description, Order_cost,
						Creation_date, Order_status, Order_weight, Deposit_id, Delivery_date, Modify_date, Content_type,
						Content_location, cashierId);
				newInvoice.setInvoice_id(Invoice_id);
			}
		} catch (SQLException e) {
			LOGGER.error("Error in PackageInvoiceRepository, getPackageInvoiceById ", e);
		}

		LOGGER.debug("Leave getPackageInvoiceById in repository");
		return newInvoice;
	}

}
