package ro.fme.internship.delivery.repository;

import java.util.ArrayList;

import ro.fme.internship.delivery.model.PackageInvoice;

public interface IPackageInvoiceRepository {

	public int insertPackageInvoice(PackageInvoice invoice);

	public int updatePackageInvoice(PackageInvoice invoice, int id);

	public ArrayList<PackageInvoice> getPackageInvoices();

	public int getInvoiceIdByContentLocation(String contentLocation);

	public PackageInvoice getPackageInvoiceById(int id);
}
