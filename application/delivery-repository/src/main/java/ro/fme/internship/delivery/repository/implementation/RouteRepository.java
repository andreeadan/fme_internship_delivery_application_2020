package ro.fme.internship.delivery.repository.implementation;

import static util.QueryHelperRoute.getALLRoutes;
import static util.QueryHelperRoute.getALLRoutesByDriverId;
import static util.QueryHelperRoute.getRouteById;
import static util.QueryHelperRoute.insertRouteStatement;
import static util.QueryHelperRoute.updateRouteDetails;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.database.connection.DBConnection;
import ro.fme.internship.delivery.model.Route;
import ro.fme.internship.delivery.repository.IRouteRepository;
import util.QueryHelperRoute;

public class RouteRepository implements IRouteRepository {

	private static final Connection DB_CONNECTION = DBConnection.getConnection();
	private static final Logger LOGGER = LoggerFactory.getLogger(RouteRepository.class);
	private PreparedStatement prepSt = null;

	/**
	 * Insert a route in the database
	 * 
	 * @param route: Route
	 */
	@Override
	public int insertRoute(Route route) {
		LOGGER.debug("Enter insertRoute in repository");
		int response = 0;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = insertRouteStatement(route, DB_CONNECTION);
			LOGGER.debug("Executing update...");
			response = prepSt.executeUpdate();
			if (response == 1) {
				LOGGER.debug("Route was successfully created!");
				return response;
			} else
				LOGGER.debug("There was a problem with the route creation!");
		} catch (SQLException e) {
			LOGGER.error("Error in RouteRepository, insertRoute " + e);
		}
		LOGGER.debug("Leave insertRoute in repository");
		return response;
	}

	/**
	 * Update a route in the database
	 * 
	 * @param route: Route, id: integer
	 */
	@Override
	public void updateRoute(Route route, int id) {
		LOGGER.debug("Enter updateRoute in repository");
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = updateRouteDetails(route, id, DB_CONNECTION);
			LOGGER.debug("Executing update...");
			int response = prepSt.executeUpdate();
			if (response == 1) {
				LOGGER.debug("Route was successfully updated!");
				LOGGER.debug("Enter updateRoute in repository");
			} else
				LOGGER.debug("There was a problem with the route update!");
		} catch (SQLException e) {
			LOGGER.error("Error in RouteRepository, updateRoute " + e);
		}
	}

	/**
	 * Get a route based on a given id
	 * 
	 * @param id: integer
	 * @return route: Route
	 */
	@Override
	public Route getRouteByID(int id) {
		LOGGER.debug("Enter getRouteByID in repository in repository");
		Route route = null;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = getRouteById(id, DB_CONNECTION);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery();

			while (rs.next()) {
				int idRoute = rs.getInt("ROUTE_ID");
				int idDep = rs.getInt("DEPOSIT_ID");
				int nbDriver = rs.getInt("DRIVER_ID");
				String expCounty = rs.getString("EXP_COUNTY");
				String destCounty = rs.getString("DEST_COUNTY");

				route = new Route(idDep, nbDriver, expCounty, destCounty);
				route.setIdRoute(idRoute);
			}
		} catch (SQLException e) {
			LOGGER.error("Error in RouteRepository, getRouteByID " + e);
		}
		LOGGER.debug("Leave getRouteByID in repository");
		return route;
	}

	/**
	 * Get all routes in the database
	 * 
	 * @return routes: ArrayList<Route>
	 */
	@Override
	public ArrayList<Route> getAllRoutes() {
		LOGGER.debug("Enter getAllRoutes in repository");
		ArrayList<Route> routes = new ArrayList<>();
		try {
			LOGGER.debug("Prepare statement...");
			PreparedStatement prepSt = getALLRoutes(DB_CONNECTION);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery();

			while (rs.next()) {
				int routeID = rs.getInt("ROUTE_ID");
				int depositID = rs.getInt("DEPOSIT_ID");
				int driverID = rs.getInt("DRIVER_ID");
				String expCounty = rs.getString("EXP_COUNTY");
				String destCounty = rs.getString("DEST_COUNTY");

				Route route = new Route(depositID, driverID, expCounty, destCounty);
				route.setIdRoute(routeID);
				routes.add(route);
			}
		} catch (SQLException e) {
			LOGGER.error("Error in RouteRepository, getAllRoutes " + e);
		}
		LOGGER.debug("Leave getAllRoutes in repository");
		return routes;
	}
	
	@Override
	public ArrayList<Route> getAllRoutesByDriverID(int idDriver) {
		LOGGER.debug("Enter getAllRoutes in repository");
		ArrayList<Route> routes = new ArrayList<>();
		try {
			LOGGER.debug("Prepare statement...");
			PreparedStatement prepSt = getALLRoutesByDriverId(idDriver, DB_CONNECTION);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery();

			while (rs.next()) {
				int routeID = rs.getInt("ROUTE_ID");
				int depositID = rs.getInt("DEPOSIT_ID");
				int driverID = rs.getInt("DRIVER_ID");
				String expCounty = rs.getString("EXP_COUNTY");
				String destCounty = rs.getString("DEST_COUNTY");

				Route route = new Route(depositID, driverID, expCounty, destCounty);
				route.setIdRoute(routeID);
				routes.add(route);
			}
		} catch (SQLException e) {
			LOGGER.error("Error in RouteRepository, getAllRoutes " + e);
		}
		LOGGER.debug("Leave getAllRoutes in repository");
		return routes;
	}
}
