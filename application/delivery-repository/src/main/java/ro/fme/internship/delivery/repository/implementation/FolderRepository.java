package ro.fme.internship.delivery.repository.implementation;

import static util.QueryHelperFolder.getFolderByIdStatement;
import static util.QueryHelperFolder.insertFolderStatement;
import static util.QueryHelperFolder.selectAllFolders;
import static util.QueryHelperFolder.updateFolderStatement;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.database.connection.DBConnection;
import ro.fme.internship.delivery.model.Folder;
import ro.fme.internship.delivery.repository.IFolderRepository;

public class FolderRepository implements IFolderRepository {

	private static final Connection DB_CONNECTION = DBConnection.getConnection();
	private static final Logger LOGGER = LoggerFactory.getLogger(FolderRepository.class);
	PreparedStatement prepSt = null;

	public FolderRepository() {
	}

	/**
	 * Insert a folder in the database
	 * 
	 * @param folder: Folder
	 * @return response: integer
	 */
	@Override
	public int insertFolder(Folder folder) {
		LOGGER.debug("Enter insertFolder in repository");
		int response = 0;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = insertFolderStatement(folder, DB_CONNECTION);
			LOGGER.debug("Executing query...");
			response = prepSt.executeUpdate();
			if (response == 1) {
				LOGGER.debug("New folder was created!");
				return response;
			} else {
				LOGGER.debug("Folder not created!");
				return response;
			}
		} catch (SQLException e) {
			LOGGER.error("Error in FolderRepository, insertFolder ", e);
			return response;
		}
	}

	/**
	 * Update a folder in the database
	 * 
	 * @param folder: Folder, id: integer
	 * @return response: integer
	 */
	@Override
	public void updateFolder(Folder folder, int id) {
		LOGGER.debug("Enter updateFolder");
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = updateFolderStatement(folder, id, DB_CONNECTION);
			LOGGER.debug("Executing query...");
			int response = prepSt.executeUpdate();
			if (response == 1) {
				LOGGER.debug("The folder was updated!");
			} else {
				LOGGER.debug("Folder not updated!");
			}
		} catch (SQLException e) {
			LOGGER.error("Error in FolderRepository, updateFolder ", e);
		}
	}

	/**
	 * Get list of all folders from Folder table
	 * 
	 * @return folders: ArrayList<Folder>
	 */
	@Override
	public ArrayList<Folder> getAllFolders() {
		LOGGER.debug("Enter getAllFolders in repository");
		ArrayList<Folder> folders = new ArrayList<>();
		try {
			LOGGER.debug("Prepare statement...");
			PreparedStatement prepSt = selectAllFolders(DB_CONNECTION);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery();
			while (rs.next()) {
				String name = rs.getString("name");
				Date creationD = rs.getDate("creation_date");
				String owner = rs.getString("owner");
				int childNr = rs.getInt("child_nr");
				String folderType = rs.getString("folder_type");
				Date modifyD = rs.getDate("modify_date");

				Folder folder = new Folder(name, creationD, owner, childNr, modifyD, folderType);
				folder.setFolder_id(rs.getInt("folder_id"));
				folders.add(folder);
			}
		} catch (SQLException e) {
			LOGGER.error("Error in FolderRepository, getAllFolders ", e);
		}
		LOGGER.debug("Leave getAllFolders in repository");
		return folders;
	}

	/**
	 * Get the id on the target folder
	 * 
	 * @param folder: Folder
	 * @return id: integer
	 */
	@SuppressWarnings("deprecation")
	public int findTargetFolderId(Folder folder) {
		LOGGER.debug("Enter findTargetFolderId in repository");
		PreparedStatement prepSt = null;
		int id = 0;
		String sql = "";
		try {
			LOGGER.debug("Creating the sql statement...");

			if (folder.getName().equals(String.valueOf(folder.getCreation_date().getYear()))
					&& folder.getFolder_type().equals("intern")) {
				LOGGER.debug("Folder is year");

				sql = "Select FOLDER_ID from FOLDER where NAME=\'" + folder.getName() + "\'";

			} else if (folder.getFolder_type().equals("intern") && folder.getName().contains("-")) {
				LOGGER.debug("Folder is month");
				sql = "SELECT f2.folder_id" + "FROM FOLDER f1" + "INNER JOIN relation_folder_in_folder rff"
						+ "ON f1.folder_id = rff.target_id" + "INNER JOIN folder f2" + "ON rff.source_id = f2.folder_id"
						+ "WHERE f1.name =\'" + folder.getCreation_date().getYear() + "\'" + "AND f2.name = \'"
						+ folder.getCreation_date().getMonth() + "-" + folder.getCreation_date().getDate() + "\'";

			} else if (folder.getFolder_type().equals("record")) {
				LOGGER.debug("Folder is client");

				sql = "SELECT f3.folder_id" + "FROM FOLDER f1" + "INNER JOIN relation_folder_in_folder rff"
						+ "ON f1.folder_id = rff.target_id" + "INNER JOIN folder f2" + "ON rff.source_id = f2.folder_id"
						+ "inner join relation_folder_in_folder rff2" + "on f2.folder_id= rff2.target_id"
						+ "inner join folder f3 on" + "rff2.source_id = f3.folder_id" + "WHERE f1.name =\'"
						+ folder.getCreation_date().getYear() + "\'" + "AND f2.name =\'"
						+ folder.getCreation_date().getMonth() + "-" + folder.getCreation_date().getDate() + "\'"
						+ "and f3.name = \'" + folder.getName() + "\'";
			}

			LOGGER.debug("Prepare statement...");
			prepSt = DB_CONNECTION.prepareStatement(sql);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery(sql);

			while (rs.next()) {
				id = rs.getInt("folder_id");
			}
		} catch (SQLException e) {
			LOGGER.error("Error in FolderRepository, findTargetFolderId ", e);
		}
		LOGGER.debug("Leave findTargetFolderId in repository");
		return id;
	}

	/**
	 * Get the id for the source folder
	 * 
	 * @param folder: Folder
	 * @return id: integer
	 */
	@SuppressWarnings("deprecation")
	public int findSourceFolderId(Folder folder) {

		LOGGER.debug("Enter findSourceFolderId  in repository");
		PreparedStatement prepSt = null;
		int id = 0;
		LOGGER.debug("Creating the sql statement...");
		String sql = "Select FOLDER_ID from FOLDER where NAME=\'" + folder.getName() + "\'";
		try {
			if (!folder.getName().equals(String.valueOf(folder.getCreation_date().getYear()))
					|| !folder.getFolder_type().equals("intern")) {
				sql += "and FOLDER_ID not in (select source_id from relation_folder_in_folder )";
			}

			LOGGER.debug("Prepare statement...");
			prepSt = DB_CONNECTION.prepareStatement(sql);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery(sql);

			while (rs.next()) {
				id = rs.getInt("folder_id");
			}
		} catch (SQLException e) {
			LOGGER.error("Error in FolderRepository, findSourceFolderId ", e);
		}
		LOGGER.debug("Leave findSourceFolderId  in repository");
		return id;
	}

	
	/**
	 * Get the Folder object with a given name
	 * 
	 * @param name: String
	 * @return folder: Folder
	 */
	@Override
	public Folder getFolderByName(String name) {
		LOGGER.debug("Enter getFolderByName in repository");
		ArrayList<Folder> folderRepo = getAllFolders();
		try {
			LOGGER.debug("Search all folders...");
			for (Folder folder : folderRepo) {
				if (folder.getName().equals(name) && folder.getFolder_type().equals("intern")) {
					LOGGER.debug("Leave getFolderByName in repository");
					return folder;
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error in FolderRepository, getFolderByName ", e);
		}
		LOGGER.debug("Leave getFolderByName on null");
		return null;
	}

	/**
	 * Get the Folder object with a given id
	 * 
	 * @param id: integer
	 * @return folder: Folder
	 */
	@Override
	public Folder getFolderWithId(int id) {
		LOGGER.debug("Enter getFolderWithId in repository");
		Folder folder = null;
		try {
			LOGGER.debug("Prepare statement...");
			prepSt = getFolderByIdStatement(id, DB_CONNECTION);
			LOGGER.debug("Executing query...");
			ResultSet rs = prepSt.executeQuery();

			while (rs.next()) {
				String name = rs.getString("name");
				Date creation_date = rs.getDate("creation_date");
				String owner = rs.getString("owner");
				int child_nr = rs.getInt("child_nr");
				String folder_type = rs.getString("folder_type");
				Date modify_date = rs.getDate("modify_date");

				folder = new Folder(name, creation_date, owner, child_nr, modify_date, folder_type);
				folder.setFolder_id(rs.getInt("folder_id"));
				LOGGER.debug("Folder was found!");
			}
		} catch (SQLException e) {
			LOGGER.error("Error in FolderRepository, getFolderWithId ", e);
		}
		LOGGER.debug("Leave getFolderWithId  in repository");
		return folder;
	}

	/**
	 * get list of year folder
	 * 
	 * @return list of all folders that represents an year
	 */
	public ArrayList<Folder> getAllYearFolders() {
		LOGGER.debug("Enter getAllYearFolders in repository");
		ArrayList<Folder> yearFolders = new ArrayList<>();
		PreparedStatement prepSt = null;
		// statement to get all folders that are year(intern and four letters
		String sql = "Select * from FOLDER where FOLDER_TYPE = 'intern' and NAME like '____' and NAME not like '%-%'";
		try {
			prepSt = DB_CONNECTION.prepareStatement(sql);
			ResultSet rs = prepSt.executeQuery();

			while (rs.next()) {
				String name = rs.getString("name");
				Date creation_date = rs.getDate("creation_date");
				String owner = rs.getString("owner");
				int child_nr = rs.getInt("child_nr");
				String folder_type = rs.getString("folder_type");
				Date modify_date = rs.getDate("modify_date");

				Folder folder = new Folder(name, creation_date, owner, child_nr, modify_date, folder_type);
				folder.setFolder_id(rs.getInt("folder_id"));
				yearFolders.add(folder);
			}
		} catch (SQLException e) {
			LOGGER.error("Error in FolderRepository, getAllYearfolders " + e);
			System.out.println(e.getMessage());
		}
		LOGGER.debug("Leave getAllYearFolders  in repository");
		return yearFolders;
	}

	/**
	 * get all subfolders of a folder
	 * 
	 * @param parent_id
	 * @return list of children of a folder
	 */
	public ArrayList<Folder> getAllChildren(int parent_id) {
		LOGGER.debug("Enter getAllChildren in repository");
		ArrayList<Folder> children = new ArrayList<>();

		PreparedStatement prepSt = null;
		// statement to get all folders that are year(intern and four letters
		String sql = "Select * from Folder f INNER JOIN relation_folder_in_folder rff ON f.folder_id = rff.source_id where rff.target_id = ?";
		try {
			prepSt = DB_CONNECTION.prepareStatement(sql);
			prepSt.setInt(1, parent_id);
			ResultSet rs = prepSt.executeQuery();

			while (rs.next()) {
				String name = rs.getString("name");
				Date creation_date = rs.getDate("creation_date");
				String owner = rs.getString("owner");
				int child_nr = rs.getInt("child_nr");
				String folder_type = rs.getString("folder_type");
				Date modify_date = rs.getDate("modify_date");

				Folder folder = new Folder(name, creation_date, owner, child_nr, modify_date, folder_type);
				folder.setFolder_id(rs.getInt("folder_id"));
				children.add(folder);
			}
		} catch (SQLException e) {
			LOGGER.error("Error in FolderRepository, getAllChildren " + e);
			System.out.println(e.getMessage());
		}
		LOGGER.debug("Leave getAllChildren  in repository");
		return children;
	}

}
