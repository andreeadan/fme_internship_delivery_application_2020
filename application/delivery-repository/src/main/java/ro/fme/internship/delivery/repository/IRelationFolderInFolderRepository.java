package ro.fme.internship.delivery.repository;

import java.util.ArrayList;

import ro.fme.internship.delivery.model.Folder;
import ro.fme.internship.delivery.model.RelationFolderInFolder;

public interface IRelationFolderInFolderRepository {

	public int insertRelationFolderInFolder(Folder sourceFolder, Folder targetFolder);

	public int getFolderId(Folder folder);

	public ArrayList<RelationFolderInFolder> getAllRelationFolderInFolder();

	public ArrayList<RelationFolderInFolder> getRelationsOfTargetFolder(Folder targetFolder);
}
