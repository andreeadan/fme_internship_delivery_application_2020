package ro.fme.internship.delivery.repository;

import java.util.ArrayList;

import ro.fme.internship.delivery.model.Admin;
import ro.fme.internship.delivery.model.Cashier;
import ro.fme.internship.delivery.model.Client;
import ro.fme.internship.delivery.model.Driver;
import ro.fme.internship.delivery.model.PackageInvoice;
import ro.fme.internship.delivery.model.User;

public interface IUserRepository {

	public int insertUser(User user);

	public int updateUser(User user, String username);

	public int updateDriverCar(String a, String b);

	public int deleteUser(User user);

	public void deleteAllRecords(String tableName);

	public ArrayList<Admin> getAdminUsers();

	public ArrayList<Driver> getDriverUsers();

	public ArrayList<Client> getClientUsers();

	public ArrayList<Cashier> getCashierUsers();

	public Driver getDriver(int id);

	public Admin getAdmin(int id);

	public Client getClient(int id);

	public Cashier getCashier(int id);

	public User getLoginUser(String username, String password);

	public int getDriverIdByUsername(String username);

	int updateUserById(User user, int id);
	
	public int getClientIdByUsername(String username);
	
	public Client getClientByUsername(String username);
	
	public Cashier getCashierByUsername(String username);

	public Admin getAdminByUsername(String username);

}
