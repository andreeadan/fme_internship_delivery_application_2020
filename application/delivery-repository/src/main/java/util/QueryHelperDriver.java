package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import ro.fme.internship.delivery.model.Cashier;
import ro.fme.internship.delivery.model.Driver;

public class QueryHelperDriver {

	public static final String INSERT_DRIVER_STATEMENT = "INSERT INTO DRIVER(USERNAME, PASSWORD, FIRSTNAME, LASTNAME, CAR) VALUES(?, ?, ?, ?, ?)";

	public static final String UPDATE_DRIVER_DETAILS = "UPDATE DRIVER SET FIRSTNAME=?, LASTNAME=?, PASSWORD=? WHERE USERNAME=?";

	public static final String UPDATE_DRIVER_CAR = "UPDATE DRIVER SET CAR=? WHERE USERNAME=?";

	public static final String DELETE_DRIVER_STATEMENT = "UPDATE DRIVER SET CAR = NULL, USERNAME = NULL, PASSWORD = NULL, FIRSTNAME = NULL, "
			+ "LASTNAME = NULL WHERE USERNAME = ?";

	public static final String GET_DRIVER_DETAILS = "SELECT * FROM DRIVER WHERE DRIVER_ID=?";
	
	public static final String GET_DRIVER_ID_BY_USERNAME = "SELECT DRIVER_ID FROM DRIVER WHERE USERNAME=?";
	
	public static final String GET_DRIVER_BY_USERNAME_AND_PASSWORD = "(SELECT * FROM DRIVER WHERE USERNAME=? AND PASSWORD=?)";
	
	public static final String UPDATE_DRIVER_USER = "UPDATE DRIVER SET FIRSTNAME=?, LASTNAME=?, PASSWORD=?, USERNAME=?, CAR=? WHERE DRIVER_ID=?";

	
	public static PreparedStatement getDriverUserByUsernameAndPassword(String username, String password, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_DRIVER_BY_USERNAME_AND_PASSWORD);
		prepSt.setString(1, username);
		prepSt.setString(2, password);

		return prepSt;
	}

	public static final String GET_DRIVERS = "SELECT * FROM DRIVER";

	public static PreparedStatement getDriverUsersStatement(Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_DRIVERS);

		return prepSt;
	}

	public static PreparedStatement getDriverStatement(Driver user, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(INSERT_DRIVER_STATEMENT);
		prepSt.setString(1, user.getUsername());
		prepSt.setString(2, user.getPassword());
		prepSt.setString(3, user.getFirstName());
		prepSt.setString(4, user.getLastName());
		prepSt.setString(5, ((Driver) user).getCar());

		return prepSt;
	}

	public static PreparedStatement updateDriverStatement(Driver user, String username, Connection conn)
			throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(UPDATE_DRIVER_DETAILS);
		prepSt.setString(1, user.getFirstName());
		prepSt.setString(2, user.getLastName());
		prepSt.setString(3, user.getPassword());
		prepSt.setString(4, username);

		return prepSt;
	}
	
	public static PreparedStatement updateDriverUserStatement(Driver user,int driver_id, Connection conn)
			throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(UPDATE_DRIVER_USER);
		prepSt.setString(1, user.getFirstName());
		prepSt.setString(2, user.getLastName());
		prepSt.setString(3, user.getPassword());
		prepSt.setString(4, user.getUsername());
		prepSt.setString(5, user.getCar());
		prepSt.setInt(6, driver_id);

		return prepSt;
	}

	public static PreparedStatement updateCar(String newCar, String username, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(UPDATE_DRIVER_CAR);
		prepSt.setString(1, newCar);
		prepSt.setString(2, username);

		return prepSt;
	}

	public static PreparedStatement deleteDriverStatement(Driver user, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(DELETE_DRIVER_STATEMENT);
		prepSt.setString(1, user.getUsername());

		return prepSt;
	}

	public static PreparedStatement getDriverByIdStatement(int id, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_DRIVER_DETAILS);
		prepSt.setInt(1, id);

		return prepSt;
	}
	
	public static PreparedStatement getDriverIdByUsername(String username, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_DRIVER_ID_BY_USERNAME);
		prepSt.setString(1, username);
		return prepSt;
	}

}
