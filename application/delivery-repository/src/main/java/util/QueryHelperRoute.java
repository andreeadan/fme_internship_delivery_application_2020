package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import ro.fme.internship.delivery.model.Route;

public class QueryHelperRoute {

	public static final String INSERT_ROUTE_STATEMENT = "INSERT INTO ROUTE(DEPOSIT_ID, DRIVER_ID, EXP_COUNTY, DEST_COUNTY) VALUES(?,?,?,?)";

	public static final String UPDATE_ROUTE_DETAILS = "UPDATE ROUTE SET DEPOSIT_ID=?, DRIVER_ID=? WHERE ROUTE_ID=?";

	public static final String GET_ROUTE_BY_ID = "SELECT * FROM ROUTE WHERE ROUTE_ID=?";

	public static final String GET_ALL_ROUTES = "SELECT * FROM ROUTE";
	
	public static final String GET_ALL_ROUTES_BY_DRIVER_ID = "SELECT * FROM ROUTE WHERE DRIVER_ID=?";

	public static PreparedStatement updateRouteDetails(Route route, int number, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(UPDATE_ROUTE_DETAILS);
		prepSt.setInt(1, route.getIdDeposit());
		prepSt.setInt(2, route.getIdDriver());
		prepSt.setInt(3, number);

		return prepSt;
	}

	public static PreparedStatement insertRouteStatement(Route route, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(INSERT_ROUTE_STATEMENT);
		prepSt.setInt(1, route.getIdDeposit());
		prepSt.setInt(2, route.getIdDriver());
		prepSt.setString(3, route.getExpCounty());
		prepSt.setString(4, route.getDestCounty());

		return prepSt;
	}

	public static PreparedStatement getRouteById(int idRoute, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_ROUTE_BY_ID);
		prepSt.setInt(1, idRoute);

		return prepSt;
	}

	public static PreparedStatement getALLRoutes(Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_ALL_ROUTES);

		return prepSt;
	}
	
	public static PreparedStatement getALLRoutesByDriverId(int idDriver, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_ALL_ROUTES_BY_DRIVER_ID);
		prepSt.setInt(1, idDriver);

		return prepSt;
	}

}
