package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import ro.fme.internship.delivery.model.Client;
import ro.fme.internship.delivery.model.Driver;

public class QueryHelperClient {

	public static final String INSERT_CLIENT_STATEMENT = "INSERT INTO CLIENT(USERNAME, PASSWORD, FIRSTNAME, LASTNAME, ADDRESS, PHONE_NUMBER, "
			+ "ZIPCODE, CITY, COUNTY) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";

	public static final String UPDATE_CLIENT_DETAILS = "UPDATE CLIENT SET ADDRESS=?, PHONE_NUMBER=?, ZIPCODE=?, CITY=?, COUNTY=?, PASSWORD=?, FIRSTNAME=?, LASTNAME=? WHERE USERNAME=?";

	public static final String DELETE_CLIENT_STATEMENT = "UPDATE CLIENT SET ADDRESS = NULL, PHONE_NUMBER = NULL, ZIPCODE = NULL, CITY = NULL, "
			+ "COUNTY = NULL, USERNAME = NULL, PASSWORD = NULL, FIRSTNAME = NULL, LASTNAME = NULL WHERE USERNAME = ?";

	public static final String GET_CLIENT_DETAILS = "SELECT * FROM CLIENT WHERE CLIENT_ID=?";

	public static final String GET_ALL_CLIENTS = "SELECT * FROM CLIENT";
	
	public static final String UPDATE_CLIENT_USER = "UPDATE CLIENT SET ADDRESS=?, PHONE_NUMBER=?, ZIPCODE=?, CITY=?, COUNTY=?, PASSWORD=?, FIRSTNAME=?, LASTNAME=?, USERNAME=? WHERE CLIENT_ID=?";
	
	public static final String GET_CLIENT_BY_USERNAME_AND_PASSWORD = "(SELECT * FROM CLIENT WHERE USERNAME=? AND PASSWORD=?)";
	
	public static final String GET_CLIENT_ID_BY_USERNAME = "SELECT CLIENT_ID FROM CLIENT WHERE USERNAME=?";
		
	public static final String GET_CLIENT_BY_USERNAME = "SELECT * FROM CLIENT WHERE USERNAME=?";
	
	public static PreparedStatement getClientUserByUsername(String username, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_CLIENT_BY_USERNAME);
		prepSt.setString(1, username);

		return prepSt;
	}
	

	public static PreparedStatement getClientUserByUsernameAndPassword(String username, String password, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_CLIENT_BY_USERNAME_AND_PASSWORD);
		prepSt.setString(1, username);
		prepSt.setString(2, password);

		return prepSt;
	}

	public static PreparedStatement getClientStatement(Client user, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(INSERT_CLIENT_STATEMENT);
		prepSt.setString(1, user.getUsername());
		prepSt.setString(2, user.getPassword());
		prepSt.setString(3, user.getFirstName());
		prepSt.setString(4, user.getLastName());
		prepSt.setString(5, ((Client) user).getAddress());
		prepSt.setString(6, ((Client) user).getPhoneNumber());
		prepSt.setInt(7, ((Client) user).getZipCode());
		prepSt.setString(8, ((Client) user).getCity());
		prepSt.setString(9, ((Client) user).getCounty());
		
		return prepSt;
	}

	public static PreparedStatement deleteClientStatement(Client user, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(DELETE_CLIENT_STATEMENT);
		prepSt.setString(1, user.getUsername());

		return prepSt;
	}

	public static PreparedStatement updateClientStatement(Client user, String username, Connection conn)
			throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(UPDATE_CLIENT_DETAILS);

		prepSt.setString(1, user.getAddress());
		prepSt.setString(2, user.getPhoneNumber());
		prepSt.setInt(3, user.getZipCode());
		prepSt.setString(4, user.getCity());
		prepSt.setString(5, user.getCounty());
		prepSt.setString(6, user.getPassword());
		prepSt.setString(7, user.getFirstName());
		prepSt.setString(8, user.getLastName());
		prepSt.setString(9, username);

		return prepSt;
	}
	
	public static PreparedStatement updateClientUserStatement(Client user,int client_id, Connection conn)
			throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(UPDATE_CLIENT_USER);
		prepSt.setString(1, user.getAddress());
		prepSt.setString(2, user.getPhoneNumber());
		prepSt.setInt(3, user.getZipCode());
		prepSt.setString(4, user.getCity());
		prepSt.setString(5, user.getCounty());
		prepSt.setString(6, user.getPassword());
		prepSt.setString(7, user.getFirstName());
		prepSt.setString(8, user.getLastName());
		prepSt.setString(9, user.getUsername());
		prepSt.setInt(10, client_id);

		return prepSt;
	}

	public static PreparedStatement getClientByID(int id, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_CLIENT_DETAILS);
		prepSt.setInt(1, id);

		return prepSt;
	}
	
	public static PreparedStatement geClientIdByUsernameStatement(String username, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_CLIENT_ID_BY_USERNAME);
		prepSt.setString(1, username);

		return prepSt;
	}
	
}
