package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import ro.fme.internship.delivery.model.Admin;
import ro.fme.internship.delivery.model.Cashier;

public class QueryHelperCashier {

	public static final String INSERT_CASHIER_STATEMENT = "INSERT INTO CASHIER (USERNAME, PASSWORD, FIRSTNAME, LASTNAME) VALUES (?, ?, ?, ?)";

	public static final String UPDATE_CASHIER_DETAILS = "UPDATE CASHIER SET FIRSTNAME=?, LASTNAME=?, PASSWORD=? WHERE USERNAME=?";

	public static final String DELETE_CASHIER_STATEMENT = "UPDATE CASHIER SET USERNAME = NULL, PASSWORD = NULL, FIRSTNAME = NULL, LASTNAME = NULL "
			+ "WHERE USERNAME = ?";

	public static final String GET_CASHIER_BY_ID = "SELECT * FROM CASHIER WHERE CASHIER_ID = ?";
	
	public static final String UPDATE_CASHIER_USER = "UPDATE CASHIER SET FIRSTNAME=?, LASTNAME=?, PASSWORD=?, USERNAME=? WHERE CASHIER_ID=?";

	
	public static final String GET_CASHIER_BY_USERNAME_AND_PASSWORD = "(SELECT * FROM CASHIER WHERE USERNAME=? AND PASSWORD=?)";
	
public static final String GET_CASHIER_BY_USERNAME = "SELECT * FROM CASHIER WHERE USERNAME=?";
	
	public static PreparedStatement getCashierUserByUsername(String username, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_CASHIER_BY_USERNAME);
		prepSt.setString(1, username);

		return prepSt;
	}
	
	public static PreparedStatement getCashierUserByUsernameAndPassword(String username, String password, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_CASHIER_BY_USERNAME_AND_PASSWORD);
		prepSt.setString(1, username);
		prepSt.setString(2, password);

		return prepSt;
	}

	public static final String GET_CASHIERS = "SELECT * FROM CASHIER";

	public static PreparedStatement getCashierUsersStatement(Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_CASHIERS);
		return prepSt;
	}

	public static PreparedStatement getCashierStatement(Cashier user, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(INSERT_CASHIER_STATEMENT);
		prepSt.setString(1, user.getUsername());
		prepSt.setString(2, user.getPassword());
		prepSt.setString(3, user.getFirstName());
		prepSt.setString(4, user.getLastName());
		return prepSt;
	}
	
	public static PreparedStatement updateCashierUserStatement(Cashier user,int cashier_id, Connection conn)
			throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(UPDATE_CASHIER_USER);
		prepSt.setString(1, user.getFirstName());
		prepSt.setString(2, user.getLastName());
		prepSt.setString(3, user.getPassword());
		prepSt.setString(4, user.getUsername());
		prepSt.setInt(5, cashier_id);

		return prepSt;
	}

	public static PreparedStatement deleteCashierStatement(Cashier user, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(DELETE_CASHIER_STATEMENT);
		prepSt.setString(1, user.getUsername());

		return prepSt;
	}

	public static PreparedStatement updateCashierStatement(Cashier user, String username, Connection conn)
			throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(UPDATE_CASHIER_DETAILS);
		prepSt.setString(1, user.getFirstName());
		prepSt.setString(2, user.getLastName());
		prepSt.setString(3, user.getPassword());
		prepSt.setString(4, username);
		return prepSt;
	}

	public static PreparedStatement getCashierByIdStatement(int id, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_CASHIER_BY_ID);
		prepSt.setInt(1, id);
		return prepSt;
	}

}
