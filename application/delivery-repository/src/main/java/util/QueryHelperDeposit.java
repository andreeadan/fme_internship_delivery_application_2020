package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import ro.fme.internship.delivery.model.Deposit;

public class QueryHelperDeposit {

	public static final String INSERT_DEPOSIT_STATEMENT = "INSERT INTO DEPOSIT(COUNTY, ADDRESS, STATUS) VALUES (?, ?, ?)";

	public static final String UPDATE_DEPOSIT_DETAILS = "UPDATE DEPOSIT SET COUNTY=?, ADDRESS=?, STATUS=? WHERE DEPOSIT_ID=?";

	public static final String GET_DEPOSIT_DETAILS = "SELECT * FROM DEPOSIT WHERE DEPOSIT_ID=?";
	
	public static final String GET_DEPOSIT_BY_COUNTY= "SELECT * FROM DEPOSIT WHERE COUNTY=?" ;
	
	public static final String GET_ALL_DEPOSITS = "SELECT * FROM DEPOSIT";

	public static PreparedStatement getDepositStatement(Deposit deposit, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(INSERT_DEPOSIT_STATEMENT);
		prepSt.setString(1, deposit.getCounty());
		prepSt.setString(2, deposit.getAddress());
		prepSt.setString(3, deposit.getStatus());
		return prepSt;
	}

	public static PreparedStatement updateDepositStatement(Deposit deposit, int number, Connection conn)
			throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(UPDATE_DEPOSIT_DETAILS);
		prepSt.setString(1, deposit.getCounty());
		prepSt.setString(2, deposit.getAddress());
		prepSt.setString(3, deposit.getStatus());
		prepSt.setInt(4, number);

		return prepSt;
	}

	public static PreparedStatement getDepositByIdStatement(int id, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_DEPOSIT_DETAILS);
		prepSt.setInt(1, id);

		return prepSt;
	}
	
	public static PreparedStatement getAllDepositsStatement(Connection conn) throws SQLException {
		PreparedStatement prepSt =conn.prepareStatement(GET_ALL_DEPOSITS);
		
		return prepSt;
	}
	
	public static PreparedStatement getDepositByCountyStatement(String county, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_DEPOSIT_BY_COUNTY);
		prepSt.setString(1, county);

		return prepSt;
	}
	
}
