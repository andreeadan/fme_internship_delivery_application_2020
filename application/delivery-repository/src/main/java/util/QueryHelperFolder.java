package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import ro.fme.internship.delivery.model.Folder;

public class QueryHelperFolder {

	public static final String INSERT_FOLDER_STATEMENT = "INSERT INTO FOLDER (NAME, CREATION_DATE, OWNER, CHILD_NR, FOLDER_TYPE, MODIFY_DATE) VALUES (?, ?, ?, ?, ?, ?)";

	public static final String UPDATE_FOLDER_DETAILS = "UPDATE FOLDER SET CHILD_NR=?, MODIFY_DATE=?, WHERE FOLDER_ID=?";

	public static final String SELECT_FOLDER_ID = "SELECT FOLDER_ID FROM FOLDER WHERE (NAME) = (?)";

	public static final String INSERT_RELATION_FOLDER_IN_FOLDER_STATEMENT = "INSERT INTO RELATION_FOLDER_IN_FOLDER (SOURCE_ID, TARGET_ID) VALUES (?, ?)";

	public static final String SELECT_ALL_FOLDERS = "SELECT * FROM FOLDER";

	public static final String INSERT_RELATION_DOCUMENT_IN_FOLDER_STATEMENT = "INSERT INTO RELATION_DOCUMENT_IN_FOLDER (SOURCE_ID_FK,TARGET_ID_FK) VALUES (?,?)";

	public static final String GET_FOLDER_DETAILS = "SELECT * FROM FOLDER WHERE FOLDER_ID=?";

	public static PreparedStatement insertFolderStatement(Folder folder, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(INSERT_FOLDER_STATEMENT);

		prepSt.setString(1, folder.getName());
		prepSt.setDate(2, folder.getCreation_date());
		prepSt.setString(3, folder.getOwner());
		prepSt.setInt(4, folder.getChild_nr());
		prepSt.setString(5, folder.getFolder_type());
		prepSt.setDate(6, folder.getModify_date());

		return prepSt;
	}

	/**
	 * 
	 * @param folder   the updated folder; it was updated in the controller
	 * @param idFolder
	 * @param conn
	 * @return prepared statement
	 * @throws SQLException
	 */
	public static PreparedStatement updateFolderStatement(Folder folder, int idFolder, Connection conn)
			throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(UPDATE_FOLDER_DETAILS);

		prepSt.setInt(1, folder.getChild_nr());
		prepSt.setDate(2, folder.getModify_date());
		prepSt.setInt(3, folder.getFolder_id());

		return prepSt;
	}

	public static PreparedStatement selectAllFolders(Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(SELECT_ALL_FOLDERS);
		return prepSt;
	}

	public static PreparedStatement getFolderByIdStatement(int id, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_FOLDER_DETAILS);
		prepSt.setInt(1, id);
		return prepSt;
	}
}
