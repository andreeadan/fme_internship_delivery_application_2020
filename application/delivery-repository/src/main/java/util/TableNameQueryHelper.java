package util;

public enum TableNameQueryHelper {

	ADMIN("DELETE * FROM ADMIN"), CASHIER("DELETE * FROM CASHIER"), CLIENT("DELETE * FROM CLIENT"),
	DRIVER("DELETE * FROM DRIVER");

	private final String query;

	TableNameQueryHelper(final String query) {
		this.query = query;
	}

	public String getQuery() {
		return query;
	}
}
