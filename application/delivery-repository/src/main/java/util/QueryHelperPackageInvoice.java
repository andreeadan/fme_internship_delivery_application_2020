package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import ro.fme.internship.delivery.model.PackageInvoice;

public class QueryHelperPackageInvoice {

	public static final String INSERT_PACKAGE_INVOICE_STATEMENT = "INSERT INTO PACKAGE_INVOICE (RETURN_ID, EXP_CLIENT_ID, DEST_CLIENT_ID, ORDER_DESCRIPTION, "
			+ "ORDER_COST,CREATION_DATE, ORDER_STATUS, ORDER_WEIGHT, DEPOSIT_ID, DELIVERY_DATE, MODIFY_DATE, CONTENT_TYPE, CONTENT_LOCATION, CASHIER_ID) "
			+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	public static final String UPDATE_PACKAGE_INVOICE = "UPDATE PACKAGE_INVOICE SET RETURN_ID=?, ORDER_DESCRIPTION=?, ORDER_COST=?, DEPOSIT_ID=?, DELIVERY_DATE=?, ORDER_STATUS=?, MODIFY_DATE=? WHERE INVOICE_ID=?";

	public static final String SELECT_ALL_PACKAGE_INVOICES = "SELECT * FROM PACKAGE_INVOICE";

	public static final String SELECT_PACKAGE_INVOICE_BY_ID = "SELECT * FROM PACKAGE_INVOICE WHERE INVOICE_ID=?";

	public static PreparedStatement getPackageInvoiceStatement(PackageInvoice invoice, Connection conn)
			throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(INSERT_PACKAGE_INVOICE_STATEMENT);
		prepSt.setInt(1, invoice.getReturn_id());
		prepSt.setInt(2, invoice.getExp_client_id());
		prepSt.setInt(3, invoice.getDest_client_id());
		prepSt.setString(4, invoice.getOrder_description());
		prepSt.setLong(5, (long) invoice.getOrder_cost());
		prepSt.setDate(6, invoice.getCreation_date());
		prepSt.setString(7, invoice.getOrder_status());
		prepSt.setLong(8, (long) invoice.getOrder_weight());
		prepSt.setInt(9, invoice.getDeposit_id());
		prepSt.setDate(10, invoice.getDelivery_date());
		prepSt.setDate(11, invoice.getModify_date());
		prepSt.setString(12, invoice.getContent_type());
		prepSt.setString(13, invoice.getContent_location());
		prepSt.setInt(14, invoice.getCashier_id());

		return prepSt;
	}

	public static PreparedStatement updatePackageInvoiceDetails(PackageInvoice package_invoice, int id, Connection conn)
			throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(UPDATE_PACKAGE_INVOICE);
		prepSt.setInt(1, package_invoice.getReturn_id());
		prepSt.setString(2,  package_invoice.getOrder_description());
		prepSt.setFloat(3, package_invoice.getOrder_cost());
		prepSt.setInt(4, package_invoice.getDeposit_id());
		prepSt.setDate(5, package_invoice.getDelivery_date());
		prepSt.setString(6, package_invoice.getOrder_status());
		prepSt.setDate(7, package_invoice.getModify_date());
		prepSt.setInt(8, id);

		return prepSt;
	}

	public static PreparedStatement selectAllPackageInvoices(Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(SELECT_ALL_PACKAGE_INVOICES);

		return prepSt;
	}

	// prepare statement for getPackageInvoiceById
	public static PreparedStatement getPackageInvoiceByIdStatement(Connection conn, int packageInvoiceId)
			throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(SELECT_PACKAGE_INVOICE_BY_ID);
		prepSt.setInt(1, packageInvoiceId);

		return prepSt;
	}
}
