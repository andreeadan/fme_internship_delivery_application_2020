package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import ro.fme.internship.delivery.model.Admin;

public class QueryHelperAdmin {

	public static final String INSERT_ADMIN_STATEMENT = "INSERT INTO ADMIN (USERNAME, PASSWORD, FIRSTNAME, LASTNAME) VALUES (?, ?, ?, ?)";

	public static final String UPDATE_ADMIN_DETAILS = "UPDATE ADMIN SET FIRSTNAME=?, LASTNAME=?, PASSWORD=? WHERE USERNAME=?";
    
	public static final String UPDATE_ADMIN_USER = "UPDATE ADMIN SET FIRSTNAME=?, LASTNAME=?, PASSWORD=?, USERNAME=? WHERE ADMIN_ID=?";

	public static final String DELETE_ADMIN_STATEMENT = "UPDATE ADMIN SET USERNAME = NULL, PASSWORD = NULL, FIRSTNAME = NULL, LASTNAME = NULL "
			+ "WHERE USERNAME = ?";

	public static final String GET_ADMIN_DETAILS = "SELECT * FROM ADMIN WHERE ADMIN_ID=?";
	
	public static final String GET_ADMIN_BY_USERNAME_AND_PASSWORD = "(SELECT * FROM ADMIN WHERE USERNAME=? AND PASSWORD=?)";
	
	public static final String GET_ADMIN_BY_USERNAME = "SELECT * FROM ADMIN WHERE USERNAME=?";

	public static PreparedStatement getAdminUserByUsername(String username, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_ADMIN_BY_USERNAME);
		prepSt.setString(1, username);

		return prepSt;
	}
	public static PreparedStatement getAdminUserByUsernameAndPassword(String username, String password, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_ADMIN_BY_USERNAME_AND_PASSWORD);
		prepSt.setString(1, username);
		prepSt.setString(2, password);

		return prepSt;
	}

	public static final String GET_ADMINS = "SELECT * FROM ADMIN";

	public static PreparedStatement getAdminUsersStatement(Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_ADMINS);

		return prepSt;
	}

	public static PreparedStatement getAdminStatement(Admin user, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(INSERT_ADMIN_STATEMENT);
		prepSt.setString(1, user.getUsername());
		prepSt.setString(2, user.getPassword());
		prepSt.setString(3, user.getFirstName());
		prepSt.setString(4, user.getLastName());
		
		return prepSt;
	}

	public static PreparedStatement deleteAdminStatement(Admin user, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(DELETE_ADMIN_STATEMENT);
		prepSt.setString(1, user.getUsername());
		return prepSt;
	}

	public static PreparedStatement updateAdminStatement(Admin user, String username, Connection conn)
			throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(UPDATE_ADMIN_DETAILS);
		prepSt.setString(1, user.getFirstName());
		prepSt.setString(2, user.getLastName());
		prepSt.setString(3, user.getPassword());
		prepSt.setString(4, username);
		return prepSt;
	}
	
	public static PreparedStatement updateAdminUserStatement(Admin user,int admin_id, Connection conn)
			throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(UPDATE_ADMIN_USER);
		prepSt.setString(1, user.getFirstName());
		prepSt.setString(2, user.getLastName());
		prepSt.setString(3, user.getPassword());
		prepSt.setString(4, user.getUsername());
		prepSt.setInt(5, admin_id);

		return prepSt;
	}

	public static PreparedStatement deleteAllRecords(String query, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(query);
		return prepSt;
	}

	public static PreparedStatement getAdminByIdStatement(int id, Connection conn) throws SQLException {
		PreparedStatement prepSt = conn.prepareStatement(GET_ADMIN_DETAILS);
		prepSt.setInt(1, id);
		return prepSt;
	}
}