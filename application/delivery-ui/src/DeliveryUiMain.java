import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import ro.fme.internship.delivery.model.Admin;
import ro.fme.internship.delivery.model.Cashier;
import ro.fme.internship.delivery.model.Client;
import ro.fme.internship.delivery.model.Deposit;
import ro.fme.internship.delivery.model.Driver;
import ro.fme.internship.delivery.model.Folder;
import ro.fme.internship.delivery.model.PackageInvoice;
import ro.fme.internship.delivery.model.User;
import ro.fme.internship.delivery.repository.DepositRepository;
import ro.fme.internship.delivery.repository.FolderRepository;
import ro.fme.internship.delivery.repository.PackageInvoiceRepository;
import ro.fme.internship.delivery.repository.RelationFolderInFolderRepository;
import ro.fme.internship.delivery.repository.UserRepository;
import ro.fme.internship.delivery.repository.interfaces.IUserRepository;
import ro.fme.internship.delivery.services.DepositService;
import ro.fme.internship.delivery.services.FolderService;
import ro.fme.internship.delivery.services.PackageInvoiceService;
import ro.fme.internship.delivery.services.RelationFolderInFolderService;
import ro.fme.internship.delivery.services.UserService;

public class DeliveryUiMain {
	
	public static void main(String[] args) throws Exception {
		
		UserRepository userRepository=new UserRepository();
		UserService userService = new UserService(userRepository);
		
		User user1=new Admin("user1","pass1","Ion","Popescu");
		userService.createUser(user1);
		
		User user2=new Cashier("user2","pass2","Maria","Pop");
		userService.createUser(user2);
		
		User user3=new Client("user3","pass3","Liana","Turcu","str.Mircea Eliade", +740901200,405100, "Cluj-Napoca","Cluj");
		userService.createUser(user3);
        
		User user4=new Driver("user4","pass4","Mihai","Eminescu","Tesla");
		userService.createUser(user4);
		
		DepositRepository depositRepository=new DepositRepository();
		DepositService depositService = new DepositService(depositRepository);
		
		Deposit deposit = new Deposit("Cluj","Str.Dimitrie Bolitneanu nr 12","activ");
		depositService.createDeposit(deposit);
		
		FolderRepository folderRepository=new FolderRepository();
		FolderService folderService = new FolderService(folderRepository);
		
		Date mydate= new Date(2020,11,11);
		Date mycreationdate= new Date(2020,11,10);
		Folder folder = new Folder("2020",mycreationdate,"Ion Popescu",3,mydate,"intern");
		folderService.createFolder(folder);
		
		PackageInvoiceRepository packageInvoiceRepository=new PackageInvoiceRepository();
		PackageInvoiceService packageInvoiceService = new PackageInvoiceService(packageInvoiceRepository);
		
		Date d1=new Date(2020, 9, 9);
		Date d2=new Date(2020, 8, 9);
		Date d3=new Date(2020, 10, 10);
		PackageInvoice pack = new PackageInvoice(1, 1, 1, "description",(long) 100, d1, "delivered", (long) 100, 1,d2, d3, "books", "Cluj-Napoca", 1);
		
		packageInvoiceService.createPackageInvoice(pack);
		
	}

	

}
