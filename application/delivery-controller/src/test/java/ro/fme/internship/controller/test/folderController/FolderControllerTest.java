package ro.fme.internship.controller.test.folderController;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Folder;
import ro.fme.internship.delivery.repository.IFolderRepository;
import ro.fme.internship.delivery.repository.implementation.FolderRepository;
import ro.fme.internship.delivery.repository.test.FolderRepositoryTest.FolderRepositoryTest;
import ro.fme.internship.delivery.service.implementation.FolderService;

public class FolderControllerTest {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FolderRepositoryTest.class);
	IFolderRepository folderRepository = new FolderRepository();
	FolderService service ;

	/**
	 * set data for tests
	 */
	@Before
	public void setUp() {
		service = new FolderService(folderRepository);
	}
	
	
	/**
	 * test if a folder was found in Folder table
	 */
	
	@Test
	public void testGetFolderById() {
		LOGGER.info("\ntestGetFolderById started...");
		
		Folder folder;
		int id=1;
		folder=service.getFolder(id);
		
		LOGGER.info("test if the folder  was found in db");
		assertFalse(folder!=null);
		
		LOGGER.info("test if the folder found in db has the same id");
		assertEquals(folder.getFolder_id(),id);

		LOGGER.info("testGetFolderById finished...\n");
	}

}
