package ro.fme.internship.controller.test.DepositControllerTest;
import static org.junit.Assert.assertNotEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Deposit;
import ro.fme.internship.delivery.service.IDepositService;
import ro.fme.internship.delivery.service.implementation.DepositService;

public class DepositServiceTest {
	
	private static DepositService depositService;
	private static final Logger LOGGER = LoggerFactory.getLogger(DepositServiceTest.class);

	@BeforeClass
	public static void setUp() {
		depositService = new DepositService();
	}
	
	@Test
	public void testGetDepositService() {
		LOGGER.info("\ntestGetDepositService started...");
		
		LOGGER.info("perform get...");
		Deposit deposit = depositService.getDeposit(1);
		
		assertNotEquals(null, deposit);
		LOGGER.info("testGetDepositService... \n");
	}
}
