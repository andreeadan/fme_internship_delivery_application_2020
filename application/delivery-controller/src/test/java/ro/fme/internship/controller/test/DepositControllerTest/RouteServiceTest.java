package ro.fme.internship.controller.test.DepositControllerTest;

import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Route;
import ro.fme.internship.delivery.service.IRouteService;
import ro.fme.internship.delivery.service.implementation.RouteService;

public class RouteServiceTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(RouteServiceTest.class);
	private static IRouteService routeService;

	@BeforeClass
	public static void setUp() {
		routeService = new RouteService();
	}

	@Test
	public void testGetRouteByID() {
		LOGGER.info("\ntestGetRouteByID in service started...");

		LOGGER.info("get the route from repository...");
		Route route = routeService.getRouteById(3);

		LOGGER.info("test the values from route fields...");
		assertTrue(route.getIdDeposit() == 1);
		assertTrue(route.getIdDriver() == 1);
		assertTrue(route.getDestCounty().equals("countyDest"));
		assertTrue(route.getExpCounty().equals("countyExp"));

		LOGGER.info("testGetRouteByID in service started...\n");
	}

}
