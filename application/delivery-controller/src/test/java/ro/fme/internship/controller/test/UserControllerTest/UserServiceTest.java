package ro.fme.internship.controller.test.UserControllerTest;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Admin;
import ro.fme.internship.delivery.model.Cashier;
import ro.fme.internship.delivery.model.Client;
import ro.fme.internship.delivery.model.Driver;
import ro.fme.internship.delivery.service.implementation.UserService;

public class UserServiceTest {

	private static UserService userService;
	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceTest.class);
	
	@BeforeClass
	public static void setUp() {
		userService = new UserService();
	}
	
	@Test
	public void testGetDriverService() {
		LOGGER.info("\ntestGetDriverService started...");
		
		LOGGER.info("perform get...");
		Driver driver = userService.getDriver(81);
		
		assertNotEquals(null, driver);
		
		LOGGER.info("testGetDriverService finished...\n");
	}

	
	@Test
	public void testGetClientService() {
		LOGGER.info("\ntestGetClientService started...");
		
		LOGGER.info("perform get...");
		
		Client client = userService.getClient(1);
		
		assertNotEquals(null, client);
		
		LOGGER.info("testGetClientService finished...\n");
	}
	/**
	 * test getAdmin from Service
	 */
	@Test
	public void testGetAdminService() {
		LOGGER.info("\ntestGetAdminService started...");	
		
		LOGGER.info("perform get...");
		
		Admin admin = userService.getAdmin(4);
		
		assertNotEquals(null, admin);
		
		LOGGER.info("test if password is correct...");
		assertTrue(admin.getPassword().equals("a"));
		
		LOGGER.info("testGetAdminService finished...\n");
	}
	/**
	 * test getCashier from Service
	 */
	@Test
	public void testGetCashierService() {
		LOGGER.info("\ntestGetCashierService started...");
		
		LOGGER.info("perform get...");
		
		Cashier cashier = userService.getCashier(61);
		
		assertNotEquals(null, cashier);
		
		LOGGER.info("testGetCashierService finished...\n");
	}

}
