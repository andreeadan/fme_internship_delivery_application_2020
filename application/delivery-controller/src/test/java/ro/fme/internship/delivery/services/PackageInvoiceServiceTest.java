package ro.fme.internship.delivery.services;

import static org.junit.Assert.*;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Folder;
import ro.fme.internship.delivery.model.PackageInvoice;
import ro.fme.internship.delivery.repository.implementation.PackageInvoiceRepository;
import ro.fme.internship.delivery.repository.test.PackageInvoiceRepositoyTest.PackageInvoiceRepositoryTest;
import ro.fme.internship.delivery.service.implementation.PackageInvoiceService;

public class PackageInvoiceServiceTest {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PackageInvoiceRepositoryTest.class);
	PackageInvoiceRepository packRepo;
	PackageInvoiceService packInvServ;
	
	@Before
	public void setUp() {
		packRepo= new PackageInvoiceRepository();
		packInvServ= new PackageInvoiceService(packRepo);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testGetPackageInvoiceById() {
		LOGGER.info("\n testGetPackageInvoiceById started...");
		Date d1 = new Date(2020, 9, 9);
		Date d2 = new Date(2020, 8, 9);
		Date d3 = new Date(2020, 10, 10);
		
		LOGGER.info("Creating a new object of type PackageInvoice...");
		PackageInvoice pack = new PackageInvoice(20, 67, 68, "description2", 100, d1, "delivered", 100, 1, d2, d3, "books", "Cluj-Napoca", 61);
		pack.setInvoice_id(6);
		
		LOGGER.info("Comparing the newly created object with the one retrieved from the database by using the service...");
		assertEquals(pack.toString(), packInvServ.getPackageInvoiceById(6).toString());
		
		LOGGER.info("Verify that the object retrieved from the DB is not null...");
		assertNotEquals(packRepo.getPackageInvoiceById(6), null);
		
		LOGGER.info("testGetPackageInvoiceById finished...\n");
	}
	
	@Test
	public void testGetInvoicesByRecord() {
		LOGGER.info("\n testGetInvoicesByRecord started...");
		
		LocalDate date = LocalDate.of(2020, 9,8);
		LOGGER.info("perform get...");
		ArrayList<PackageInvoice> packageInvoices = packInvServ.getInvoicesByRecord(date, 1);
		LOGGER.info("test if the list is null...");
		assertNotNull(packageInvoices);
		LOGGER.info("test if the list size is not the wrong size");
		assertNotEquals(2, packageInvoices.size());
		LOGGER.info("test if the the list is the right size");
		assertEquals(1, packageInvoices.size());
		LOGGER.info("test if the element of the list is correct");
		assertEquals("processing",packageInvoices.get(0).getOrder_status());
		
		LOGGER.info("testGetInvoicesByRecord finished...\n");
	}
}
