package util;

import java.sql.Date;
import java.time.LocalDate;

public class ServiceHelper {

	public Date toSqlDate(LocalDate date) {
		return Date.valueOf(date);
	}

	public LocalDate toLocalDate(Date date) {
		return date.toLocalDate();
	}

}
