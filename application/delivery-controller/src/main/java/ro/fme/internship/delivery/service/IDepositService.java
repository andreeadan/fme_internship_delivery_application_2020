package ro.fme.internship.delivery.service;

import java.util.ArrayList;

import ro.fme.internship.delivery.model.Deposit;

public interface IDepositService {

	public Deposit getDeposit(int id);

	public Boolean createDeposit(Deposit deposit);

	public Boolean updateDeposit(Deposit deposit, int id);
	
	public ArrayList<Deposit> getAllDeposits();

}
