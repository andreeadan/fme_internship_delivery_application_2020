package ro.fme.internship.delivery.service.plugin;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.DottedLineSeparator;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;

import ro.fme.internship.delivery.model.Cashier;
import ro.fme.internship.delivery.model.Client;
import ro.fme.internship.delivery.model.Deposit;
import ro.fme.internship.delivery.model.PackageInvoice;

public class GeneratePackageInvoicePDF {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GeneratePackageInvoicePDF.class);
	private final String TEMP_LOCATION="//temp//exports";
	private final  String fileName="packageInvoice_";
	private static final Font timesRoman = new Font(Font.FontFamily.TIMES_ROMAN,16,Font.BOLDITALIC);
	private static final Font timesRoman_2 = new Font(Font.FontFamily.TIMES_ROMAN,9,Font.NORMAL);
	private static final Font tableFont= new Font(Font.FontFamily.TIMES_ROMAN,12,Font.BOLD);
	
	public void generatePDF(PackageInvoice packInv, Client exp, Client dest, Cashier cashier, Deposit dep) {
		
	    String sysEnvPath = System.getenv("CATALINA_HOME");
	    String exportFolder= sysEnvPath.concat(TEMP_LOCATION);
	    
	    File directory= new File(exportFolder);
	    
	    if(!exists(directory)) {
	    	directory.mkdir();
	    }
	    
	    File newFile=new File(exportFolder+"/"+fileName+packInv.getInvoice_id()+".pdf");
		writePDFDocument(newFile, packInv, exp, dest, cashier, dep);
	}

	private void insertHeader(PdfPTable table, String text, int align, int colspan, Font font){
		
		  LOGGER.info("Create a new cell and set a specified text and font to it");
		  PdfPCell cell = new PdfPCell(new Phrase(text.toString().trim(), font));
		  LOGGER.info("Set the alignment to the cell");
		  cell.setHorizontalAlignment(align);
		  LOGGER.info("Set the cell column span in case you want to merge more cells than 1");
		  cell.setColspan(colspan);
		  LOGGER.info("Instruction for creating an empty row");
		  if(text.trim().equalsIgnoreCase("")){
		   cell.setMinimumHeight(10f);
		  }
		  LOGGER.info("Adding cell to the table");
		  table.addCell(cell);
	}
	
	private Paragraph insertColumn(Object o, Font timesRoman_2) {
		
		LOGGER.info("Creating paragraphs for the table in document...");
		Paragraph tablePara= new Paragraph();
		Phrase tablePhrase= new Phrase();
		
		if(o instanceof Client) {
			Chunk firstname= new Chunk("NAME: "+((Client)o).getFirstName()+" "+((Client)o).getLastName());
			Chunk address = new Chunk("STREET: "+((Client)o).getAddress());
			Chunk address_2= new Chunk("COUNTY: "+((Client)o).getCity()+", "+((Client)o).getCounty()+", "+((Client)o).getZipCode());
			Chunk phone= new Chunk("PHONE NUMBER: "+((Client)o).getPhoneNumber());
			tablePhrase.add(firstname);
			tablePhrase.add(Chunk.NEWLINE);
			tablePhrase.add(address);
			tablePhrase.add(Chunk.NEWLINE);
			tablePhrase.add(address_2);
			tablePhrase.add(Chunk.NEWLINE);
			tablePhrase.add(phone);
		}
		else if (o instanceof PackageInvoice) {
			Chunk description= new Chunk("DESCRIPTION: "+((PackageInvoice) o).getOrder_description());
			Chunk content= new Chunk("CONTENT TYPE: "+((PackageInvoice) o).getContent_type());
			Chunk price= new Chunk("PRICE: "+((PackageInvoice) o).getOrder_cost());
			
			tablePhrase.add(description);
			tablePhrase.add(Chunk.NEWLINE);
			tablePhrase.add(content);
			tablePhrase.add(Chunk.NEWLINE);
			tablePhrase.add(price);
			
		}
		tablePhrase.add(Chunk.NEWLINE);
		tablePara.add(tablePhrase);
		tablePara.setFont(timesRoman_2);
		
		return tablePara;
	}
	
	public void writePDFDocument(File newFile, PackageInvoice packInv, Client exp, Client dest, Cashier cashier, Deposit dep) {
		Document document=new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream(newFile));
			document.open();
		
			Paragraph title= new Paragraph("Package Invoice Number: "+packInv.getInvoice_id(), timesRoman);
			Paragraph secondTitle= new Paragraph("Return Invoice Number: "+packInv.getReturn_id(), timesRoman);
			title.setAlignment(Element.ALIGN_CENTER);
			secondTitle.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph dateParagraph= new Paragraph();
			Phrase phrase= new Phrase();
			phrase.add(Chunk.NEWLINE);
			Chunk chunk_1= new Chunk("DATE: "+packInv.getCreation_date(), timesRoman_2);
			phrase.add(chunk_1);
			phrase.add(Chunk.NEWLINE);
			Chunk chunk_2= new Chunk("DELIVERY DATE: "+packInv.getDelivery_date(), timesRoman_2);
			phrase.add(chunk_2);
			phrase.add(Chunk.NEWLINE);
			Chunk chunk_3= new Chunk("RETURN DATE: "+packInv.getModify_date(), timesRoman_2);
			phrase.add(chunk_3);
			phrase.add(Chunk.NEWLINE);
			Chunk chunk_4= new Chunk("CURRENT LOCATION: "+packInv.getContent_location(), timesRoman_2);
			phrase.add(chunk_4);
			phrase.add(Chunk.NEWLINE);
			Chunk linebreak = new Chunk(new DottedLineSeparator());
			phrase.add(linebreak);
			phrase.add(Chunk.NEWLINE);
			phrase.add(Chunk.NEWLINE);
			
			float [] columnWidths= {5f, 5f, 5f};
			Paragraph tableParagraph= new Paragraph();
		
			PdfPTable table= new PdfPTable(columnWidths);
			table.setWidthPercentage(90f);
			LOGGER.info("Inserting the headers into the table");
			insertHeader(table, "SENDER", Element.ALIGN_CENTER, 1, tableFont);
			insertHeader(table, "RECEIVER", Element.ALIGN_CENTER, 1, tableFont);
			insertHeader(table, "ORDER DETAILS", Element.ALIGN_CENTER, 1, tableFont);
			table.setHeaderRows(1);
			
			
			LOGGER.info("Inserting columns into the table");
			PdfPCell firstCell = new PdfPCell();
			Paragraph firstRow= insertColumn(exp, timesRoman_2);
			firstCell.addElement(firstRow);
			
			PdfPCell secondCell= new PdfPCell();
			Paragraph secondRow= insertColumn(dest, timesRoman_2); 
			secondCell.addElement(secondRow);
			
			PdfPCell thirdCell= new PdfPCell();
			Paragraph thirdRow= insertColumn(packInv, timesRoman_2); 
			thirdCell.addElement(thirdRow);
			
			table.addCell(firstCell);
			table.addCell(secondCell);
			table.addCell(thirdCell);
			
			Phrase newLine= new Phrase();
			Chunk newLineChunk= new Chunk(new DottedLineSeparator());
			newLine.add(newLineChunk);
			
			Paragraph otherDetailsParagraph= new Paragraph();
			Phrase otherDetailsphrase= new Phrase();
			otherDetailsphrase.add(Chunk.NEWLINE);
			Chunk firstDetailsChunk= new Chunk("CASHIER: "+cashier.getFirstName()+" "+cashier.getLastName(), timesRoman_2);
			otherDetailsphrase.add(firstDetailsChunk);
			otherDetailsphrase.add(Chunk.NEWLINE);
			Chunk secondDetailsChunk= new Chunk("DEPOSIT: "+dep.getAddress(), timesRoman_2);
			otherDetailsphrase.add(secondDetailsChunk);
			otherDetailsphrase.add(Chunk.NEWLINE);
			Chunk thirdDetailsChunk= new Chunk("PACKAGE STATUS: "+packInv.getOrder_status(), timesRoman_2);
			otherDetailsphrase.add(thirdDetailsChunk);
			otherDetailsphrase.add(Chunk.NEWLINE);
			otherDetailsphrase.add(Chunk.NEWLINE);
			Chunk expSignature= new Chunk("SENDER SIGNATURE: ", timesRoman_2);
			otherDetailsphrase.add(expSignature);
			Chunk casSignature= new Chunk("CASHIER SIGNATURE: ", timesRoman_2);
			Chunk glue = new Chunk(new VerticalPositionMark());
			otherDetailsphrase.add(glue);
			otherDetailsphrase.add(casSignature);
			otherDetailsParagraph.add(otherDetailsphrase);
				     
			LOGGER.info("Adding all the paragraphs to the document");
			tableParagraph.add(table);
			dateParagraph.add(phrase);
			document.add(title);
			document.add(secondTitle);
			document.add(dateParagraph);
			document.add(tableParagraph);
			document.add(newLine);
			document.add(otherDetailsParagraph);
		
			document.close();
		} catch (FileNotFoundException e) {
			LOGGER.info("Error:"+e);
		} catch (DocumentException e) {
			LOGGER.info("Error:"+e);
		}
	}
	
	public boolean exists(File directory) {
		return directory.exists();
	}
	
    public Calendar transformDateToCalendar(Date date) {
    	Calendar cal= new GregorianCalendar();
    	cal.setTime(date);
		return cal;
	}
}
