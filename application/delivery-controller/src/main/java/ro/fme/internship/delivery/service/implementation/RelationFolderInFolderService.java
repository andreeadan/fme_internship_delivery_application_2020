package ro.fme.internship.delivery.service.implementation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Folder;
import ro.fme.internship.delivery.repository.implementation.RelationFolderInFolderRepository;
import ro.fme.internship.delivery.service.IRelationFolderInFolderService;

public class RelationFolderInFolderService implements IRelationFolderInFolderService {

	private RelationFolderInFolderRepository relationFolderInFolderRepository = new RelationFolderInFolderRepository();
	private static final Logger LOGGER = LoggerFactory.getLogger(RelationFolderInFolderService.class);

	public RelationFolderInFolderService() {
	}

	public RelationFolderInFolderService(RelationFolderInFolderRepository relationFolderInFolderRepository) {
		this.relationFolderInFolderRepository = relationFolderInFolderRepository;
	}

	/**
	 * Connects the Web Service with the Repository Insert a folder in folder
	 * relation into the database
	 * 
	 * @param sourceFolder: Folder, targetFolder: Folder
	 * @return Boolean
	 */
	@Override
	public Boolean createRelationFolderInFolderRepository(Folder sourceFolder, Folder targetFolder) {
		LOGGER.debug("Enter createRelationFolderInFolderRepository in the controller");
		int response = relationFolderInFolderRepository.insertRelationFolderInFolder(sourceFolder, targetFolder);
		LOGGER.debug("Leave createRelationFolderInFolderRepository in the controller");
		if (response == 1)
			return true;
		else
			return false;
	}
}
