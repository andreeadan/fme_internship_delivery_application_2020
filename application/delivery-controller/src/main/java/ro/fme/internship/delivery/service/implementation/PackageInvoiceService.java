package ro.fme.internship.delivery.service.implementation;

import java.time.LocalDate;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Cashier;
import ro.fme.internship.delivery.model.Client;
import ro.fme.internship.delivery.model.Deposit;
import ro.fme.internship.delivery.model.Folder;
import ro.fme.internship.delivery.model.PackageInvoice;
import ro.fme.internship.delivery.model.RelationFolderInFolder;
import ro.fme.internship.delivery.repository.IPackageInvoiceRepository;
import ro.fme.internship.delivery.repository.IUserRepository;
import ro.fme.internship.delivery.repository.implementation.DepositRepository;
import ro.fme.internship.delivery.repository.implementation.FolderRepository;
import ro.fme.internship.delivery.repository.implementation.PackageInvoiceRepository;
import ro.fme.internship.delivery.repository.implementation.RelationDocumentInFolderRepository;
import ro.fme.internship.delivery.repository.implementation.RelationFolderInFolderRepository;
import ro.fme.internship.delivery.repository.implementation.UserRepository;
import ro.fme.internship.delivery.service.IPackageInvoiceService;
import ro.fme.internship.delivery.service.plugin.GeneratePackageInvoicePDF;

public class PackageInvoiceService implements IPackageInvoiceService {
	private IUserRepository userRepository = new UserRepository();
	private IPackageInvoiceRepository packageInvoiceRepository = new PackageInvoiceRepository();
	private RelationFolderInFolderRepository folderInFolderRepo = new RelationFolderInFolderRepository();
	private RelationDocumentInFolderRepository documentInFolderRepo = new RelationDocumentInFolderRepository();
	private FolderRepository folderRepository = new FolderRepository();
	private UserRepository repository= new UserRepository();
	private DepositRepository depRepository= new DepositRepository();
	private GeneratePackageInvoicePDF util= new GeneratePackageInvoicePDF();
	private static final Logger LOGGER = LoggerFactory.getLogger(PackageInvoiceService.class);

	public PackageInvoiceService() {
	}

	public PackageInvoiceService(IPackageInvoiceRepository packageInvoiceRepository) {
		this.packageInvoiceRepository = packageInvoiceRepository;

	}

	/**
	 * get invoice by client and date find a folder record given by date and client
	 * call invoice repository of get_invoice_by_folder
	 */

	/**
	 * 
	 * @param date
	 * @param clientId
	 * @return all of a client's invoices for a given date
	 */

	@Override
	public ArrayList<PackageInvoice> getInvoicesByRecord(LocalDate date, int clientId) {
		LOGGER.debug("Enter getInvoicesByRecord in the controller");
		// initialize variables needed
		ArrayList<PackageInvoice> invoices = new ArrayList<>();
		ArrayList<RelationFolderInFolder> relationsFromMonthDayFolder = null;
		Folder recordRepoFolder = new Folder();
		Folder clientFolder = new Folder();

		// split the year and month-day because they represent folder names
		String year = String.valueOf(date.getYear());

		String monthDay = String.valueOf(date.getMonthValue() + "-" + date.getDayOfMonth());

		// search by year in the folder repository
		Folder yearFolderId = folderRepository.getFolderByName(year);

		if (yearFolderId == null)
			LOGGER.error("Error in InvoiceService, there is no folder with the given year!");
		else {
			// search in relation-folder-in-folder repository the monthDay-year combo
			ArrayList<RelationFolderInFolder> relationsFromYearFolder = folderInFolderRepo
					.getRelationsOfTargetFolder(yearFolderId);

			if (relationsFromYearFolder == null)
				LOGGER.error("Error in InvoiceService, there are no folder relations for the given year!");
			else {
				// check each folder form the array with the required name - monthDay
				for (RelationFolderInFolder relation : relationsFromYearFolder) {

					Folder possibleSource = folderRepository.getFolderWithId(relation.getSourceFolderID());
					// if possible folder is null
					if (possibleSource == null) {
						LOGGER.error("Error in InvoiceService, there is no intern folder for the given id!");
					} else if (possibleSource.getName().equals(monthDay)) {
						recordRepoFolder = possibleSource;
						// all folders in relation with the found record repository of the given date
						relationsFromMonthDayFolder = folderInFolderRepo.getRelationsOfTargetFolder(recordRepoFolder);
					}

				}

				if (relationsFromMonthDayFolder == null)
					LOGGER.error("Error in InvoiceService, there are no folder relations for the given month-day!");
				else {
					// check each folder form the array with the required name - clientId
					for (RelationFolderInFolder relation : relationsFromMonthDayFolder) {
						Folder possibleSource = folderRepository.getFolderWithId(relation.getSourceFolderID());
						if (possibleSource == null) {
							LOGGER.error("Error in InvoiceService, there is no record folder for the given id!");
						} else {
							if (possibleSource.getName().equals(String.valueOf(clientId))) {
								clientFolder = possibleSource;
								// all documents in relation with the found record repository of the given
								// clientFolder
								invoices = documentInFolderRepo.getInvoicesOfTargetFolder(clientFolder);
							} else {
								LOGGER.error(
										"Error in PackageInvoiceService, there are no documents relations for the given clientFolder!");
							}
						}
					}
				}
			}
		}
		LOGGER.debug("Leave getInvoicesByRecord in the controller");
		return invoices;
	}

	/**
	 * Connects the Web Service with the Repository Insert a package invoice into
	 * the database
	 * 
	 * @param packageInvoice: PackageInvoice
	 * @return Boolean
	 */
	@Override
	public Boolean createPackageInvoice(PackageInvoice packageInvoice) {
		LOGGER.debug("Enter createPackageInvoice in the controller");
		int response = packageInvoiceRepository.insertPackageInvoice(packageInvoice);
		LOGGER.debug("Leave createPackageInvoice in the controller");
		if (response == 1)
			return true;
		else
			return false;
	}

	/**
	 * Connects the Web Service with the Repository Get the package invoice by given
	 * id
	 * 
	 * @param id: integer
	 * @return packageInvoice: PackageInvoice
	 */
	@Override
	public PackageInvoice getPackageInvoiceById(int id) {
		LOGGER.debug("Enter getPackageInvoiceById in the controller");
		PackageInvoice packageInvoice = packageInvoiceRepository.getPackageInvoiceById(id);
		LOGGER.debug("Leave getPackageInvoiceById in the controller");
		return packageInvoice;
	}

	/**
	 * Connects the Web Service with the Repository Get all the existing package
	 * invoices from the database
	 * 
	 * @return ArrayList<PackageInvoice>
	 */
	@Override
	public ArrayList<PackageInvoice> getPackageInvoices() {
		LOGGER.debug("Return getPackageInvoices in the controller");
		return packageInvoiceRepository.getPackageInvoices();
	}

	@Override
	public void generatePDF(PackageInvoice packInv) {
		Client exp = repository.getClient(packInv.getExp_client_id());
		Client dest= repository.getClient(packInv.getDest_client_id());
		Cashier cashier= repository.getCashier(packInv.getCashier_id());
		Deposit dep= depRepository.getDeposit(packInv.getDeposit_id());
		util.generatePDF(packInv, exp, dest, cashier, dep);
	}

	/**
	 * get all received packageInvoices by client id
	 * @param username :String 
	 * @return receivedInvoices : ArrayList<PackageInvoice>  
	 */
	public ArrayList<PackageInvoice> getReceivedInvoices(String username) {
		LOGGER.debug("Enter getSentInvoices");
		int id = userRepository.getClientIdByUsername(username);
		ArrayList<PackageInvoice> receivedInvoices = new ArrayList<PackageInvoice>();
		ArrayList<PackageInvoice> allInvoices = packageInvoiceRepository.getPackageInvoices();
		for (PackageInvoice packageInvoice : allInvoices) {
			if(packageInvoice.getDest_client_id() == id ) {
				receivedInvoices.add(packageInvoice);
			}
		}
		LOGGER.debug("Leave getSentInvoices");
		return receivedInvoices;
	}
	
	/**
	 * get all sent packageInvoices by client id
	 * @param username :String  
	 * @return sentInvoices : ArrayList<PackageInvoice>  
	 */
	public ArrayList<PackageInvoice> getSentInvoices(String username) {
		LOGGER.debug("Enter getSentInvoices");
		int id = userRepository.getClientIdByUsername(username);
		ArrayList<PackageInvoice> sentInvoices = new ArrayList<PackageInvoice>();
		ArrayList<PackageInvoice> allInvoices = packageInvoiceRepository.getPackageInvoices();
		for (PackageInvoice packageInvoice : allInvoices) {
			if(packageInvoice.getExp_client_id() == id ) {
				sentInvoices.add(packageInvoice);
			}
		}
		LOGGER.debug("Leave getSentInvoices");
		return sentInvoices;
		
	}

	/**
	 * get all invoices received/sent of a client
	 * @param username :String 
	 * @return clientsInvoices : ArrayList<PackageInvoice> 
	 */	
	public ArrayList<PackageInvoice> getClientsInvoices(String username){
		LOGGER.debug("Enter getClientsInvices");
		int id = userRepository.getClientIdByUsername(username);
		ArrayList<PackageInvoice> clientsInvoices = new ArrayList<PackageInvoice>();
		ArrayList<PackageInvoice> allInvoices = packageInvoiceRepository.getPackageInvoices();
		for (PackageInvoice packageInvoice : allInvoices) {
			if(packageInvoice.getExp_client_id() == id || packageInvoice.getDest_client_id() == id  ) {
				clientsInvoices.add(packageInvoice);
			}
		}
		LOGGER.debug("Leave getClientsInvices");
		return clientsInvoices;
	}
	
	@Override
	public Boolean updatePackageInvoice(PackageInvoice invoice, int id) {
		LOGGER.debug("Enter updatePackageInvoice in the controller");
		int response = packageInvoiceRepository.updatePackageInvoice(invoice, id);
		LOGGER.debug("Leave updatePackageInvoice in the controller");
		if (response == 1)
			return true;
		else
			return false;
	}
}
