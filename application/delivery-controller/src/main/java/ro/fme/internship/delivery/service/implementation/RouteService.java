package ro.fme.internship.delivery.service.implementation;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Route;
import ro.fme.internship.delivery.repository.IDepositRepository;
import ro.fme.internship.delivery.repository.IRouteRepository;
import ro.fme.internship.delivery.repository.IUserRepository;
import ro.fme.internship.delivery.repository.implementation.DepositRepository;
import ro.fme.internship.delivery.repository.implementation.RouteRepository;
import ro.fme.internship.delivery.repository.implementation.UserRepository;
import ro.fme.internship.delivery.service.IRouteService;

public class RouteService implements IRouteService {

	private IRouteRepository routeRepository = new RouteRepository();
	private static final Logger LOGGER = LoggerFactory.getLogger(RouteService.class);
	private IUserRepository userRepository = new UserRepository();
	private IDepositRepository depositRepository = new DepositRepository();

	public RouteService() {
	}

	public RouteService(RouteRepository routeRepository) {
		this.routeRepository = routeRepository;
	}

	/**
	 * Connects the Web Service with the Repository Get the route by given id
	 * 
	 * @param id: integer
	 * @return route: Route
	 */
	@Override
	public Route getRouteById(int id) {
		LOGGER.debug("Enter getRouteById in the controller");
		Route route;
		route = routeRepository.getRouteByID(id);
		LOGGER.debug("Leave getRouteById in the controller");
		return route;
	}

	/**
	 * Connects the Web Service with the Repository Get all the existing routes from
	 * the database
	 * 
	 * @return ArrayList<Route>
	 */
	@Override
	public ArrayList<Route> getAllRoutes() {
		LOGGER.debug("Return getAllRoutes in the controller");
		return routeRepository.getAllRoutes();
	}

	public ArrayList<Route> getRouteByDriverId(int idDriver) {
		return routeRepository.getAllRoutesByDriverID(idDriver);
	}

	@Override
	public boolean createRoute(String expCounty, String destCounty, int driverID) {
		LOGGER.debug("Enter createRoute in controller");
		Route route = new Route();
		route.setDestCounty(destCounty);
		route.setExpCounty(expCounty);
		route.setIdDriver(driverID);
		
		int depositId = depositRepository.getDepositIdByCounty(expCounty);
		route.setIdDeposit(depositId);
		int response = routeRepository.insertRoute(route);
		LOGGER.debug("Leave createRoute in controller");
		if (response == 1)
			return true;
		else
			return false;

	}

}
