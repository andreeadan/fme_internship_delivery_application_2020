package ro.fme.internship.delivery.service;

import java.time.LocalDate;
import java.util.ArrayList;

import ro.fme.internship.delivery.model.PackageInvoice;

public interface IPackageInvoiceService {

	Boolean createPackageInvoice(PackageInvoice packageInvoice);

	public PackageInvoice getPackageInvoiceById(int packageInvoiceId);

	public ArrayList<PackageInvoice> getPackageInvoices();
	
	public void generatePDF(PackageInvoice packageInvoice);

	ArrayList<PackageInvoice> getInvoicesByRecord(LocalDate date, int clientId);

	public ArrayList<PackageInvoice> getReceivedInvoices(String username);
	
	public ArrayList<PackageInvoice> getSentInvoices(String username);

	Boolean updatePackageInvoice(PackageInvoice invoice, int id);
	
	public ArrayList<PackageInvoice> getClientsInvoices(String username);
}
