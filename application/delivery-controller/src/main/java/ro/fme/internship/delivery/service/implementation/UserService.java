package ro.fme.internship.delivery.service.implementation;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Admin;
import ro.fme.internship.delivery.model.Cashier;
import ro.fme.internship.delivery.model.Client;
import ro.fme.internship.delivery.model.Driver;
import ro.fme.internship.delivery.model.PackageInvoice;
import ro.fme.internship.delivery.model.User;
import ro.fme.internship.delivery.repository.IPackageInvoiceRepository;
import ro.fme.internship.delivery.repository.IUserRepository;
import ro.fme.internship.delivery.repository.implementation.PackageInvoiceRepository;
import ro.fme.internship.delivery.repository.implementation.UserRepository;
import ro.fme.internship.delivery.service.IUserService;

public class UserService implements IUserService {

	private IUserRepository userRepository = new UserRepository();
	private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

	public UserService() {
	}

	public UserService(IUserRepository userRepository) {
		this.userRepository = userRepository;
	}

	/**
	 * Connects the Web Service with the Repository Get the driver by given id
	 * 
	 * @param id: integer
	 * @return driver: Driver
	 */
	@Override
	public Driver getDriver(int id) {
		LOGGER.debug("Enter getDriver in the controller");
		Driver driver = new Driver();
		driver = userRepository.getDriver(id);
		LOGGER.debug("Leave getDriver in the controller");
		if (driver == null)
			return null;
		else
			return driver;
	}

	/**
	 * Connects the Web Service with the Repository Get the admin by given id
	 * 
	 * @param id: integer
	 * @return admin: Admin
	 */
	@Override
	public Admin getAdmin(int id) {
		LOGGER.debug("Enter getAdmin in the controller");
		Admin admin = new Admin();
		admin = userRepository.getAdmin(id);
		LOGGER.debug("Leave getAdmin in the controller");
		if (admin == null)
			return null;
		else
			return admin;
	}

	/**
	 * Connects the Web Service with the Repository Get the client by given id
	 * 
	 * @param id: integer
	 * @return client: Client
	 */
	@Override
	public Client getClient(int id) {
		LOGGER.debug("Enter getClient in the controller");
		Client client = new Client();
		client = userRepository.getClient(id);
		LOGGER.debug("Leave getClient in the controller");
		if (client == null)
			return null;
		else
			return client;
	}

	/**
	 * Connects the Web Service with the Repository Get the cashier by given id
	 * 
	 * @param id: integer
	 * @return cashier: Cashier
	 */
	@Override
	public Cashier getCashier(int id) {
		LOGGER.debug("Enter getCashier in the controller");
		Cashier cashier = userRepository.getCashier(id);
		LOGGER.debug("Leave getCashier in the controller");
		return cashier;
	}

	/**
	 * Connects the Web Service with the Repository Insert a user into the database
	 * 
	 * @param user: User
	 * @return Boolean
	 */
	@Override
	public Boolean createUser(User user) {
		LOGGER.debug("Enter createUser in the controller");
		int response = userRepository.insertUser(user);
		LOGGER.debug("Leave createUser in the controller");
		if (response == 1)
			return true;
		else
			return false;
	}

	/**
	 * Connects the Web Service with the Repository Update the user with a given
	 * name
	 * 
	 * @param user: User, username: String
	 * @return Boolean
	 */
	@Override
	public Boolean updateUser(User user, String username) {
		LOGGER.debug("Enter updateUser in the controller");
		int response = userRepository.updateUser(user, username);
		LOGGER.debug("Leave updateUser in the controller");
		if (response == 1)
			return true;
		else
			return false;
	}
	
	/**
	 * Connects the Web Service with the Repository Update the user with a given id
	 * 
	 * 
	 * @param user: User, id: Integer
	 * @return Boolean
	 */
	@Override
	public Boolean updateUserById(User user, int id) {
		LOGGER.debug("Enter updateUserById in the controller");
		int response = userRepository.updateUserById(user, id);
		LOGGER.debug("Leave updateUserById in the controller");
		if (response == 1)
			return true;
		else
			return false;
	}

	/**
	 * Connects the Web Service with the Repository Update a driver's car
	 * 
	 * @param newCar: String, username: String
	 * @return Boolean
	 */
	@Override
	public Boolean updateDriverCar(String newCar, String username) {
		LOGGER.debug("Enter updateDriverCar in the controller");
		int response = userRepository.updateDriverCar(newCar, username);
		LOGGER.debug("Leave updateDriverCar in the controller");
		if (response == 1)
			return true;
		else
			return false;
	}

	/**
	 * Connects the Web Service with the Repository Delete a user from the database
	 * 
	 * @param user: User
	 * @return Boolean
	 */
	public Boolean deleteUser(User user) {
		LOGGER.debug("Enter deleteUser in the controller");
		int response = userRepository.deleteUser(user);
		LOGGER.debug("Leave deleteUser in the controller");
		if (response == 1)
			return true;
		else
			return false;
	}
	
	/**
	 * Connects the Web Service with the Repository Get the cashiers
	 * 
	 * @return cashiersList: ArrayList<Cashier>
	 */
	@Override
	public ArrayList<Cashier> getAllCashiers() {
		LOGGER.debug("Enter getAllCashiers in the controller");
		ArrayList<Cashier> cashiersList = new ArrayList<Cashier>();
		cashiersList=userRepository.getCashierUsers();
		LOGGER.debug("Leave getAllCashiers in the controller");
		if (cashiersList == null)
			return null;
		else
			return cashiersList;
	}
	
	/**
	 * Connects the Web Service with the Repository Get the admins
	 * 
	 * @return adminsList: ArrayList<Admin>
	 */
	@Override
	public ArrayList<Admin> getAllAdmins() {
		LOGGER.debug("Enter getAllAdmins in the controller");
		ArrayList<Admin> adminsList = new ArrayList<Admin>();
		adminsList=userRepository.getAdminUsers();
		LOGGER.debug("Leave getAllAdmins in the controller");
		if (adminsList == null)
			return null;
		else
			return adminsList;
	}
	
	/**
	 * Connects the Web Service with the Repository Get the drivers
	 * 
	 * @return adminsList: ArrayList<Admin>
	 */
	@Override
	public ArrayList<Driver> getAllDrivers() {
		LOGGER.debug("Enter getAllDrivers in the controller");
		ArrayList<Driver> driverList = new ArrayList<Driver>();
		driverList=userRepository.getDriverUsers();
		LOGGER.debug("Leave getAllDrivers in the controller");
		if (driverList == null)
			return null;
		else
			return driverList;
	}
	
	/**
	 * Connects the Web Service with the Repository Get the drivers
	 * 
	 * @return adminsList: ArrayList<Client>
	 */
	@Override
	public ArrayList<Client> getAllClients() {
		LOGGER.debug("Enter getAllDrivers in the controller");
		ArrayList<Client> clientList = new ArrayList<Client>();
		clientList=userRepository.getClientUsers();
		LOGGER.debug("Leave getAllDrivers in the controller");
		if (clientList == null)
			return null;
		else
			return clientList;
	}



	/**
	 * Connects the Web Service with the Repository Get a user from the database
	 * 
	 * @param username: String
	 * @param password: String
	 * 
	 * @return User
	 */
	public User getLoginUser(String username, String password) {
		LOGGER.debug("Enter getLoginUser in the controller");
		User user = userRepository.getLoginUser(username, password);
		LOGGER.debug("Leave getLoginUser in the controller");
		if (user == null)
			return null;
		else
			return user;
	}

	
	/**
	 * Connects the Web Service with the Repository Get a driver id from the database based on his username
	 * 
	 * @param username: String
	 * 
	 * @return driverID: int
	 */
	public int getDriverIdByUsername(String username) {
		LOGGER.debug("Enter getDriverIdByUsername in the controller");
		int driverID= userRepository.getDriverIdByUsername(username);
		LOGGER.debug("Leave getDriverIdByUsername in the controller");
		return driverID;
	}
	
	/**
	 * Connects the Web Service with the Repository Get the client by given username
	 * 
	 * @param username: String
	 * @return client: Client
	 */
	@Override
	public Client getClientByUsername(String username) {
		LOGGER.debug("Enter getClientByUsername in the controller");
		Client client = new Client();
		client = userRepository.getClientByUsername(username);
		LOGGER.debug("Leave getClientByUsername in the controller");
		if (client == null)
			return null;
		else
			return client;
	}

	/**
	 * Connects the Web Service with the Repository Get the cashier by given username
	 * 
	 * @param username: String
	 * @return cashier: Cashier
	 */
	@Override
	public Cashier getCashierByUsername(String username) {
		LOGGER.debug("Enter getCashierByUsername in the controller");
		Cashier cashier = new Cashier();
		cashier = userRepository.getCashierByUsername(username);
		LOGGER.debug("Leave getCashierByUsername in the controller");
		if (cashier == null)
			return null;
		else
			return cashier;
	}
	
	/**
	 * Connects the Web Service with the Repository Get the admin by given username
	 * 
	 * @param username: String
	 * @return admin: Admin
	 */
	@Override
	public Admin getAdminByUsername(String username) {
		LOGGER.debug("Enter getAdminByUsername in the controller");
		Admin admin = new Admin();
		admin = userRepository.getAdminByUsername(username);
		LOGGER.debug("Leave getAdminByUsername in the controller");
		if (admin == null)
			return null;
		else
			return admin;
	}

}
