package ro.fme.internship.delivery.service.implementation;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Deposit;
import ro.fme.internship.delivery.repository.IDepositRepository;
import ro.fme.internship.delivery.repository.implementation.DepositRepository;
import ro.fme.internship.delivery.service.IDepositService;

public class DepositService implements IDepositService {

	private IDepositRepository depositRepository = new DepositRepository();
	private static final Logger LOGGER = LoggerFactory.getLogger(DepositService.class);

	public DepositService() {
	}

	public DepositService(DepositRepository depositRepository) {
		this.depositRepository = depositRepository;
	}

	/**
	 * Connects the Web Service with the Repository Get the deposit by given id
	 * 
	 * @param id: integer
	 * @return deposit: Deposit
	 */
	@Override
	public Deposit getDeposit(int id) {
		LOGGER.debug("Enter getDeposit in the controller");
		Deposit deposit = depositRepository.getDeposit(id);
		LOGGER.debug("Leave getDeposit in the controller");
		if (deposit == null)
			return null;
		else
			return deposit;

	}

	/**
	 * Connects the Web Service with the Repository Insert a deposit into the
	 * database
	 * 
	 * @param deposit: Deposit
	 * @return Boolean
	 */
	@Override
	public Boolean createDeposit(Deposit deposit) {
		LOGGER.debug("Enter createDeposit in the controller");
		int response = depositRepository.insertDeposit(deposit);
		LOGGER.debug("Leave createDeposit in the controller");
		if (response == 1)
			return true;
		else
			return false;
	}

	/**
	 * Connects the Web Service with the Repository Update the deposit with a given
	 * id
	 * 
	 * @param deposit: Deposit, id: integer
	 * @return Boolean
	 */
	@Override
	public Boolean updateDeposit(Deposit deposit, int id) {
		LOGGER.debug("Enter updateDeposit in the controller");
		int response = depositRepository.updateDeposit(deposit, id);
		LOGGER.debug("Leave updateDeposit in the controller");
		if (response == 1)
			return true;
		else
			return false;
	}

	/**
	 * Get all deposits
	 */
	/**
	 * @return deposits: ArrayList<Deposit>
	 */
	@Override
	public ArrayList<Deposit> getAllDeposits() {
		LOGGER.debug("Enter getAllDeposits in the controller");
		ArrayList<Deposit> deposits = depositRepository.getAllDeposits();
		LOGGER.debug("Leave getAllDeposits in the controller");
		return deposits;
	}
}
