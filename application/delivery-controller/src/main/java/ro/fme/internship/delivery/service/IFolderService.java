package ro.fme.internship.delivery.service;

import java.util.ArrayList;

import ro.fme.internship.delivery.model.Folder;

public interface IFolderService {

	public Boolean createFolder(Folder folder);

	public Folder getFolder(int id);

	public ArrayList<Folder> getFoldersAsTree();
}
