package ro.fme.internship.delivery.service;

import java.util.ArrayList;

import ro.fme.internship.delivery.model.Admin;
import ro.fme.internship.delivery.model.Cashier;
import ro.fme.internship.delivery.model.Client;
import ro.fme.internship.delivery.model.Driver;
import ro.fme.internship.delivery.model.PackageInvoice;
import ro.fme.internship.delivery.model.User;

public interface IUserService {

	public Driver getDriver(int id);

	public Admin getAdmin(int id);

	public Client getClient(int id);

	public Cashier getCashier(int id);

	public Boolean createUser(User user);

	public Boolean updateUser(User user, String username);

	public Boolean updateDriverCar(String newCar, String username);

	public Boolean deleteUser(User user);
	
	public User getLoginUser(String username, String password);

	public ArrayList<Driver> getAllDrivers();

	public ArrayList<Client> getAllClients();

	public ArrayList<Admin> getAllAdmins();

	public ArrayList<Cashier> getAllCashiers();
	
	public int getDriverIdByUsername(String username);

	public Boolean updateUserById(User user, int id);
	
	public Client getClientByUsername(String username);
	
	public Cashier getCashierByUsername(String username);

	public Admin getAdminByUsername(String username);

}
