package ro.fme.internship.delivery.service;

import ro.fme.internship.delivery.model.Folder;

public interface IRelationFolderInFolderService {

	public Boolean createRelationFolderInFolderRepository(Folder sourceFolder, Folder targetFolder);
}
