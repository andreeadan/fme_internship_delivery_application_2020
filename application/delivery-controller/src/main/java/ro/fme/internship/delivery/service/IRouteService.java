package ro.fme.internship.delivery.service;

import java.util.ArrayList;

import ro.fme.internship.delivery.model.Route;

public interface IRouteService {

	Route getRouteById(int id);

	ArrayList<Route> getAllRoutes();
	
	ArrayList<Route> getRouteByDriverId(int idDriver);
	
	public boolean createRoute(String expCounty,String destCounty,int deriverID);
}
