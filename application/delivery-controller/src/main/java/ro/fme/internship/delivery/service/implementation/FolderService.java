package ro.fme.internship.delivery.service.implementation;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Folder;
import ro.fme.internship.delivery.repository.IFolderRepository;
import ro.fme.internship.delivery.repository.implementation.FolderRepository;
import ro.fme.internship.delivery.service.IFolderService;

public class FolderService implements IFolderService {

	private IFolderRepository folderRepository = new FolderRepository();
	private static final Logger LOGGER = LoggerFactory.getLogger(FolderService.class);

	public FolderService() {
	}

	public FolderService(IFolderRepository folderRepository) {
		this.folderRepository = folderRepository;
	}

	/**
	 * Connects the Web Service with the Repository Insert a folder into the
	 * database
	 * 
	 * @param folder: Folder
	 * @return Boolean
	 */
	@Override
	public Boolean createFolder(Folder folder) {
		LOGGER.debug("Enter createFolder in the controller");
		int response = folderRepository.insertFolder(folder);
		LOGGER.debug("Leave createFolder in the controller");
		if (response == 1)
			return true;
		else
			return false;
	}

	/**
	 * Connects the Web Service with the Repository Get the folder by given id
	 * 
	 * @param id: integer
	 * @return folder: Folder
	 */
	@Override
	public Folder getFolder(int id) {
		LOGGER.debug("Return getFolder in the controller");
		return folderRepository.getFolderWithId(id);
	}

	@Override
	/**
	 * get all folders and their children
	 * 
	 * @return list of folders as tree
	 */
	public ArrayList<Folder> getFoldersAsTree() {
		LOGGER.debug("Enter getFoldersAsTree in controller");
		LOGGER.debug("get all folders that represents the year...");
		ArrayList<Folder> yearFolders = folderRepository.getAllYearFolders();

		for (Folder yearFolder : yearFolders) {
			LOGGER.debug("get every folder of an year that represents the date");
			ArrayList<Folder> dateFolders = folderRepository.getAllChildren(yearFolder.getFolder_id());
			yearFolder.setSubFolders(dateFolders);
			for (Folder dateFolder : dateFolders) {
				LOGGER.debug("get every folder of a date that represents the client...");
				ArrayList<Folder> clientFolders = folderRepository.getAllChildren(dateFolder.getFolder_id());
				dateFolder.setSubFolders(clientFolders);
			}
		}
		LOGGER.debug("Return getFoldersAsTree in the controller");
		return yearFolders;
	}

}
