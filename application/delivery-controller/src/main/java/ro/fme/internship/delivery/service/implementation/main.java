package ro.fme.internship.delivery.service.implementation;


import java.sql.Date;

import ro.fme.internship.delivery.model.PackageInvoice;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Date d1 = new Date(2020, 8, 15);
		Date d2 = new Date(2020, 8, 9);
		Date d3 = new Date(2020, 10, 10);
		
		PackageInvoice pack = new PackageInvoice(20, 67, 68, "description2", 100, d1, "delivered", 100, 1, d2, d3, "books", "Cluj-Napoca", 81);
		pack.setInvoice_id(13);
		PackageInvoiceService p = new PackageInvoiceService();
		p.generatePDF(pack);
	}

}
