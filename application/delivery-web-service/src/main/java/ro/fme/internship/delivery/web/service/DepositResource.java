package ro.fme.internship.delivery.web.service;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Deposit;
import ro.fme.internship.delivery.service.IDepositService;
import ro.fme.internship.delivery.service.implementation.DepositService;

@Path("deposits") // .../delivery-web-service/webapi/deposits
public class DepositResource {

	private IDepositService depositService = new DepositService();
	private static final Logger LOGGER = LoggerFactory.getLogger(DepositResource.class);

	/**
	 * GET for testing purposes
	 * 
	 * @return Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllDeposits() {
		LOGGER.debug("Enter getAllDeposits in web-service");
		ArrayList<Deposit> deposits = depositService.getAllDeposits();
		if(deposits==null) {
			LOGGER.debug("no deposits found...");
			return Response.status(Status.NOT_FOUND).build();
		}
		LOGGER.debug("leave getAllDeposits in web-service");
		return Response.ok().entity(deposits).build();
	}

	/**
	 * GET a deposit by the given id
	 * 
	 * @param id: String
	 * @return Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{depositId}") // .../delivery-web-service/webapi/deposits/{depositId}
	public Response getDeposit(@PathParam("depositId") String id) {
		LOGGER.debug("Enter getDeposit in web-service");
		int depositId = 0;
		try {
			LOGGER.debug("Getting the integer value...");
			depositId = Integer.parseInt(id);
		} catch (Exception e) {
			LOGGER.error("Error in DepositResource, getDeposit ", e);
			return Response.status(Status.NOT_FOUND).build();
		}

		if (depositId < 0) {
			LOGGER.error("Error in DepositResource, invalid deposit id");
			return Response.status(Status.BAD_REQUEST).build();
		}

		Deposit deposit = depositService.getDeposit(depositId);
		if (deposit == null) {
			LOGGER.error("Error in DepositResource, no deposit found");
			return Response.status(Status.NOT_FOUND).build();
		}
		LOGGER.debug("Leave getDeposit in web-service");
		return Response.ok().entity(deposit).build();
	}
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("add")
	public Response createDeposit(Deposit depositSent) {
		LOGGER.debug("Enter createDeposit in web-service");
		Deposit deposit = new Deposit();
		deposit.setCounty(depositSent.getCounty());
		deposit.setAddress(depositSent.getAddress());
		deposit.setStatus("active");
		boolean added = depositService.createDeposit(deposit);
		if (!added) {
			LOGGER.error("Error in DepositResource, createDeposit ,deposit was not created ");
			return Response.status(Status.NOT_FOUND).build();
		}
		LOGGER.debug("Leave createDeposit in web-service");
		return Response.ok().entity(deposit).build();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("update")
	public Response updateDeposit(Deposit depositSent) {
		LOGGER.debug("Enter updateDeposit in web-service");
		
		boolean added = depositService.updateDeposit(depositSent,depositSent.getDepositID());
		if (!added) {
			LOGGER.error("Error in DepositResource, updateDeposit ,deposit was not updated ");
			return Response.status(Status.NOT_FOUND).build();
		}
		LOGGER.debug("Leave updateDeposit in web-service");
		return Response.ok().entity(depositSent).build();
	}
	

}
