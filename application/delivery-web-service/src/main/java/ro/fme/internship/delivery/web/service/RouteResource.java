package ro.fme.internship.delivery.web.service;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Deposit;
import ro.fme.internship.delivery.model.PackageInvoice;
import ro.fme.internship.delivery.model.Route;
import ro.fme.internship.delivery.service.IRouteService;
import ro.fme.internship.delivery.service.IUserService;
import ro.fme.internship.delivery.service.implementation.RouteService;
import ro.fme.internship.delivery.service.implementation.UserService;

@Path("routes") // .../delivery-web-service/webapi/routes
public class RouteResource {

	private IRouteService routeService = new RouteService();
	private IUserService userService = new UserService();
	private static final Logger LOGGER = LoggerFactory.getLogger(RouteResource.class);

	/**
	 * GET a route by the given id
	 * 
	 * @param id: String
	 * @return Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{routeId}") // .../delivery-web-service/webapi/routes/{routeId}
	public Response getRoute(@PathParam("routeId") String id) {
		LOGGER.debug("Enter getRoute in web-service");
		LOGGER.debug("Getting the integer value...");
		Route route = routeService.getRouteById(Integer.parseInt(id));

		if (route == null) {
			LOGGER.error("Error in RouteService, no route found");
			return Response.status(Status.NOT_FOUND).build();
		}
		LOGGER.debug("Leave getRoute in web-service");
		return Response.ok().entity(route).build();
	}

	/**
	 * GET all routes from the database
	 * 
	 * @return Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRoutes() {
		LOGGER.debug("Enter getRoutes in web-service");
		ArrayList<Route> routes = routeService.getAllRoutes();
		GenericEntity<ArrayList<Route>> entity = new GenericEntity<ArrayList<Route>>(routes) {
		};
		LOGGER.debug("Leave getRoutes in web-service");
		return Response.ok(entity).build();
	}

	/**
	 * 
	 * @param name: String
	 * @return Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("driver/{driverName}")
	public Response getRouteByDriverID(@PathParam("driverName") String name) {
		LOGGER.debug("Getting driver id by driver name");
		int idDriver = userService.getDriverIdByUsername(name);
		LOGGER.info("Getting all the routes with a specific driver id");
		ArrayList<Route> routes = routeService.getRouteByDriverId(idDriver);
		GenericEntity<ArrayList<Route>> entity = new GenericEntity<ArrayList<Route>>(routes) {
		};

		return Response.ok(entity).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("add")
	public Response createRoute(Route route) {
		LOGGER.debug("Enter createRoute in web-service");
		boolean added = routeService.createRoute(route.getExpCounty(), route.getDestCounty(), route.getIdDriver());
		if (!added) {
			LOGGER.error("Error in RouteResource, createRoute ,route was not created ");
			return Response.status(Status.NOT_FOUND).build();
		}
		LOGGER.debug("Leave createRoute in web-service");
		return Response.ok().build();
	}
}
