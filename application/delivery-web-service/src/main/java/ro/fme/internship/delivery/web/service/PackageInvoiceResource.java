package ro.fme.internship.delivery.web.service;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Cashier;
import ro.fme.internship.delivery.model.Client;
import ro.fme.internship.delivery.model.Deposit;
import ro.fme.internship.delivery.model.PackageInvoice;
import ro.fme.internship.delivery.repository.IDepositRepository;
import ro.fme.internship.delivery.repository.IPackageInvoiceRepository;
import ro.fme.internship.delivery.repository.IUserRepository;
import ro.fme.internship.delivery.repository.implementation.DepositRepository;
import ro.fme.internship.delivery.repository.implementation.PackageInvoiceRepository;
import ro.fme.internship.delivery.repository.implementation.UserRepository;
import ro.fme.internship.delivery.service.IPackageInvoiceService;
import ro.fme.internship.delivery.service.implementation.PackageInvoiceService;
import ro.fme.internship.delivery.service.plugin.GeneratePackageInvoicePDF;

@Path("packageInvoices")
public class PackageInvoiceResource {

	private IPackageInvoiceRepository packageInvoiceRepository = new PackageInvoiceRepository();
	private IPackageInvoiceService packageInvoiceService = new PackageInvoiceService(packageInvoiceRepository);
	private static final Logger LOGGER = LoggerFactory.getLogger(PackageInvoiceResource.class);
	private GeneratePackageInvoicePDF generatePackageInvoicePDF = new GeneratePackageInvoicePDF();
	private IUserRepository userRepository = new UserRepository();
	private IDepositRepository depositRepository = new DepositRepository();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPackageInvoices() {
		LOGGER.debug("Enter getPackageInvoices in web-service");
		ArrayList<PackageInvoice> packInvoices = packageInvoiceService.getPackageInvoices();
		GenericEntity<ArrayList<PackageInvoice>> entity = new GenericEntity<ArrayList<PackageInvoice>>(packInvoices) {
		};
		LOGGER.debug("Leave getPackageInvoices in web-service");
		return Response.ok(entity).build();
	}

	/**
	 * get the response from quering the db:if ok=>pagkage invoice from db else 404
	 * 
	 * @param packageInvoiceId
	 * @return packageInvoice as json
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{packageInvoiceId}")
	public Response getPackageInvoiceById(@PathParam("packageInvoiceId") String packageInvId) {
		LOGGER.debug("Enter getPackageInvoiceById in web-service");
		int packageInvoiceId = 0;
		try {
			packageInvoiceId = Integer.parseInt(packageInvId);
		} catch (Exception e) {
			LOGGER.error("The id is not a valid value!");
			return Response.status(Status.BAD_REQUEST).build();
		}

		PackageInvoice packageInvoice = packageInvoiceService.getPackageInvoiceById(packageInvoiceId);

		// not found but valid id => 404
		if (packageInvoice == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		// Everything is OK
		LOGGER.debug("Leave getPackageInvoiceById in web-service");
		return Response.status(Status.OK).entity(packageInvoice).build();
	}

	/**
	 * get the response from quering the db:if ok=>pagkage invoice from db else 404
	 * 
	 * @param recorddate
	 * @param clientId
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{date}/{clientId}")
	public Response getPackageInvoicesByRecord(@PathParam("date") String recorddate,
			@PathParam("clientId") String clientId) {

		LOGGER.debug("Enter getPackageInvoicesByRecord in web-service");
		LocalDate localDate;

		try {

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-d");
			localDate = LocalDate.parse(recorddate, formatter);
		} catch (Exception e) {
			LOGGER.error("The dateis  not a valid format!");
			return Response.status(Status.BAD_REQUEST).build();
		}
		int clientFolderId = 0;

		try {

			clientFolderId = Integer.parseInt(clientId);
		} catch (Exception e) {
			LOGGER.error("The clientFolderId is not a valid value!");
			return Response.status(Status.BAD_REQUEST).build();
		}

		ArrayList<PackageInvoice> packInvoices = packageInvoiceService.getInvoicesByRecord(localDate, clientFolderId);

		GenericEntity<ArrayList<PackageInvoice>> entity = new GenericEntity<ArrayList<PackageInvoice>>(packInvoices) {
		};

		// not found but valid id => 404
		if (entity == null) {
			LOGGER.debug("Not found! Leave getPackageInvoicesByRecord in web-service");
			return Response.status(Status.NOT_FOUND).build();
		}

		LOGGER.debug("Leave getPackageInvoicesByRecord in web-service");

		// Everything is OK
		return Response.status(Status.OK).entity(entity).build();
	}
	
	/**
	 * get all received invoices of a client 
	 * @param username : String
	 * @return Response
	 */
	@GET
	@Path("receivedInvoices/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReceived(@PathParam("username") String username) {
		LOGGER.debug("Enter getPackageInvoices in web-service");
		ArrayList<PackageInvoice> packInvoices = packageInvoiceService.getReceivedInvoices(username);
		GenericEntity<ArrayList<PackageInvoice>> entity = new GenericEntity<ArrayList<PackageInvoice>>(packInvoices) {
		};
		LOGGER.debug("Leave getPackageInvoices in web-service");
		return Response.ok(entity).build();
	}
	/**
	 * get all sent invoices of a client 
	 * @param username : String
	 * @return Response
	 */
	@GET
	@Path("sentInvoices/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSent(@PathParam("username") String username) {
		LOGGER.debug("Enter getPackageInvoices in web-service");
		ArrayList<PackageInvoice> packInvoices = packageInvoiceService.getSentInvoices(username);
		GenericEntity<ArrayList<PackageInvoice>> entity = new GenericEntity<ArrayList<PackageInvoice>>(packInvoices) {
		};
		LOGGER.debug("Leave getPackageInvoices in web-service");
		return Response.ok(entity).build();
	}

	/**
	 * get all invoices of a client 
	 * @param username : String
	 * @return Response
	 */
	@GET
	@Path("client/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getClientsInvoices(@PathParam("username") String username) {
		LOGGER.debug("Enter getClientsInvoices in web-service");
		ArrayList<PackageInvoice> packInvoices = packageInvoiceService.getClientsInvoices(username);
		GenericEntity<ArrayList<PackageInvoice>> entity = new GenericEntity<ArrayList<PackageInvoice>>(packInvoices) {
		};
		LOGGER.debug("Leave getClientsInvoices in web-service");
		return Response.ok(entity).build();
	}

	/**
	 * Create a new invoice in the database
	 * 
	 * @param invoiceSent: PackageInvoice
	 * @return Response
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("add") // .../delivery-web-service/webapi/packageInvoices/add
	public Response createPackageInvoice(PackageInvoice invoiceSent) {
		LOGGER.debug("Enter createPackageInvoice in web-service");
		Date creationDate = Date.valueOf(java.time.LocalDate.now());
		Date deliveryDate = invoiceSent.getDelivery_date();
		Date modifyDate = creationDate;

		PackageInvoice invoice = new PackageInvoice(invoiceSent.getExp_client_id(), invoiceSent.getDest_client_id(),
				invoiceSent.getOrder_description(), invoiceSent.getOrder_cost(), creationDate, "Processing",
				invoiceSent.getOrder_weight(), invoiceSent.getDeposit_id(), deliveryDate, modifyDate,
				invoiceSent.getContent_type(), invoiceSent.getContent_location(), invoiceSent.getCashier_id());

		boolean wasAdded = packageInvoiceService.createPackageInvoice(invoice);
		if (!wasAdded) {
			LOGGER.error("Error in PackageInvoiceResource, createPackageInvoice, invoice was not created ");
			return Response.status(Status.NOT_FOUND).build();
		}

		LOGGER.debug("Leave createPackageInvoice in web-service");
		return Response.ok().entity(invoice).build();
	}

	/**
	 * Update a given invoice in the database
	 * 
	 * @param invoiceSent: PackageInvoice
	 * @return Response
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("update") // .../delivery-web-service/webapi/packageInvoices/update
	public Response updatePackageInvoice(PackageInvoice invoiceSent) {
		LOGGER.debug("Enter updatePackageInvoice in web-service");

		invoiceSent.setModify_date(Date.valueOf(java.time.LocalDate.now()));
		boolean wasAdded = packageInvoiceService.updatePackageInvoice(invoiceSent, invoiceSent.getInvoice_id());
		if (!wasAdded) {
			LOGGER.error("Error in PackageInvoiceResource, updatePackageInvoice, invoice was not updated ");
			return Response.status(Status.NOT_FOUND).build();
		}
		LOGGER.debug("Leave updatePackageInvoice in web-service");
		return Response.ok().entity(invoiceSent).build();
	}

	/**
	 * Export PDF
	 * 
	 * @param packageInvoice: PackageInvoice
	 * @return Response
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("export") // .../delivery-web-service/webapi/packageInvoices/export
	public Response export(PackageInvoice packageInvoice) {
		LOGGER.debug("Enter export in web-service");

		try {
			Client sender = userRepository.getClient(packageInvoice.getExp_client_id());
			Client reciever = userRepository.getClient(packageInvoice.getDest_client_id());
			Cashier cashier = userRepository.getCashier(packageInvoice.getCashier_id());
			Deposit deposit = depositRepository.getDeposit(packageInvoice.getDeposit_id());

			generatePackageInvoicePDF.generatePDF(packageInvoice, sender, reciever, cashier, deposit);
		} catch (Exception e) {
			LOGGER.error("Error in PackageInvoiceResource, export, invoice was not exported ");
			return Response.status(Status.NOT_FOUND).build();
		}
		LOGGER.debug("Leave export in web-service");
		return Response.ok().build();

	}
}
