package ro.fme.internship.delivery.web.service;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ro.fme.internship.delivery.model.Folder;
import ro.fme.internship.delivery.service.IFolderService;
import ro.fme.internship.delivery.service.implementation.FolderService;

@Path("folders") // .../delivery-web-service/webapi/folders
public class FolderResource {

	private IFolderService folderService = new FolderService();
	private static final Logger LOGGER = LoggerFactory.getLogger(FolderResource.class);

	/**
	 * GET for testing purposes
	 * 
	 * @return String
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getIt() {
		LOGGER.debug("Return getIt in web-service");
		return "Got it!";
	}

	/**
	 * GET a folder by the given id
	 * 
	 * @param id: String
	 * @return Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{folderId}") // .../delivery-web-service/webapi/folders/{folderId}
	public Response getFolder(@PathParam("folderId") String id) {
		LOGGER.debug("Enter getFolder in web-service");
		int folderId = 0;
		try {
			LOGGER.debug("Getting the integer value...");
			folderId = Integer.parseInt(id);
		} catch (Exception e) {
			LOGGER.error("Error in FolderResource, getFolder ", e);
			return Response.status(Status.NOT_FOUND).build();
		}

		if (folderId < 0) {
			LOGGER.error("Error in FolderResource, invalid folder id");
			return Response.status(Status.BAD_REQUEST).build();
		}

		Folder folder = folderService.getFolder(folderId);
		if (folder == null) {
			LOGGER.error("Error in FolderResource, no folder found");
			return Response.status(Status.NOT_FOUND).build();
		}
		LOGGER.debug("Leave getFolder in web-service");
		return Response.ok().entity(folder).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("children")
	public Response getAllFoldersAsTree() {
		LOGGER.debug("Enter getFoldersAsTree in web-service");
		ArrayList<Folder> folders = folderService.getFoldersAsTree();

		if (folders == null) {
			LOGGER.debug("no folders found...");
			return Response.status(Status.NOT_FOUND).build();
		}
		LOGGER.debug("leave getAllFoldersAsTree in web-service");
		return Response.ok().entity(folders).build();

	}

}
