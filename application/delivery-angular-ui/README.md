# DeliveryAngularUi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.1.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## First steps

Run `npm install` after opening the project in Visual Studio Code

If the Angular Material is not installed, run `ng add @angular/material`
    > 'Indigo/Pink' theme
    > 'Yes' global typography styles
    > 'Yes' browser animations

## Proxy configuration

In the main project file (outside of `src`) open the `proxy.conf.json` file and change the port to your own
