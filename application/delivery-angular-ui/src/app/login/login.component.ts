import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { User } from '../models/User';
import { AuthenticationService } from '../services/authentication/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  hide: boolean = true;
  loggedUser: User;

  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.loggedUser = new User();
  }

  login(event: any): void {
    event.preventDefault();
    this.authenticationService.login(this.username, this.password)
      .pipe(first())
      .subscribe({
        next: () => {
          let role = this.authenticationService.getRole();
          console.log(localStorage.getItem('currentUser'));
          switch (role) {
            case ("admin").valueOf():
              this.router.navigate(['/admin-profile']);
              break;
            case ("client").valueOf():
              this.router.navigate(['/client-profile']);
              break;
            case ("driver").valueOf():
              this.router.navigate(['/driver-profile']); // route to driver view
              break;
            case ("cashier").valueOf():
              this.router.navigate(['/cashier-profile']); // route to cashier view
              break;
            default:
              this.router.navigate(['/login']);
          }
        },
        error: error => {
          console.log("Error in Login Component, login");
        }
      });

  }

  register(event: any): void {
    event.preventDefault();
    this.router.navigate(['/register'])
  }
}
