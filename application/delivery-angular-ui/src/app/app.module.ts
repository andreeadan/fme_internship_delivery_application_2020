import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { PackageInvoiceComponent } from './package-invoice/package-invoice.component';
import { NewInvoiceDialogComponent } from './package-invoice/new-invoice-dialog/new-invoice-dialog.component';
import { DepositViewComponent } from './deposit-view/deposit-view.component';
import { EditInvoiceDialogComponent } from './package-invoice/edit-invoice-dialog/edit-invoice-dialog.component';
import { NewDepositDialogComponent } from './deposit-view/new-deposit-dialog/new-deposit-dialog.component';
import { EditDepositDialogComponent } from './deposit-view/edit-deposit-dialog/edit-deposit-dialog.component';
import { VerifyDepositCreationComponent } from './deposit-view/verify-deposit-creation/verify-deposit-creation.component';
import { ViewInvoiceDialogComponent } from './package-invoice/view-invoice-dialog/view-invoice-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { AdminsComponent } from './users-groups/admins/admins.component';
import { ClientsComponent } from './users-groups/clients/clients.component';
import { DriversComponent } from './users-groups/drivers/drivers.component';
import { CashiersComponent } from './users-groups/cashiers/cashiers.component';
import { GroupsRootComponent } from './users-groups/groups-root/groups-root.component';
import { NewAdminDialogComponent } from './users-groups/new-admin-dialog/new-admin-dialog.component';
import { NewClientDialogComponent } from './users-groups/new-client-dialog/new-client-dialog.component';
import { NewDriverDialogComponent } from './users-groups/new-driver-dialog/new-driver-dialog.component';
import { NewCashierDialogComponent } from './users-groups/new-cashier-dialog/new-cashier-dialog.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NavtreeComponent } from './navtree/navtree.component';
import { MatTableModule } from '@angular/material/table';
import { ExportInvoiceDialogComponent } from './package-invoice/export-invoice-dialog/export-invoice-dialog.component';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { MatListModule } from '@angular/material/list';
import { RoutesViewComponent } from './routes-view/routes-view.component';
import { NewRouteDialogComponent } from './routes-view/new-route-dialog/new-route-dialog.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { AdminProfileComponent } from './profile/admin-profile/admin-profile.component';
import { CashierProfileComponent } from './profile/cashier-profile/cashier-profile.component';
import { ClientViewComponent } from './profile/client-view/client-view.component';
import { EditAdminDialogComponent } from './users-groups/edit-admin-dialog/edit-admin-dialog.component';
import { EditCashierDialogComponent } from './users-groups/edit-cashier-dialog/edit-cashier-dialog.component';
import { EditDriverDialogComponent } from './users-groups/edit-driver-dialog/edit-driver-dialog.component';
import { EditClientDialogComponent } from './users-groups/edit-client-dialog/edit-client-dialog.component';
import { DriverComponent } from './profile/driver-profile/driver.component';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTabsModule } from '@angular/material/tabs';



@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    LoginComponent,
    RegisterComponent,
    PackageInvoiceComponent,
    AdminsComponent,
    ClientsComponent,
    DriversComponent,
    CashiersComponent,
    GroupsRootComponent,
    NewAdminDialogComponent,
    NewClientDialogComponent,
    NewDriverDialogComponent,
    NewCashierDialogComponent,
    NewInvoiceDialogComponent,
    EditInvoiceDialogComponent,
    DepositViewComponent,
    NewDepositDialogComponent,
    PackageInvoiceComponent,
    EditDepositDialogComponent,
    VerifyDepositCreationComponent,
    DriverComponent,
    NavbarComponent,
    NavtreeComponent,
    EditAdminDialogComponent,
    ViewInvoiceDialogComponent,
    ClientViewComponent,
    ViewInvoiceDialogComponent,
    ExportInvoiceDialogComponent,
    RoutesViewComponent,
    NewRouteDialogComponent,
    EditCashierDialogComponent,
    EditDriverDialogComponent,
    EditClientDialogComponent,
    RoutesViewComponent,
    AdminProfileComponent,
    CashierProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatCardModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    FormsModule,
    HttpClientModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    HttpClientModule,
    MatSortModule,
    MatDialogModule,
    MatSidenavModule,
    MatMenuModule,
    MatListModule,
    MatTabsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
