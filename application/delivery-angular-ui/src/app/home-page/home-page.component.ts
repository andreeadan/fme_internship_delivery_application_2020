import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { User } from '../models/User';
import { AuthenticationService } from '../services/authentication/authentication.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  user: User;

  constructor(private router: Router, private authenticationService: AuthenticationService) { 
    this.user = this.authenticationService.currentUserValue;
  }

  ngOnInit(): void {
  }

}
