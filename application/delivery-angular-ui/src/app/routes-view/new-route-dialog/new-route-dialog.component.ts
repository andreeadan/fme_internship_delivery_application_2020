import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Route } from 'src/app/models/Route';
import { DriverService } from 'src/app/services/driver/driver.service';
import { RouteService } from 'src/app/services/route/route.service';

@Component({
  selector: 'app-new-route-dialog',
  templateUrl: './new-route-dialog.component.html',
  styleUrls: ['./new-route-dialog.component.css']
})
export class NewRouteDialogComponent implements OnInit {

  driver: string;
  expedition: string;
  destination: string;

  constructor(public dialogRef: MatDialogRef<NewRouteDialogComponent>, private routeService: RouteService, private driverService: DriverService) { }

  ngOnInit(): void {

  }


  closeDialog() {
    this.dialogRef.close();
  }

  createRoute() {
    let route = new Route();
    route.expCounty = this.expedition;
    route.destCounty = this.destination;
    route.idDeposit = "";
    route.idRoute = "";

    this.driverService.getDriverIdByUsername(this.driver).subscribe((data: string) => {
      route.idDriver = data;
      this.routeService.createRoute(route).subscribe(); console.log(data)
    });

    this.closeDialog();
  }

}
