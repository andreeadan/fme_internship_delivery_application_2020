import { Component, OnInit, ViewChild } from '@angular/core';
import { Driver } from '../models/Driver';
import { DriverService } from '../services/driver/driver.service';
import { Route } from '../models/Route';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AuthenticationService } from '../services/authentication/authentication.service';
import { RouteService } from '../services/route/route.service';
import { NewRouteDialogComponent } from './new-route-dialog/new-route-dialog.component';
@Component({
  selector: 'app-routs-view',
  templateUrl: './routes-view.component.html',
  styleUrls: ['./routes-view.component.css']
})
export class RoutesViewComponent implements OnInit {
  pageTitle: string;
  errorMessage: string;
  userRole: string;
  routes: Route[] = [];
  displayedColumns: string[] = ['idRoute', 'idDeposit', 'idDriver', 'expCounty', 'destCounty'];
  dataSource = new MatTableDataSource<Route>();

  //paginator
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private routeService: RouteService, private authenticationService: AuthenticationService, private driverService:DriverService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.userRole = String(this.authenticationService.getRole());
    // this.pageTitle="Welcome back, "+this.oneDriver.driver_id;

    if (this.userRole == 'driver') {
      this.populateData();
    }
    else if (this.userRole == 'admin') {
      this.populateAllRoutes();
    }
    // this.populateData();
    setTimeout(() => this.dataSource.sort = this.sort);
    setTimeout(() => this.dataSource.paginator = this.paginator);
  }
  populateAllRoutes() {
    this.routeService.getAllRoutes().subscribe(
      (routes: Route[]) => { this.setRoutesAppearance(routes); },
      (err: any) => console.log(err)
    );
  }

  populateData() {
    this.routeService.getRoutesForDriver().subscribe(
      (routes: Route[]) => { this.setRoutesAppearance(routes); },
      (err: any) => console.log(err)
    );
  }

  setRoutesAppearance(routes:Route[]){
    for (let route of routes) {
      this.driverService.getDriverById(route.idDriver).subscribe(
        (driver: Driver) => {
          route.idDriver = driver.username; }
      );
      this.dataSource = new MatTableDataSource<Route>(routes);
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  createRoutes() {
    let dialogCreate = this.dialog.open(NewRouteDialogComponent, {
      width: '300px',
      height: '250px',
      data: {},
    });
    dialogCreate.afterClosed().subscribe(result => { this.ngOnInit() });
  }
}
