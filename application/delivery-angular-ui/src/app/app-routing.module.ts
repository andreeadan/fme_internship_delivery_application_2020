import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminGuard } from './guards/admin/admin.guard';
import { AuthenticationGuard } from './guards/authentication.guard';
import { CashierGuard } from './guards/cashier/cashier.guard';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginComponent } from './login/login.component';
import { PackageInvoiceComponent } from './package-invoice/package-invoice.component';
import { RegisterComponent } from './register/register.component';
import { RoutesViewComponent } from './routes-view/routes-view.component';
import { AdminsComponent } from './users-groups/admins/admins.component';
import { CashiersComponent } from './users-groups/cashiers/cashiers.component';
import { ClientsComponent } from './users-groups/clients/clients.component';
import { DriversComponent } from './users-groups/drivers/drivers.component';
import { GroupsRootComponent } from './users-groups/groups-root/groups-root.component';

import { AdminProfileComponent } from './profile/admin-profile/admin-profile.component';
import { CashierProfileComponent } from './profile/cashier-profile/cashier-profile.component';
import { DepositViewComponent } from './deposit-view/deposit-view.component';
import { DriverGuard } from './guards/driver/driver.guard';
import { DriverComponent } from './profile/driver-profile/driver.component';
import { ClientGuard } from './guards/client/client.guard';
import { ClientViewComponent } from './profile/client-view/client-view.component';


const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'home',
    component: HomePageComponent,
    canActivate: [AuthenticationGuard],
    children: [
      { path: 'driver', component: DriverComponent }
    ]
  },
  {
    path: 'admin-profile',
    component: AdminProfileComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'cashier-profile',
    component: CashierProfileComponent,
    canActivate: [CashierGuard]
  },
  {
    path: 'deposits',
    component: DepositViewComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'invoices',
    component: PackageInvoiceComponent,
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'groups/admins',
    component: AdminsComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'groups/clients',
    component: ClientsComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'groups/drivers',
    component: DriversComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'groups/cashiers',
    component: CashiersComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'groups',
    component: GroupsRootComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'routes',
    component: RoutesViewComponent,
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'driver-profile',
    component: DriverComponent,
    canActivate: [DriverGuard]
  },
  {
    path: 'client-profile',
    component: ClientViewComponent,
    canActivate: [ClientGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
