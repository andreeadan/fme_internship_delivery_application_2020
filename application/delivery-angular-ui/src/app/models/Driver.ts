import { User } from './User';

export class Driver extends User{
    driverID: string;
    car: string;
}