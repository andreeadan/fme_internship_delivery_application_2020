export class PackageInvoice {
    invoice_id: string;
    return_id: string;
    exp_client_id: string;
    dest_client_id: string;
    order_description: string;
    order_cost: string;
    creation_date: string;
    order_status: string;
    order_weight: string;
    deposit_id: string;
    delivery_date: string;
    modify_date: string;
    content_type: string;
    content_location: string;
    cashier_id: string;
}