import { User } from './User';

export class Client extends User{
    clientID: string;
    zipCode: string;
    address:string;
    city:string;
    phoneNumber:string;
    county:string;
}