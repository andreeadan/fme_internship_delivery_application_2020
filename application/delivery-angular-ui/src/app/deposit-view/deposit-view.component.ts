import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { IDeposit } from '../models/Deposit';
import { DepositViewService } from '../services/depositView/deposit-view.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { NewDepositDialogComponent } from './new-deposit-dialog/new-deposit-dialog.component';
import { MatMenuTrigger } from '@angular/material/menu';
import { EditDepositDialogComponent } from './edit-deposit-dialog/edit-deposit-dialog.component';
@Component({
  selector: 'app-deposit-view',
  templateUrl: './deposit-view.component.html',
  styleUrls: ['./deposit-view.component.css']
})
export class DepositViewComponent implements OnInit {

  displayedColumns: string[] = ['depositID', 'county', 'address', 'status'];
  dataSource = new MatTableDataSource<IDeposit>();
  dataEntry: IDeposit;
  statusDeposit: string;
  menuTopLeftPosition = { x: '0', y: '0' }

  constructor(private depositService: DepositViewService, private dialog: MatDialog) { }

  //paginator
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatMenuTrigger, { static: true }) matMenuTrigger: MatMenuTrigger;

  ngOnInit(): void {
    this.populateData();
    setTimeout(() => this.dataSource.sort = this.sort);
    setTimeout(() => this.dataSource.paginator = this.paginator);
  }

  populateData() {
    this.depositService.getDeposits().subscribe(
      (data: IDeposit[]) => this.dataSource = new MatTableDataSource<IDeposit>(data),
      (err: any) => console.log(err)
    );
  }

  onRightClick(event: MouseEvent, item) {
    // preventDefault avoids to show the visualization of the right-click menu of the browser
    event.preventDefault();

    // we record the mouse position in our object
    this.menuTopLeftPosition.x = event.clientX + 'px';
    this.menuTopLeftPosition.y = event.clientY + 'px';

    // we open the menu
    this.matMenuTrigger.openMenu();
    this.dataEntry = item.content;
    this.statusDeposit = this.dataEntry.status.toString().trim();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  createDeposit() {
    let dialogCreate = this.dialog.open(NewDepositDialogComponent, {
      width: '300px',
      height: '250px',
      data: {},
    });
    dialogCreate.afterClosed().subscribe(result => { this.ngOnInit() });
  }

  getDepostitFromTable(row: IDeposit) {
    this.dataEntry = row;
  }

  editDeposit() {
    let dialogEdit = this.dialog.open(EditDepositDialogComponent, {
      width: '250px',
      height: '300px',
      data: {
        depositID: this.dataEntry.depositID,
        county: this.dataEntry.county,
        address: this.dataEntry.address,
        status: this.dataEntry.status
      },
    });
    dialogEdit.afterClosed().subscribe(result => { this.ngOnInit() });
  }

  setDepositStatus() {
    if (this.statusDeposit === "active") {
      this.dataEntry.status = "inactive"
    } else {
      this.dataEntry.status = "active"
    }
    this.depositService.updateDeposit(this.dataEntry).subscribe();
  }
}
