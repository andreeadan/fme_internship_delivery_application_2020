import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { IDeposit } from 'src/app/models/Deposit';
import { DepositViewService } from 'src/app/services/depositView/deposit-view.service';
import { VerifyDepositCreationComponent } from '../verify-deposit-creation/verify-deposit-creation.component';

@Component({
  selector: 'app-new-deposit-dialog',
  templateUrl: './new-deposit-dialog.component.html',
  styleUrls: ['./new-deposit-dialog.component.css']
})
export class NewDepositDialogComponent implements OnInit {

  county: string = ""
  address: string = ""

  constructor(public dialogRef: MatDialogRef<NewDepositDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private depositService: DepositViewService, private dialog: MatDialog) { }

  ngOnInit(): void {
  }

  closeDialog() {
    this.dialogRef.close();
  }

  openVerificationDialog() {
    let dialogRefVerify = this.dialog.open(VerifyDepositCreationComponent, {
      width: '300px',
      height: '150px',
      data: {},
    });

    dialogRefVerify.afterClosed().subscribe(result => {
      result ?
        this.createDeposit() : this.closeDialog()
    });
  }

  createDeposit() {
    var newDeposit = new IDeposit();

    newDeposit.county = this.county;
    newDeposit.address = this.address;
    newDeposit.status = '';
    if ((this.county != "") && (this.address != "")) {
      this.closeDialog();
      return this.depositService.createDeposit(newDeposit).subscribe();
    }
  }
}
