import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';



@Component({
  selector: 'app-verify-deposit-creation',
  templateUrl: './verify-deposit-creation.component.html',
  styleUrls: ['./verify-deposit-creation.component.css']
})
export class VerifyDepositCreationComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<VerifyDepositCreationComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit(): void {
  }


}
