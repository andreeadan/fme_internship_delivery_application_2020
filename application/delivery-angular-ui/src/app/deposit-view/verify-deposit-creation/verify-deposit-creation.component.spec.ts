import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyDepositCreationComponent } from './verify-deposit-creation.component';

describe('VerifyDepositCreationComponent', () => {
  let component: VerifyDepositCreationComponent;
  let fixture: ComponentFixture<VerifyDepositCreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerifyDepositCreationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyDepositCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
