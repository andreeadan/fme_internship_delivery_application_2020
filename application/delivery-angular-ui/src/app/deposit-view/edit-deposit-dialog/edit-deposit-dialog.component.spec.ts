import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDepositDialogComponent } from './edit-deposit-dialog.component';

describe('EditDepositDialogComponent', () => {
  let component: EditDepositDialogComponent;
  let fixture: ComponentFixture<EditDepositDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditDepositDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDepositDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
