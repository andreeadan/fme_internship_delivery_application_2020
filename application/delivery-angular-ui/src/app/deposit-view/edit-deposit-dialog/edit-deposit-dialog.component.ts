import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IDeposit } from 'src/app/models/Deposit';
import { DepositViewService } from 'src/app/services/depositView/deposit-view.service';

@Component({
  selector: 'app-edit-deposit-dialog',
  templateUrl: './edit-deposit-dialog.component.html',
  styleUrls: ['./edit-deposit-dialog.component.css'],

})
export class EditDepositDialogComponent implements OnInit {
  county: string = "";
  address: string = "";
  status: string = "";
  idDeposit: any;
  depositObject: any;

  constructor(public dialogRef: MatDialogRef<EditDepositDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private depositService: DepositViewService) { }

  ngOnInit(): void {
    this.depositObject = this.data;
    this.county = this.depositObject.county;
    this.address = this.depositObject.address;
    this.status = this.depositObject.status;
  }

  closeDialog() {
    this.dialogRef.close();
  }

  updateDeposit() {
    var newDeposit = new IDeposit();

    newDeposit.county = this.county;
    newDeposit.address = this.address;
    newDeposit.status = this.status;
    newDeposit.depositID = this.depositObject.depositID;

    if ((this.county != "") && (this.address != "") && (this.status != "")) {
      this.closeDialog();
      this.depositService.updateDeposit(newDeposit).subscribe();
    }
  }
}
