import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Client } from '../models/Client';
import { User } from '../models/User';
import { AuthenticationService } from '../services/authentication/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  hide: boolean = true;
  registeredUser: Client;

  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.registeredUser = new Client();
  }

  register(event: any): void {
    event.preventDefault();
    console.log(this.registeredUser);
    this.authenticationService.register(this.registeredUser).subscribe();
    this.router.navigate(['/login']);
  }

  cancel() {
    this.router.navigate(['/login']);
  }
}
