import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Client } from 'src/app/models/Client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  private url: string = `/delivery-web-service/webapi/users`; 

  constructor(private http: HttpClient) { }

  getClientById(id: string) {
    return this.http.get(this.url + `/client/` + id);
  }

  getClientByUsername(username: string){
    return this.http.get<Client>(this.url + `/clients/` + username);
  }


  getAllClients(): Observable<Client[]> {
    return this.http.get<Client[]>(this.url + `/clients`);
  }

  createClient(newClient:Client ):Observable<Client>{
    return this.http.post<Client>(this.url+'/clients/add', newClient);
  }

  updateClient(newClient:Client):Observable<Client>{
    return this.http.put<Client>(this.url+'/update/client', newClient);
  }

  private handleError(err :HttpErrorResponse){
    let errorMessage='';

    if(err.error instanceof ErrorEvent){
      errorMessage = 'There is an eror:$(err.error.message)';
    }else{
      errorMessage = 'Server returned:$(err.status),error message : $(err.message)';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
