import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Driver } from 'src/app/models/Driver';
import { Route } from '../../models/Route';

@Injectable({
  providedIn: 'root'
})
export class DriverService {


  private routeURL = '/delivery-web-service/webapi/routes';
  private url: string = `/delivery-web-service/webapi`;

  constructor(private http: HttpClient) { }

  getRoutesForDriver(): Observable<Route[]> {
    var driver = JSON.parse(localStorage.getItem("currentUser"))['username'];
    return this.http.get<Route[]>(this.routeURL + "/driver/" + driver).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  updateDriver(newDriver: Driver): Observable<Driver> {
    return this.http.put<Driver>(this.url + '/users/update/driver', newDriver);
  }

  getAllDrivers(): Observable<Driver[]> {
    return this.http.get<Driver[]>(this.url + `/users/drivers`);
  }


  
  getDriverIdByUsername(username: string){
  
    return this.http.get<string>(this.url + `/users/drivers/` + username);
  }

  getDriverById(id:string){
  
    return this.http.get<Driver>(this.url + `/users/driver/` + id);
  } 

  createDriver(newDriver: Driver): Observable<Driver> {
    return this.http.post<Driver>(this.url + '/users/drivers/add', newDriver);
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';

    if (err.error instanceof ErrorEvent) {
      errorMessage = 'There is an eror:$(err.error.message)';
    } else {
      errorMessage = 'Server returned:$(err.status),error message : $(err.message)';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
