import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Admin } from 'src/app/models/Admin';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  private url: string = `/delivery-web-service/webapi`; 

  constructor(private http: HttpClient) { }

  getAllAdmins(): Observable<Admin[]> {
    return this.http.get<Admin[]>(this.url + `/users/admins`);
  }

  getAdminByUsername(username : string): Observable<Admin[]> {
    return this.http.get<Admin[]>(this.url + `/users/admins/`+username);
  }

  createAdmin(newAdmin:Admin ):Observable<Admin>{
    return this.http.post<Admin>(this.url+'/users/admins/add', newAdmin);
  }

  updateAdmin(newAdmin:Admin):Observable<Admin>{
    return this.http.put<Admin>(this.url+'/users/update/admin', newAdmin);
  }

  private handleError(err :HttpErrorResponse){
    let errorMessage='';

    if(err.error instanceof ErrorEvent){
      errorMessage = 'There is an eror:$(err.error.message)';
    }else{
      errorMessage = 'Server returned:$(err.status),error message : $(err.message)';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}