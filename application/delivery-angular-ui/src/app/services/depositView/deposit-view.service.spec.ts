import { TestBed } from '@angular/core/testing';

import { DepositViewService } from './deposit-view.service';

describe('DepositViewService', () => {
  let service: DepositViewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DepositViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
