import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { IDeposit } from 'src/app/models/Deposit';
import { catchError, tap, map } from 'rxjs/operators';
import { $ } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class DepositViewService {

  private depositUrl = "/delivery-web-service/webapi/deposits";
  constructor(private http: HttpClient) { }

  getDeposits(): Observable<IDeposit[]> {
    return this.http.get<IDeposit[]>(this.depositUrl);
    // return this.http.get<IDeposit[]>(this.depositUrl).pipe( tap(data=> console.log('All: '+JSON.stringify(data)))),
    // catchError(this.handleError)
  }

  createDeposit(newDeposit: IDeposit): Observable<IDeposit> {
    return this.http.post<IDeposit>(this.depositUrl + '/add', newDeposit);
  }

  updateDeposit(newDeposit: IDeposit): Observable<IDeposit> {
    return this.http.put<IDeposit>(this.depositUrl + '/update', newDeposit);
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';

    if (err.error instanceof ErrorEvent) {
      errorMessage = 'There is an eror:$(err.error.message)';
    } else {
      errorMessage = 'Server returned:$(err.status),error message : $(err.message)';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
