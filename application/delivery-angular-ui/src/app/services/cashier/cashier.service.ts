import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Cashier } from 'src/app/models/Cashier';
import { User } from 'src/app/models/User';

@Injectable({
  providedIn: 'root'
})
export class CashierService {
  private url: string = `/delivery-web-service/webapi`; 

  constructor(private http: HttpClient) { }

  getCashierById(id: string) {
    return this.http.get(this.url + `/users/cashier/` + id);
  }

  getCashierByUsername(username: string) {
    return this.http.get<User>(this.url + `/users/cashiers/` + username);
  }

  getAllCashiers(): Observable<Cashier[]> {
    return this.http.get<Cashier[]>(this.url + `/users/cashiers`);
  }
  
  createCashier(newCashier:Cashier ):Observable<Cashier>{
    return this.http.post<Cashier>(this.url+'/users/cashiers/add', newCashier);
  }

  updateCashier(newCashier:Cashier):Observable<Cashier>{
    return this.http.put<Cashier>(this.url+'/users/update/cashier', newCashier);
  }

  private handleError(err :HttpErrorResponse){
    let errorMessage='';

    if(err.error instanceof ErrorEvent){
      errorMessage = 'There is an eror:$(err.error.message)';
    }else{
      errorMessage = 'Server returned:$(err.status),error message : $(err.message)';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
  
}
