import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Driver } from 'src/app/models/Driver';
import { Route } from '../../models/Route';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RouteService {

  private routeURL = '/delivery-web-service/webapi/routes';

  constructor(private http: HttpClient) { }
  
  getRoutesForDriver(): Observable<Route[]> {
    var driver = JSON.parse(localStorage.getItem("currentUser"))['username'];
    return this.http.get<Route[]>(this.routeURL + "/driver/" + driver).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  getAllRoutes(): Observable<Route[]> {
    return this.http.get<Route[]>(this.routeURL).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occured: ${err.error.message}`;
    } else {
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
  
  createRoute(route:Route){
    return this.http.post<Route>(this.routeURL+'/add', route);
  }
}
