import { TestBed } from '@angular/core/testing';

import { PackageInvoiceService } from './package-invoice.service';

describe('PackageInvoiceService', () => {
  let service: PackageInvoiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PackageInvoiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
