import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PackageInvoice } from 'src/app/models/PackageInvoice';

@Injectable({
  providedIn: 'root'
})
export class PackageInvoiceService {

  private packageURL: string = `/delivery-web-service/webapi/packageInvoices`; 
  constructor(private http: HttpClient) { }

  getAllInvoices(): Observable<PackageInvoice[]> {
    return this.http.get<PackageInvoice[]>(this.packageURL);
  }

  createInvoice(invoice: PackageInvoice): Observable<PackageInvoice> {
    return this.http.post<PackageInvoice>(this.packageURL + `/add`, invoice);
  }

  updateInvoice(invoice: PackageInvoice): Observable<PackageInvoice> {
    return this.http.put<PackageInvoice>(this.packageURL + `/update`, invoice);
  }
  
  export(invoice: PackageInvoice): Observable<PackageInvoice> {
    return this.http.post<PackageInvoice>(this.packageURL + `/export`, invoice);
  }

  getAllClientInvoices(username:string):Observable<PackageInvoice[]>{
    return this.http.get<PackageInvoice[]>(this.packageURL+'/client/'+username);
  }

  getSentInvoices(username:string):Observable<PackageInvoice[]>{
    return this.http.get<PackageInvoice[]>(this.packageURL+'/sentInvoices/'+username);
  }

  getReceivedInvoices(username:string):Observable<PackageInvoice[]>{
    return this.http.get<PackageInvoice[]>(this.packageURL+'/receivedInvoices/'+username);
  }
}
