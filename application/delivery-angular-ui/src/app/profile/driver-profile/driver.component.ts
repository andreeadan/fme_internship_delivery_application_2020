import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-driver',
  templateUrl: './driver.component.html',
  styleUrls: ['./driver.component.css']
})
export class DriverComponent implements OnInit {

  username: string;
  firstname: string;
  lastname: string;
  car: string;

  constructor() {
   }

  ngOnInit(): void {
    this.username= JSON.parse(localStorage.getItem("currentUser"))['username'];
    this.firstname= JSON.parse(localStorage.getItem("currentUser"))['firstName'];
    this.lastname= JSON.parse(localStorage.getItem("currentUser"))['lastName'];
    this.car= JSON.parse(localStorage.getItem("currentUser"))['car'];
   
  }
}
