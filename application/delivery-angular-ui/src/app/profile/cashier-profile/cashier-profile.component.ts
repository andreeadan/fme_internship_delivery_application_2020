import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cashier-profile',
  templateUrl: './cashier-profile.component.html',
  styleUrls: ['./cashier-profile.component.css']
})
export class CashierProfileComponent implements OnInit {
  username: string = '';
  firstName: string = '';
  lastName: string = '';

  constructor() { }

  ngOnInit(): void {
    this.getLoggedInUser();
  }

  getLoggedInUser(): void {
    this.username = JSON.parse(localStorage.getItem('currentUser'))['username'];
    this.firstName = JSON.parse(localStorage.getItem('currentUser'))['firstName'];
    this.lastName = JSON.parse(localStorage.getItem('currentUser'))['lastName'];
  }
}
