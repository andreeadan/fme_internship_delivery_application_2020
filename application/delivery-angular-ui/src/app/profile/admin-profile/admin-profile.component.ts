import { Component, OnInit } from '@angular/core';
import { Admin } from 'src/app/models/Admin';
import { AdminService } from 'src/app/services/admin/admin.service';

@Component({
  selector: 'app-admin-profile',
  templateUrl: './admin-profile.component.html',
  styleUrls: ['./admin-profile.component.css']
})
export class AdminProfileComponent implements OnInit {
  username: string= '';
  firstName: string= '';
  lastName: string= '';

  constructor(private adminService:AdminService) { }

  ngOnInit(): void {
    this.getLoggedInUser();
  }

  getLoggedInUser(): void{
    this.username=JSON.parse(localStorage.getItem('currentUser'))['username'];
    this.firstName=JSON.parse(localStorage.getItem('currentUser'))['firstName'];
    this.lastName=JSON.parse(localStorage.getItem('currentUser'))['lastName'];
  }

}
