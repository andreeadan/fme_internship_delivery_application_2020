import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Client } from 'src/app/models/Client';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { ClientService } from 'src/app/services/client/client.service';
import { EditClientDialogComponent } from 'src/app/users-groups/edit-client-dialog/edit-client-dialog.component';

@Component({
  selector: 'app-client-view',
  templateUrl: './client-view.component.html',
  styleUrls: ['./client-view.component.css']
})
export class ClientViewComponent implements OnInit {

  firstName:String;
  lastName:String;
  phoneNumber:String;
  address:String;
  city:String;
  county:String;
  zipCode:String;
  username:String;
  client:Client;

  constructor(private clientService:ClientService, private authService:AuthenticationService, private dialog: MatDialog) { }

  ngOnInit(): void {
    console.log(this.authService.currentUserValue)
    this.username = String(this.authService.currentUserValue.username);
    this.clientService.getClientByUsername(this.username.toString()).subscribe((data:Client)=>{
      this.client = data
      this.username = this.client.username
      this.city = this.client.city
      this.county = this.client.county
      this.address = this.client.address
      this.zipCode = this.client.zipCode
      this.phoneNumber = this.client.phoneNumber
      this.firstName = this.client.firstName
      this.lastName = this.client.lastName
    });
    
  }

  editProfile(){
    let dialogEdit = this.dialog.open(EditClientDialogComponent, {
      width: '270px',
      height: '470px',
      data: {
        clientID: Number(this.authService.currentUserValue.clientID),
        firstName: this.firstName,
        lastName: this.lastName,
        username: this.username,
        password: "",
        address: this.address,
        zipCode: this.zipCode,
        city: this.city,
        county: this.county,
        phoneNumber: this.phoneNumber
      },
    });
    dialogEdit.afterClosed().subscribe(result => { this.ngOnInit() });
  }

}
