import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  userRole: string;

  constructor(private router: Router,
    private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.userRole = this.authenticationService.getRole();
  }

  home() {
    switch (this.userRole) {
      case ("admin").valueOf():
        this.router.navigate(['/admin-profile']); // route to admin view
        break;
      case ("client").valueOf():
        this.router.navigate(['/client-profile']); // route to client view
        break;
      case ("driver").valueOf():
        this.router.navigate(['/driver-profile']); // route to driver view
        break;
      case ("cashier").valueOf():
        this.router.navigate(['/cashier-profile']); // route to cashier view
        break;
      default:
        this.router.navigate(['/login']);
    }
  }

  invoices() {
    this.router.navigate(['/invoices']);
  }

  groups() {
    this.router.navigate(['/groups/admins']);
  }


  deposits() {
    this.router.navigate(['/deposits']);
  }

  routes() {
    this.router.navigate(['/routes']);
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
