import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Driver } from 'src/app/models/Driver';
import { DriverService } from 'src/app/services/driver/driver.service';


@Component({
  selector: 'app-new-driver-dialog',
  templateUrl: './new-driver-dialog.component.html',
  styleUrls: ['./new-driver-dialog.component.css']
})
export class NewDriverDialogComponent implements OnInit {

  username: string = ""
  password: string = ""
  firstname: string = ""
  lastname: string = ""
  car: string = ""

  constructor(public dialogRef: MatDialogRef<NewDriverDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private driverService: DriverService) { }

  ngOnInit(): void {
  }

  closeDialog() {
    this.dialogRef.close();
  }

  createDriver() {
    var newDriver = new Driver();

    newDriver.username = this.username;
    newDriver.password = this.password;
    newDriver.firstName = this.firstname;
    newDriver.lastName = this.lastname;
    newDriver.car = this.car;

    this.closeDialog();
    window.location.reload();
    return this.driverService.createDriver(newDriver).subscribe();
  }
}
