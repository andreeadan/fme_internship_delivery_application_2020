import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewDriverDialogComponent } from './new-driver-dialog.component';

describe('NewDriverDialogComponent', () => {
  let component: NewDriverDialogComponent;
  let fixture: ComponentFixture<NewDriverDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewDriverDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewDriverDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
