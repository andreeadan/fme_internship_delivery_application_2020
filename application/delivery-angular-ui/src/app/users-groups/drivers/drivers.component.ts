import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Driver } from 'src/app/models/Driver';
import { DriverService } from 'src/app/services/driver/driver.service';
import { EditDriverDialogComponent } from '../edit-driver-dialog/edit-driver-dialog.component';
import { NewDriverDialogComponent } from '../new-driver-dialog/new-driver-dialog.component';

@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.css']
})
export class DriversComponent implements OnInit {

  displayedColumns: string[] = ['driverID', 'firstName', 'lastName', 'username', 'car'];
  dataSource = new MatTableDataSource<Driver>();
  menuTopLeftPosition = { x: '0', y: '0' }
  dataEntry: Driver;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatMenuTrigger, { static: true }) matMenuTrigger: MatMenuTrigger;


  constructor(
    private driverService: DriverService, private dialog: MatDialog) { }

  ngOnInit() {
    this.driverService.getAllDrivers().subscribe(
      (driverslist: Driver[]) => {
        this.dataSource = new MatTableDataSource<Driver>(driverslist);
      });
    setTimeout(() => this.dataSource.sort = this.sort);
    setTimeout(() => this.dataSource.paginator = this.paginator);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  createDriver() {
    this.dialog.open(NewDriverDialogComponent, {
      width: '270px',
      height: '370px',
      data: {},
    });
  }

  onRightClick(event: MouseEvent, item) {
    // preventDefault avoids to show the visualization of the right-click menu of the browser
    event.preventDefault();

    // we record the mouse position in our object
    this.menuTopLeftPosition.x = event.clientX + 'px';
    this.menuTopLeftPosition.y = event.clientY + 'px';

    // we open the menu
    this.matMenuTrigger.openMenu();
    this.dataEntry = item.content;
  }

  editDriver() {
    let dialogEdit = this.dialog.open(EditDriverDialogComponent, {
      width: '270px',
      height: '390px',
      data: {
        driverID: this.dataEntry.driverID,
        firstName: this.dataEntry.firstName,
        lastName: this.dataEntry.lastName,
        username: this.dataEntry.username,
        password: "",
        car: this.dataEntry.car
      },
    });
    dialogEdit.afterClosed().subscribe(result => { this.ngOnInit() });

  }

}
