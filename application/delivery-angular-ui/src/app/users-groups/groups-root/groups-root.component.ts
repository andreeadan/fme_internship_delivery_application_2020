import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-groups-root',
  templateUrl: './groups-root.component.html',
  styleUrls: ['./groups-root.component.css']
})
export class GroupsRootComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  showadmins(event: any): void {
    event.preventDefault();
    this.router.navigate(['/groups/admins']);
  }

  showclients(event: any): void {
    event.preventDefault();
    this.router.navigate(['/groups/clients']);
  }

  showdrivers(event: any): void {
    event.preventDefault();
    this.router.navigate(['/groups/drivers']);
  }

  showcashiers(event: any): void {
    event.preventDefault();
    this.router.navigate(['/groups/cashiers']);
  }

}
