import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupsRootComponent } from './groups-root.component';

describe('GroupsRootComponent', () => {
  let component: GroupsRootComponent;
  let fixture: ComponentFixture<GroupsRootComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupsRootComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupsRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
