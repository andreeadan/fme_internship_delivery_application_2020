import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { Client } from 'src/app/models/Client';
import { ClientService } from 'src/app/services/client/client.service';

@Component({
  selector: 'app-edit-client-dialog',
  templateUrl: './edit-client-dialog.component.html',
  styleUrls: ['./edit-client-dialog.component.css']
})
export class EditClientDialogComponent implements OnInit {

  firstName: string ="" ;
  lastName: string = "";
  username: string= "";
  password: string= "";
  address: string= "";
  zipCode: string= "";
  city: string= "";
  county: string= "";
  phoneNumber: string= "";
  idClient :any;
  depositObject:any;

  constructor(public dialogRef: MatDialogRef<EditClientDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private clientService: ClientService) { }

  ngOnInit(): void {
    this.depositObject = this.data;
    this.idClient = this.depositObject.adminID;
    this.firstName=this.depositObject.firstName;
    this.lastName = this.depositObject.lastName;
    this.username = this.depositObject.username;
    this.password = this.depositObject.password;
    this.address=this.depositObject.address;
    this.zipCode=this.depositObject.zipCode;
    this.city=this.depositObject.city;
    this.county=this.depositObject.county;
    this.phoneNumber=this.depositObject.phoneNumber;
  }

  closeDialog() {
    this.dialogRef.close();
  }
  
  updateClient(){
    var newClient = new Client();
    newClient.firstName = this.firstName;
    newClient.lastName = this.lastName;
    newClient.username = this.username;
    newClient.password = this.password;
    newClient.address = this.address;
    newClient.zipCode = this.zipCode;
    newClient.city = this.city;
    newClient.county = this.county;
    newClient.phoneNumber = this.phoneNumber;
    newClient.clientID=this.depositObject.clientID;

    if((this.firstName!="") && (this.lastName!="")&&(this.password!="")&&(this.username!="")){
      this.closeDialog();
      this.clientService.updateClient(newClient).subscribe();
    }
  }
}
