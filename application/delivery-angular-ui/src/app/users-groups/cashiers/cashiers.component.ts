import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Cashier } from 'src/app/models/Cashier';
import { CashierService } from 'src/app/services/cashier/cashier.service';
import { EditCashierDialogComponent } from '../edit-cashier-dialog/edit-cashier-dialog.component';


import { NewCashierDialogComponent } from '../new-cashier-dialog/new-cashier-dialog.component';

@Component({
  selector: 'app-cashiers',
  templateUrl: './cashiers.component.html',
  styleUrls: ['./cashiers.component.css']
})
export class CashiersComponent implements OnInit {

  displayedColumns: string[] = ['cashierID', 'firstName', 'lastName', 'username'];
  dataSource = new MatTableDataSource<Cashier>();
  menuTopLeftPosition = { x: '0', y: '0' }
  dataEntry: Cashier;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatMenuTrigger, { static: true }) matMenuTrigger: MatMenuTrigger;

  constructor(
    private cashierService: CashierService, private dialog: MatDialog) { }

  ngOnInit() {
    this.cashierService.getAllCashiers().subscribe(
      (cashierslist: Cashier[]) => {
        this.dataSource = new MatTableDataSource<Cashier>(cashierslist);
      });
    setTimeout(() => this.dataSource.sort = this.sort);
    setTimeout(() => this.dataSource.paginator = this.paginator);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  createCashier() {
    this.dialog.open(NewCashierDialogComponent, {
      width: '270px',
      height: '370px',
      data: {},
    });
  }

  onRightClick(event: MouseEvent, item) {
    // preventDefault avoids to show the visualization of the right-click menu of the browser
    event.preventDefault();

    // we record the mouse position in our object
    this.menuTopLeftPosition.x = event.clientX + 'px';
    this.menuTopLeftPosition.y = event.clientY + 'px';

    // we open the menu
    this.matMenuTrigger.openMenu();
    this.dataEntry = item.content;
  }


  editCashier() {
    let dialogEdit = this.dialog.open(EditCashierDialogComponent, {
      width: '270px',
      height: '370px',
      data: {
        cashierID: this.dataEntry.cashierID,
        firstName: this.dataEntry.firstName,
        lastName: this.dataEntry.lastName,
        username: this.dataEntry.username,
        password: ""
      },
    });
    dialogEdit.afterClosed().subscribe(result => { this.ngOnInit() });

  }

}
