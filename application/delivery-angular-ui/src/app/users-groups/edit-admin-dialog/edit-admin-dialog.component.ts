import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Admin } from 'src/app/models/Admin';
import { AdminService } from 'src/app/services/admin/admin.service';

@Component({
  selector: 'app-edit-admin-dialog',
  templateUrl: './edit-admin-dialog.component.html',
  styleUrls: ['./edit-admin-dialog.component.css']
})
export class EditAdminDialogComponent implements OnInit {

  firstName: string = "";
  lastName: string = "";
  username: string = "";
  password: string = "";
  idAdmin: any;
  depositObject: any;

  constructor(public dialogRef: MatDialogRef<EditAdminDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private adminService: AdminService) { }

  ngOnInit(): void {
    this.depositObject = this.data;
    this.idAdmin = this.depositObject.adminID;
    this.firstName = this.depositObject.county;
    this.lastName = this.depositObject.lastName;
    this.username = this.depositObject.username;
    this.password = this.depositObject.password;
  }

  closeDialog() {
    this.dialogRef.close();
  }

  updateAdmin() {
    var newAdmin = new Admin();
    newAdmin.firstName = this.firstName;
    newAdmin.lastName = this.lastName;
    newAdmin.username = this.username;
    newAdmin.password = this.password;
    newAdmin.adminID = this.depositObject.adminID;

    if ((this.firstName != "") && (this.lastName != "") && (this.password != "") && (this.username != "")) {
      this.closeDialog();
      this.adminService.updateAdmin(newAdmin).subscribe();
    }
  }
}
