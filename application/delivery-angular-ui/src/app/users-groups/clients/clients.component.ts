import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Client } from 'src/app/models/Client';
import { ClientService } from 'src/app/services/client/client.service';

import { EditClientDialogComponent } from '../edit-client-dialog/edit-client-dialog.component';
import { NewClientDialogComponent } from '../new-client-dialog/new-client-dialog.component';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  displayedColumns: string[] = ['clientID', 'firstName', 'lastName', 'username', 'address', 'zipCode', 'city', 'county', 'phoneNumber'];
  dataSource = new MatTableDataSource<Client>();
  menuTopLeftPosition = { x: '0', y: '0' }
  dataEntry: Client;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatMenuTrigger, { static: true }) matMenuTrigger: MatMenuTrigger;

  constructor(
    private clientService: ClientService, private dialog: MatDialog) { }

  ngOnInit() {
    this.clientService.getAllClients().subscribe(
      (clientslist: Client[]) => {
        this.dataSource = new MatTableDataSource<Client>(clientslist);
      });
    setTimeout(() => {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  createClient() {
    this.dialog.open(NewClientDialogComponent, {
      width: '270px',
      height: '670px',
      data: {},
    });
  }

  onRightClick(event: MouseEvent, item) {
    // preventDefault avoids to show the visualization of the right-click menu of the browser
    event.preventDefault();

    // we record the mouse position in our object
    this.menuTopLeftPosition.x = event.clientX + 'px';
    this.menuTopLeftPosition.y = event.clientY + 'px';

    // we open the menu
    this.matMenuTrigger.openMenu();
    this.dataEntry = item.content;
  }

  editClient() {
    let dialogEdit = this.dialog.open(EditClientDialogComponent, {
      width: '270px',
      height: '470px',
      data: {
        clientID: this.dataEntry.clientID,
        firstName: this.dataEntry.firstName,
        lastName: this.dataEntry.lastName,
        username: this.dataEntry.username,
        password: "",
        address: this.dataEntry.address,
        zipCode: this.dataEntry.zipCode,
        city: this.dataEntry.city,
        county: this.dataEntry.county,
        phoneNumber: this.dataEntry.phoneNumber
      },
    });
    dialogEdit.afterClosed().subscribe(result => { this.ngOnInit() });

  }
}
