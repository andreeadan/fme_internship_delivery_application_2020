import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Client } from 'src/app/models/Client';
import { ClientService } from 'src/app/services/client/client.service';


@Component({
  selector: 'app-new-client-dialog',
  templateUrl: './new-client-dialog.component.html',
  styleUrls: ['./new-client-dialog.component.css']
})
export class NewClientDialogComponent implements OnInit {

  username: string = ""
  password: string = ""
  firstname: string = ""
  lastname: string = ""
  address: string = ""
  zipCode: string = ""
  city: string = ""
  county: string = ""
  phoneNumber: string = ""

  constructor(public dialogRef: MatDialogRef<NewClientDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private clientService: ClientService) { }

  ngOnInit(): void {
  }

  closeDialog() {
    this.dialogRef.close();
  }

  createClient() {
    var newClient = new Client();

    newClient.username = this.username;
    newClient.password = this.password;
    newClient.firstName = this.firstname;
    newClient.lastName = this.lastname;
    newClient.address = this.address;
    newClient.zipCode = this.zipCode;
    newClient.city = this.city;
    newClient.county = this.county;
    newClient.phoneNumber = this.phoneNumber;

    this.closeDialog();
    window.location.reload();
    return this.clientService.createClient(newClient).subscribe();
  }
}
