import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Driver } from 'src/app/models/Driver';
import { DriverService } from 'src/app/services/driver/driver.service';

@Component({
  selector: 'app-edit-driver-dialog',
  templateUrl: './edit-driver-dialog.component.html',
  styleUrls: ['./edit-driver-dialog.component.css']
})
export class EditDriverDialogComponent implements OnInit {

  firstName: string = "";
  lastName: string = "";
  username: string = "";
  password: string = "";
  car: string = "";
  idDriver: any;
  depositObject: any;

  constructor(public dialogRef: MatDialogRef<EditDriverDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private driverService: DriverService) { }

  ngOnInit(): void {
    this.depositObject = this.data;
    this.idDriver = this.depositObject.driverID;
    this.firstName = this.depositObject.county;
    this.lastName = this.depositObject.lastName;
    this.username = this.depositObject.username;
    this.password = this.depositObject.password;
    this.car = this.depositObject.car;
  }

  closeDialog() {
    this.dialogRef.close();
  }

  updateDriver() {
    var newDriver = new Driver();
    newDriver.firstName = this.firstName;
    newDriver.lastName = this.lastName;
    newDriver.username = this.username;
    newDriver.password = this.password;
    newDriver.car = this.car;
    newDriver.driverID = this.depositObject.driverID;

    if ((this.firstName != "") && (this.lastName != "") && (this.password != "") && (this.username != "") && (this.car != "")) {
      this.closeDialog();
      this.driverService.updateDriver(newDriver).subscribe();
    }
  }
}
