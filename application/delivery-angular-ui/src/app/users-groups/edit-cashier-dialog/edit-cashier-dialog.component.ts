import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Cashier } from 'src/app/models/Cashier';
import { CashierService } from 'src/app/services/cashier/cashier.service';

@Component({
  selector: 'app-edit-cashier-dialog',
  templateUrl: './edit-cashier-dialog.component.html',
  styleUrls: ['./edit-cashier-dialog.component.css']
})
export class EditCashierDialogComponent implements OnInit {

  firstName: string = "";
  lastName: string = "";
  username: string = "";
  password: string = "";
  idCashier: any;
  depositObject: any;

  constructor(public dialogRef: MatDialogRef<EditCashierDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private cashierService: CashierService) { }

  ngOnInit(): void {
    this.depositObject = this.data;
    this.idCashier = this.depositObject.cashierID;
    this.firstName = this.depositObject.county;
    this.lastName = this.depositObject.lastName;
    this.username = this.depositObject.username;
    this.password = this.depositObject.password;
  }

  closeDialog() {
    this.dialogRef.close();
  }

  updateCashier() {
    var newCashier = new Cashier();
    newCashier.firstName = this.firstName;
    newCashier.lastName = this.lastName;
    newCashier.username = this.username;
    newCashier.password = this.password;
    newCashier.cashierID = this.depositObject.cashierID;

    if ((this.firstName != "") && (this.lastName != "") && (this.password != "") && (this.username != "")) {
      this.closeDialog();
      this.cashierService.updateCashier(newCashier).subscribe();
    }
  }
}
