import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCashierDialogComponent } from './edit-cashier-dialog.component';

describe('EditCashierDialogComponent', () => {
  let component: EditCashierDialogComponent;
  let fixture: ComponentFixture<EditCashierDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditCashierDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCashierDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
