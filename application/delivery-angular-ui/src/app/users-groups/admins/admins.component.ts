import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Admin } from 'src/app/models/Admin';
import { User } from 'src/app/models/User';
import { AdminService } from 'src/app/services/admin/admin.service';
import { EditAdminDialogComponent } from '../edit-admin-dialog/edit-admin-dialog.component';
import { NewAdminDialogComponent } from '../new-admin-dialog/new-admin-dialog.component';

@Component({
  selector: 'app-admins',
  templateUrl: './admins.component.html',
  styleUrls: ['./admins.component.css']
})
export class AdminsComponent implements OnInit {

  displayedColumns: string[] = ['adminID', 'firstName', 'lastName', 'username'];
  dataSource = new MatTableDataSource<Admin>();
  menuTopLeftPosition = { x: '0', y: '0' }
  dataEntry: Admin;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatMenuTrigger, { static: true }) matMenuTrigger: MatMenuTrigger;

  constructor(
    private adminService: AdminService, private dialog: MatDialog) { }

  ngOnInit() {

    this.adminService.getAllAdmins().subscribe(
      (adminslist: Admin[]) => {
        this.dataSource = new MatTableDataSource<Admin>(adminslist);
      });

    setTimeout(() => this.dataSource.sort = this.sort);
    setTimeout(() => this.dataSource.paginator = this.paginator);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  createAdmin() {
    let createDialog = this.dialog.open(NewAdminDialogComponent, {
      width: '270px',
      height: '370px',
      data: {},
    });

    createDialog.afterClosed().subscribe(result => { this.ngOnInit() });
  }

  onRightClick(event: MouseEvent, item) {
    // preventDefault avoids to show the visualization of the right-click menu of the browser
    event.preventDefault();

    // we record the mouse position in our object
    this.menuTopLeftPosition.x = event.clientX + 'px';
    this.menuTopLeftPosition.y = event.clientY + 'px';

    // we open the menu
    this.matMenuTrigger.openMenu();
    this.dataEntry = item.content;
  }


  editAdmin() {
    let dialogEdit = this.dialog.open(EditAdminDialogComponent, {
      width: '270px',
      height: '370px',
      data: {
        adminID: this.dataEntry.adminID,
        firstName: this.dataEntry.firstName,
        lastName: this.dataEntry.lastName,
        username: this.dataEntry.username,
        password: ""
      },
    });
    dialogEdit.afterClosed().subscribe(result => { this.ngOnInit() });
  }
}
