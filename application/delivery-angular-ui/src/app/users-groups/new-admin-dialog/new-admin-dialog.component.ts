import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { Admin } from 'src/app/models/Admin';
import { AdminService } from 'src/app/services/admin/admin.service';



@Component({
  selector: 'app-new-admin-dialog',
  templateUrl: './new-admin-dialog.component.html',
  styleUrls: ['./new-admin-dialog.component.css']
})
export class NewAdminDialogComponent implements OnInit {

  username: string = ""
  password: string = ""
  firstname: string = ""
  lastname: string = ""

  constructor(public dialogRef: MatDialogRef<NewAdminDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private adminService: AdminService) { }

  ngOnInit(): void {
  }

  closeDialog() {
    this.dialogRef.close();
  }

  createAdmin() {
    var newAdmin = new Admin();

    newAdmin.username = this.username;
    newAdmin.password = this.password;
    newAdmin.firstName = this.firstname;
    newAdmin.lastName = this.lastname;

    this.closeDialog();
    //window.location.reload();
    return this.adminService.createAdmin(newAdmin).subscribe();

  }


}
