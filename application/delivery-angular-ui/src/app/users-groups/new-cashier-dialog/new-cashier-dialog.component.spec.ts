import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCashierDialogComponent } from './new-cashier-dialog.component';

describe('NewCashierDialogComponent', () => {
  let component: NewCashierDialogComponent;
  let fixture: ComponentFixture<NewCashierDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewCashierDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCashierDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
