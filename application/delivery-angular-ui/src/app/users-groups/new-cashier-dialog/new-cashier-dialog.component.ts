import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Cashier } from 'src/app/models/Cashier';
import { CashierService } from 'src/app/services/cashier/cashier.service';

@Component({
  selector: 'app-new-cashier-dialog',
  templateUrl: './new-cashier-dialog.component.html',
  styleUrls: ['./new-cashier-dialog.component.css']
})
export class NewCashierDialogComponent implements OnInit {

  username: string = ""
  password: string = ""
  firstname: string = ""
  lastname: string = ""

  constructor(public dialogRef: MatDialogRef<NewCashierDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private cashierService: CashierService) { }

  ngOnInit(): void {
  }

  closeDialog() {
    this.dialogRef.close();
  }

  createCashier() {
    var newCashier = new Cashier();

    newCashier.username = this.username;
    newCashier.password = this.password;
    newCashier.firstName = this.firstname;
    newCashier.lastName = this.lastname;

    this.closeDialog();
    window.location.reload();
    return this.cashierService.createCashier(newCashier).subscribe();

  }

}
