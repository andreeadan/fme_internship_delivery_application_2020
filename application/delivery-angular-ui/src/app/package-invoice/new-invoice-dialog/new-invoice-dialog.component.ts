import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PackageInvoice } from 'src/app/models/PackageInvoice';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { CashierService } from 'src/app/services/cashier/cashier.service';
import { ClientService } from 'src/app/services/client/client.service';
import { PackageInvoiceService } from 'src/app/services/package-invoice/package-invoice.service';

@Component({
  selector: 'app-new-invoice-dialog',
  templateUrl: './new-invoice-dialog.component.html',
  styleUrls: ['./new-invoice-dialog.component.css']
})
export class NewInvoiceDialogComponent implements OnInit {
  userRole: string;
  return_id: string;
  exp_client_id: string;
  dest_client_id: string;
  order_description: string;
  order_cost: string;
  order_weight: string;
  deposit_id: string;
  delivery_date: string;
  content_type: string;
  content_location: string;
  cashier_id: string = JSON.parse(localStorage.getItem('currentUser'))['cashierID'];

  constructor(public dialogRef: MatDialogRef<NewInvoiceDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private packageInvoiceService: PackageInvoiceService,
    private clientService: ClientService,
    private cashierService: CashierService,
    private autService: AuthenticationService) { }

  ngOnInit(): void {
    this.userRole= this.autService.getRole();
  }

  closeDialog() {
    this.dialogRef.close();
  }

  createInvoice() {
    let newInvoice = new PackageInvoice();
    newInvoice.order_description = this.order_description;
    newInvoice.order_cost = this.order_cost;
    newInvoice.order_weight = this.order_weight;
    newInvoice.deposit_id = this.deposit_id;
    newInvoice.delivery_date = this.delivery_date;
    newInvoice.content_type = "-";
    newInvoice.content_location = "-";

    // this.exp_client_id contains the username from the form field
    this.clientService.getClientByUsername(this.exp_client_id).subscribe(
      (sender: any) => {
        // sender.clientID contains the client's id
        newInvoice.exp_client_id = sender.clientID;

        // this.dest_client_id contains the username from the form field
        this.clientService.getClientByUsername(this.dest_client_id).subscribe(
          (reciever: any) => {
            // reciever.clientID contains the client's id
            newInvoice.dest_client_id = reciever.clientID;

            // this.client_id contains the username from the form field
            if(this.userRole!='cashier'){
            this.cashierService.getCashierByUsername(this.cashier_id).subscribe(
              (cashier: any) => {
                // cashier.cashierID contains the cashier's id
                newInvoice.cashier_id = cashier.cashierID;

                if (newInvoice.exp_client_id != "" && newInvoice.dest_client_id != "" && newInvoice.order_cost != "" && newInvoice.order_weight != "" && newInvoice.deposit_id != "" && newInvoice.delivery_date != "" && newInvoice.cashier_id != "") {
                  this.closeDialog();
                  return this.packageInvoiceService.createInvoice(newInvoice).subscribe();
                }
              }
            );
          }else{
            newInvoice.cashier_id= this.cashier_id;
            if (newInvoice.exp_client_id != "" && newInvoice.dest_client_id != "" && newInvoice.order_cost != "" && newInvoice.order_weight != "" && newInvoice.deposit_id != "" && newInvoice.delivery_date != "" && newInvoice.cashier_id != "") {
              this.closeDialog();
              return this.packageInvoiceService.createInvoice(newInvoice).subscribe();
            }
          }
        });
      });
  }
}
