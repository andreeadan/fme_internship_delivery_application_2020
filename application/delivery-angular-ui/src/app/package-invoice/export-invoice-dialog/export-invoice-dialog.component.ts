import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-export-invoice-dialog',
  templateUrl: './export-invoice-dialog.component.html',
  styleUrls: ['./export-invoice-dialog.component.css']
})
export class ExportInvoiceDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ExportInvoiceDialogComponent>) { }

  ngOnInit(): void {
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
