import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportInvoiceDialogComponent } from './export-invoice-dialog.component';

describe('ExportInvoiceDialogComponent', () => {
  let component: ExportInvoiceDialogComponent;
  let fixture: ComponentFixture<ExportInvoiceDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExportInvoiceDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportInvoiceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
