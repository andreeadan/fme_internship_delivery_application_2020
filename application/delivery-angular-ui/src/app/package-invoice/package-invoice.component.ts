import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';

import { User } from '../models/User';
import { PackageInvoice } from '../models/PackageInvoice';
import { PackageInvoiceService } from '../services/package-invoice/package-invoice.service';
import { CashierService } from '../services/cashier/cashier.service';
import { ClientService } from '../services/client/client.service';
import { NewInvoiceDialogComponent } from './new-invoice-dialog/new-invoice-dialog.component';
import { MatMenuTrigger } from '@angular/material/menu';
import { EditInvoiceDialogComponent } from './edit-invoice-dialog/edit-invoice-dialog.component';
import { ViewInvoiceDialogComponent } from './view-invoice-dialog/view-invoice-dialog.component';

import { ExportInvoiceDialogComponent } from './export-invoice-dialog/export-invoice-dialog.component';
import { AuthenticationService } from '../services/authentication/authentication.service';


@Component({
  selector: 'app-package-invoice',
  templateUrl: './package-invoice.component.html',
  styleUrls: ['./package-invoice.component.css']
})
export class PackageInvoiceComponent implements OnInit {

  displayedColumns: string[] = ['invoice_id', 'orderDescription', 'expClient', 'destClient', 'orderStatus', 'creationDate', 'deliveryDate', 'modifyDate', 'cashierId'];
  dataSource = new MatTableDataSource<PackageInvoice>();
  dataEntry: PackageInvoice;
  menuTopLeftPosition = { x: '0', y: '0' };
  userRole: string;

  user: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatMenuTrigger, { static: true }) matMenuTrigger: MatMenuTrigger;

  constructor(private packageInvoiceService: PackageInvoiceService,
    private cashierService: CashierService,
    private clientService: ClientService,
    private dialog: MatDialog,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {

    this.userRole = String(this.authenticationService.getRole()); //JSON.parse(localStorage.getItem("currentUser")).role;
    this.user = this.authenticationService.currentUserValue; //JSON.parse(localStorage.getItem("currentUser"));

    if (this.userRole == 'admin' || this.userRole == 'cashier') {
      this.packageInvoiceService.getAllInvoices().subscribe(
        (invoices: PackageInvoice[]) => { this.setInvoicesAppearance(invoices); });
    }
    else if (this.userRole == 'client') {
      this.packageInvoiceService.getAllClientInvoices(this.user.username).subscribe(
        (invoices: PackageInvoice[]) => { this.setInvoicesAppearance(invoices); });
    }

    setTimeout(() => {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  showAll() {
    this.packageInvoiceService.getAllClientInvoices(this.user.username).subscribe(
      (invoices: PackageInvoice[]) => { this.setInvoicesAppearance(invoices); });
  }

  sentInvoices() {
    this.packageInvoiceService.getSentInvoices(this.user.username).subscribe(
      (invoices: PackageInvoice[]) => { this.setInvoicesAppearance(invoices); });
  }

  receivedInvoices() {
    this.packageInvoiceService.getReceivedInvoices(this.user.username).subscribe(
      (invoices: PackageInvoice[]) => { this.setInvoicesAppearance(invoices); });
  }

  setInvoicesAppearance(invoices: PackageInvoice[]) {
    // change from user_id to user_full_name for Cashier, Sender & Reciever columns
    for (let invoice of invoices) {
      this.cashierService.getCashierById(invoice.cashier_id).subscribe(
        (cashier: User) => {
          invoice.cashier_id = cashier.username; //cashier.firstName + " " + cashier.lastName;

          this.clientService.getClientById(invoice.exp_client_id).subscribe(
            (sender: User) => {
              invoice.exp_client_id = sender.username; //sender.firstName + " " + sender.lastName;

              this.clientService.getClientById(invoice.dest_client_id).subscribe(
                (reciever: User) => {
                  invoice.dest_client_id = reciever.username; //reciever.firstName + " " + reciever.lastName;
                });
            });
        });
      this.dataSource = new MatTableDataSource<PackageInvoice>(invoices);
    }

  }

  onRightClick(event: MouseEvent, item) {
    // preventDefault avoids to show the visualization of the right-click menu of the browser
    event.preventDefault();

    // we record the mouse position in our object
    this.menuTopLeftPosition.x = event.clientX + 'px';
    this.menuTopLeftPosition.y = event.clientY + 'px';

    // we open the menu
    this.matMenuTrigger.openMenu();
    this.dataEntry = item.content;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  createInvoice() {
    let dialogCreate = this.dialog.open(NewInvoiceDialogComponent, {
      width: '250px',
      data: {},
    });
    dialogCreate.afterClosed().subscribe(result => { this.ngOnInit() });
  }

  viewInvoice() {
    let dialogView = this.dialog.open(ViewInvoiceDialogComponent, {
      width: '400px',
      height: '600px',
      data: {
        invoice_id: this.dataEntry.invoice_id,
        return_id: this.dataEntry.return_id,
        exp_client_id: this.dataEntry.exp_client_id,
        dest_client_id: this.dataEntry.dest_client_id,
        order_description: this.dataEntry.order_description,
        order_cost: this.dataEntry.order_cost,
        order_weight: this.dataEntry.order_weight,
        creation_date: this.dataEntry.creation_date,
        order_status: this.dataEntry.order_status,
        deposit_id: this.dataEntry.deposit_id,
        delivery_date: this.dataEntry.delivery_date,
        cashier_id: this.dataEntry.cashier_id,
        modify_date: this.dataEntry.modify_date
      }
    });
    dialogView.afterClosed().subscribe(result => { this.ngOnInit() });
  }

  getInvoiceFromTable(row: PackageInvoice) {
    this.dataEntry = row;
  }

  editInvoice() {
    let dialogEdit = this.dialog.open(EditInvoiceDialogComponent, {
      width: '400px',
      data: {
        invoice_id: this.dataEntry.invoice_id,
        return_id: this.dataEntry.return_id,
        order_description: this.dataEntry.order_description,
        order_cost: this.dataEntry.order_cost,
        deposit_id: this.dataEntry.deposit_id,
        delivery_date: this.dataEntry.delivery_date,
        order_status: this.dataEntry.order_status
      }
    });
    dialogEdit.afterClosed().subscribe(result => { this.ngOnInit() });
  }

  exportInvoice() {
    // reseting the user's username to their id

    // this.dataEntry.exp_client_id contains the user's full name instead of id
    this.clientService.getClientByUsername(this.dataEntry.exp_client_id).subscribe(
      (sender: any) => {
        // sender.clientID contains the client's id
        this.dataEntry.exp_client_id = sender.clientID;

        // this.dataEntry.dest_client_id contains the user's full name instead of id
        this.clientService.getClientByUsername(this.dataEntry.dest_client_id).subscribe(
          (reciever: any) => {
            // reciever.clientID contains the client's id
            this.dataEntry.dest_client_id = reciever.clientID;

            // this.dataEntry.cashier_id contains the user's full name instead of id
            this.cashierService.getCashierByUsername(this.dataEntry.cashier_id).subscribe(
              (cashier: any) => {
                // cashier.cashierID contains the cashier's id
                this.dataEntry.cashier_id = cashier.cashierID;

                // finally subscribing with the new object
                this.packageInvoiceService.export(this.dataEntry).subscribe(result => {
                  this.dialog.open(ExportInvoiceDialogComponent, {
                    width: '400px',
                    height: '150px'
                  }),
                    this.ngOnInit();
                });
              }
            );
          }
        );
      }
    );
  }
}
