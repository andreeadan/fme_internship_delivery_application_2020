import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';

@Component({
  selector: 'app-view-invoice-dialog',
  templateUrl: './view-invoice-dialog.component.html',
  styleUrls: ['./view-invoice-dialog.component.css']
})
export class ViewInvoiceDialogComponent implements OnInit {
  invoice_id: string;
  return_id: string;
  exp_client_id: string;
  dest_client_id: string;
  order_description: string;
  order_cost: string;
  order_weight: string;
  creation_date: string;
  order_status: string;
  deposit_id: string;
  cashier_id: string;
  delivery_date: string;
  modify_date: string;
  invoiceObject: any;
  userRole: string;

  constructor(public dialogRef: MatDialogRef<ViewInvoiceDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.invoiceObject = this.data;
    this.invoice_id = this.invoiceObject.invoice_id;
    this.return_id = this.invoiceObject.return_id;
    this.exp_client_id = this.invoiceObject.exp_client_id;
    this.dest_client_id = this.invoiceObject.dest_client_id;
    this.order_description = this.invoiceObject.order_description;
    this.order_cost = this.invoiceObject.order_cost;
    this.order_weight = this.invoiceObject.order_weight;
    this.creation_date = this.invoiceObject.creation_date;
    this.order_status = this.invoiceObject.order_status;
    this.deposit_id = this.invoiceObject.deposit_id;
    this.cashier_id = this.invoiceObject.cashier_id;
    this.delivery_date = this.invoiceObject.delivery_date;
    this.modify_date = this.invoiceObject.modify_date;
    this.userRole = String(this.authenticationService.getRole());
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
