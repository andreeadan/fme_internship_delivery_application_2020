import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PackageInvoice } from 'src/app/models/PackageInvoice';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { PackageInvoiceService } from 'src/app/services/package-invoice/package-invoice.service';

@Component({
  selector: 'app-edit-invoice-dialog',
  templateUrl: './edit-invoice-dialog.component.html',
  styleUrls: ['./edit-invoice-dialog.component.css']
})
export class EditInvoiceDialogComponent implements OnInit {
  invoice_id: string;
  return_id: string;
  order_description: string;
  order_cost: string;
  deposit_id: string;
  delivery_date: string;
  order_status: string;
  invoiceObject: any;
  userRole: string;

  constructor(public dialogRef: MatDialogRef<EditInvoiceDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private packageInvoiceService: PackageInvoiceService,
    private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.invoiceObject = this.data;
    this.invoice_id = this.invoiceObject.invoice_id;
    this.return_id = this.invoiceObject.return_id;
    this.order_description = this.invoiceObject.order_description;
    this.order_cost = this.invoiceObject.order_cost;
    this.deposit_id = this.invoiceObject.deposit_id;
    this.delivery_date = this.invoiceObject.delivery_date;
    this.order_status = this.invoiceObject.order_status;

    this.userRole = String(this.authenticationService.getRole());
  }

  closeDialog() {
    this.dialogRef.close();
  }

  updateInvoice() {
    var newInvoice = new PackageInvoice;

    newInvoice.return_id = this.return_id;
    newInvoice.order_description = this.order_description;
    newInvoice.order_cost = this.order_cost;
    newInvoice.deposit_id = this.deposit_id;
    newInvoice.delivery_date = this.delivery_date;
    newInvoice.order_status = this.order_status;
    newInvoice.invoice_id = this.invoiceObject.invoice_id;

    console.log(newInvoice);

    if (newInvoice.order_cost != "" && newInvoice.deposit_id != "" && newInvoice.delivery_date != "" && newInvoice.order_status != "") {
      this.closeDialog();
      this.packageInvoiceService.updateInvoice(newInvoice).subscribe();
    }
  }
}
