import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class DriverGuard implements CanActivate {
  constructor(private router: Router,
    private authenticationService: AuthenticationService
  ) { }
  
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const currentUser = this.authenticationService.currentUserValue;
      let role = this.authenticationService.getRole();
      if (currentUser) {
        // logged in
        if(String(role) === "driver") {
          // admin role
          return true;
        }
        
        // not admin role
        this.router.navigate(['/home'], { queryParams: { returnUrl: state.url } });
        return false;
      }
  
      // not logged in
      this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
      return false;
  }
  
}

